using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
namespace TechnoBabelGames
{
    public class TBLineRendererDrawGizmo : MonoBehaviour
    {
        public Transform targetPoint;
        public TBLineRendererComponent parent;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            if (targetPoint != null)
            {
                Gizmos.DrawLine(this.transform.position, targetPoint.transform.position);
            }

            if (!Selection.Contains(parent.gameObject))
            {
                Gizmos.DrawWireSphere(this.transform.position, 0.2f);
            }            

            parent.SetPoints();
        }
    }
}
#endif