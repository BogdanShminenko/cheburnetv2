using System;
using AK.Wwise;
using Assets.Scripts.MVC.InternetControllMVC;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.MVC.UsersAttitude;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;
using Event = AK.Wwise.Event;

namespace Sound
{
    public class MusicController : MonoBehaviour
    {
        [Header("Events")]
        [SerializeField] private Event _musicEvent;
        [SerializeField] private Event _pauseEvent;
        [SerializeField] private Event _continueEvent;
        [SerializeField] private Event _stopEvent;
        
        [Header("States")]
        [SerializeField] private Switch _normalSwitch;
        [SerializeField] private Switch _controlSwitch;
        [SerializeField] private Switch _dangerSwitch;
        
        [Header("Coefficients")]
        [SerializeField][Range(0, 50)] private int _controlTensionPercent = 10;
        [SerializeField][Range(50, 100)] private int _dangerTensionPercent = 95;
        [SerializeField] private int _doomsdayDeadline = 365;

        [Inject] private InternetControllModel _internetControllModel;
        [Inject] private TimerModel _timerModel;
        [Inject] private UsersAttitudeModel _usersAttitudeModel;

        private int _currentTension;
        private Switch _currentSwitch;
        private DateTime _endOfGame = new System.DateTime(2025, 1, 1);
        private bool _doomsDayComes;
        
        private void Start()
        {
            if (_musicEvent.IsValid())
            {
                _normalSwitch.SetValue(gameObject);
                _musicEvent.Post(gameObject);
            }
        }

        private void Update()
        {
            ProcessMusicChanges();
        }

        private void ProcessMusicChanges()
        {
            if (_doomsDayComes)
            {
                return;
            }
            
            var timeLeft = _endOfGame - new DateTime((int) _timerModel.CurrentYear, (int) _timerModel.CurrentMonth, 1);
            if (timeLeft.TotalDays - _doomsdayDeadline < 0)
            {
                _dangerSwitch.SetValue(gameObject);
                _doomsDayComes = true;
                return;
            }
            
            var tension = GetCurrentTension();
            if (tension == _currentTension)
            {
                return;
            }
            
            _currentTension = tension;
            Switch newSwitch;
            
            if (_currentTension >= _dangerTensionPercent)
            {
                newSwitch = _dangerSwitch;
            } else if (_currentTension <= _controlTensionPercent)
            {
                newSwitch = _controlSwitch;
            } else
            {
                newSwitch = _normalSwitch;
            }
            
            if (_currentSwitch == null || newSwitch.Id != _currentSwitch.Id)
            {
                _currentSwitch = newSwitch;
                _currentSwitch.SetValue(gameObject);
            }
        }
        
        private void OnDestroy()
        {
            Stop();
        }

        private int GetCurrentTension()
        {
            var rebels = _usersAttitudeModel.RebelUsersPercent;
            var control = _internetControllModel.InternetControll;
                
            return Mathf.RoundToInt(((rebels - control) / (110 - Math.Max(rebels, control))) * 50 + 50);
        }

        public void Pause()
        {
            if (_pauseEvent.IsValid())
            {
                _pauseEvent.Post(gameObject);
            }
        }

        public void Continue()
        {
            if (_continueEvent.IsValid())
            {
                _continueEvent.Post(gameObject);
            }
        }

        public void Stop()
        {
            _stopEvent.Post(gameObject);
            _normalSwitch.SetValue(gameObject);
        }
    }
}