using System;
using AK.Wwise;
using Assets.Scripts.MVC.InternetControllMVC;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.MVC.UsersAttitude;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;
using Event = AK.Wwise.Event;

namespace Sound
{
    public class SoundSettings : MonoBehaviour
    {
        [SerializeField] private State _musicOnState;
        [SerializeField] private State _musicOffState;
        [SerializeField] private State _sfxOnState;
        [SerializeField] private State _sfxOffState;
        [SerializeField] private bool _initOnAwake;

        private string _musicPrefKey = "IsOnMusic";
        private string _sfxPrefKey = "IsOnSound";
        
        private void Awake()
        {
            if (_initOnAwake)
            {
                ApplyStates();
            }
        }
        
        private void Start()
        {
            if (!_initOnAwake)
            {
                ApplyStates();
            }
        }

        private bool IsMusicOn()
        {
            return HasEnabledPlayerPref(_musicPrefKey);
        }

        private bool IsSfxOn()
        {
            return HasEnabledPlayerPref(_sfxPrefKey);
        }

        private bool HasEnabledPlayerPref(string prefKey)
        {
            if (PlayerPrefs.HasKey(prefKey))
            {
                if (PlayerPrefs.GetInt(prefKey) == 0)
                {
                    return false;
                }
                
                return true;
            }
           
            return true;
        }

        public void ApplyStates()
        {
            if (IsMusicOn())
            {
                _musicOnState.SetValue();
            }
            else
            {
                _musicOffState.SetValue();
            }

            if (IsSfxOn())
            {
                _sfxOnState.SetValue();
            }
            else
            {
                _sfxOffState.SetValue();
            }
        }
    }
}