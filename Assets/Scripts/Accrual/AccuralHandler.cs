﻿using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.MVC.InternetControllMVC;
using Assets.Scripts.MVC.UsersAttitude;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Accrual
{
    public class AccuralHandler : MonoBehaviour
    {
        [Inject] private CurrencyController _currencyController;
        [Inject] private UsersAttitudeController _usersAttitudeController;
        [Inject] private InternetControllController _internetControllController;

        public void HandleAccuralData(AccuralData accuralData)
        {
            if(accuralData.CurrencyType == CurrencyType.Will)
            {
                CalculateWill(accuralData.OperationType, accuralData.Count);
            }
            else if (accuralData.CurrencyType == CurrencyType.Money)
            {
                CalculateMoney(accuralData.OperationType, accuralData.Count);
            }
            else if (accuralData.CurrencyType == CurrencyType.Rebel)
            {
                CalculateRebelPercent(accuralData.OperationType, accuralData.Count);
            }
            else if (accuralData.CurrencyType == CurrencyType.Loyal)
            {
                CalculateLoyalPercent(accuralData.OperationType, accuralData.Count);
            }
            else if (accuralData.CurrencyType == CurrencyType.Neutral)
            {
                CalculateNeutralPercent(accuralData.OperationType, accuralData.Count);
            }
            else if (accuralData.CurrencyType == CurrencyType.InternetControl)
            {
                CalculateInternetControll(accuralData.OperationType, accuralData.Count);
            }
        }


        private void CalculateRebelPercent(OperationType operationType, int count)
        {
            if (operationType == OperationType.Increase)
                _usersAttitudeController.IncreaseRebelPercent(count);
            else if (operationType == OperationType.Decrease)
                _usersAttitudeController.DecreaseRebelPercent(count);
        }
        private void CalculateLoyalPercent(OperationType operationType, int count)
        {
            if (operationType == OperationType.Increase)
                _usersAttitudeController.IncreaseLoyalPercent(count);
            else if (operationType == OperationType.Decrease)
                _usersAttitudeController.DecreaseLoyalPercent(count);
        }

        private void CalculateNeutralPercent(OperationType operationType, int count)
        {
            if (operationType == OperationType.Increase)
                _usersAttitudeController.IncreaseNeutralPercent(count);
            else if (operationType == OperationType.Decrease)
                _usersAttitudeController.DecreaseNeutralPercent(count);
        }

        private void CalculateMoney(OperationType operationType, int count)
        {
            if(operationType == OperationType.Increase)
                _currencyController.AddMoney(count);
            else if(operationType == OperationType.Decrease)
                _currencyController.SpendMoney(count);
        }
        private void CalculateWill(OperationType operationType, int count)
        {
            if (operationType == OperationType.Increase)
                _currencyController.AddMoney(count);
            else if (operationType == OperationType.Decrease)
                _currencyController.SpendMoney(count);
        }

        private void CalculateInternetControll(OperationType operationType, int count)
        {
            if (operationType == OperationType.Increase)
                _internetControllController.IncreaseControllModel (count);
            else if (operationType == OperationType.Decrease)
                _internetControllController.DescreaseControllModel(count);
        }
    }
}