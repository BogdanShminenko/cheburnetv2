﻿using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Accrual
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CurrencyType
    {
        Will = 0,
        Money = 1,
        Rebel = 2,
        Loyal = 3,
        Neutral = 4,
        InternetControl = 5
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum OperationType
    {
        Increase = 0,
        Decrease = 1
    }

    public class AccuralData
    {
        public CurrencyType CurrencyType { get; private set; }
        public OperationType OperationType { get; private set; }
        public int Count { get; private set; }  

        public AccuralData(CurrencyType currencyType, OperationType operationType , int count)
        {
            CurrencyType = currencyType;
            OperationType = operationType;
            Count = count;
        }
        public AccuralData(CurrencyType currencyType, int count)
        {
            CurrencyType = currencyType;
            if(count > 0)
            {
                OperationType = OperationType.Increase;
            }
            else
            {
                OperationType = OperationType.Decrease;
            }
            Count = count;
        }

    }
}