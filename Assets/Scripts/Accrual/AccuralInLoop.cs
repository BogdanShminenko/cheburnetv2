﻿using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.MVC.UsersAttitude;
using Assets.Scripts.TriggersAndOperationsTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Accrual
{
    public class AccuralInLoop
    {
        [Inject] private AccuralHandler _accuralHandler;
        public event Action OnCalculateAttitudeResult;

        private List<AttitudeResult> _attitudeResultsSlow = new List<AttitudeResult>();
        private List<AttitudeResult> _attitudeResultsNormal = new List<AttitudeResult>();
        private List<AttitudeResult> _attitudeResultsFast = new List<AttitudeResult>();

        public IEnumerable<AttitudeResult> AttitudeResults => _attitudeResultsSlow.Union(_attitudeResultsNormal).Union(_attitudeResultsFast);

        public IEnumerable<AttitudeResult> AttitudeResultsSlow => _attitudeResultsSlow;
        public IEnumerable<AttitudeResult> AttitudeResultsNormal => _attitudeResultsNormal;
        public IEnumerable<AttitudeResult> AttitudeResultsFast => _attitudeResultsFast;

        public void SetAttitudeResults(IEnumerable<AttitudeResult> attitudeResultsSlow , 
                                        IEnumerable<AttitudeResult> attitudeResultsNormal,
                                        IEnumerable<AttitudeResult> attitudeResultsFast)
        {
            _attitudeResultsSlow.AddRange(attitudeResultsSlow);
            _attitudeResultsNormal.AddRange(attitudeResultsNormal);
            _attitudeResultsFast.AddRange(attitudeResultsFast);
        }

        public void HandleAttitudeResult(AttitudeResult attitudeResult)
        {
            if(attitudeResult == null)
            {
                Debug.LogError("AttitudeResult is null");
                return;
            }

            if (attitudeResult.ValueCangeType == ValueCangeType.Moment)
            {
                _accuralHandler.HandleAccuralData(
                    new AccuralData(attitudeResult.CurrencyType, attitudeResult.OperationType, attitudeResult.StartCount));
            }
            else
            {
                AddInLoop(attitudeResult);
            }
        }

        public void CalculateAttitudeResultsSlow()
        {
            OnCalculateAttitudeResult?.Invoke();
            for (int i = 0; i < _attitudeResultsSlow.Count; i++)
            {
                HandleAttitudeResultInLoop(_attitudeResultsSlow[i], ref _attitudeResultsSlow);
            }  
        }

        public void CalculateAttitudeResultsNormal()
        {
            OnCalculateAttitudeResult?.Invoke();
            for (int i = 0; i < _attitudeResultsNormal.Count; i++)
            {
                HandleAttitudeResultInLoop(_attitudeResultsNormal[i], ref _attitudeResultsNormal);
            }
        }

        public void CalculateAttitudeResultsFast()
        {
            OnCalculateAttitudeResult?.Invoke();
            for (int i = 0; i < _attitudeResultsFast.Count; i++)
            {
                HandleAttitudeResultInLoop(_attitudeResultsFast[i] , ref _attitudeResultsFast);
            }
        }

        private void HandleAttitudeResultInLoop(AttitudeResult attitudeResult , ref List<AttitudeResult> attitudeResults)
        {
            if (attitudeResult.Count <= 0)
            {
                attitudeResults.Remove(attitudeResult);
            }
            else
            {
                _accuralHandler.HandleAccuralData(
                    new AccuralData(attitudeResult.CurrencyType, attitudeResult.OperationType, 1));
                attitudeResult.DecreaseCountByOne();
            }
        }

        private void AddInLoop(AttitudeResult attitudeResult)
        {
            if(attitudeResult.ValueCangeType == ValueCangeType.Slow)
            {
                _attitudeResultsSlow.Add(attitudeResult);
            }else if(attitudeResult.ValueCangeType == ValueCangeType.Normal)
            {
                _attitudeResultsNormal.Add(attitudeResult);
            }else if(attitudeResult.ValueCangeType== ValueCangeType.Fast)
            {
                _attitudeResultsFast.Add(attitudeResult);
            }
        }
    }
}