﻿using Assets.Scripts.MVC.UsersAttitude;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.InternetObjects.User
{
    [Serializable]
    public sealed class UserObjectInfo 
    {
        public Vector2 Position;
        public Vector2 Offset;
        public float Distance;
        public ObjectLocationType ObjectLocationType;
        public AttitudeTypes AttitudeTypes;

        public UserObjectInfo(Vector2 position, float distance, ObjectLocationType objectLocationType, Vector2 offset, AttitudeTypes attitudeTypes)
        {
            Position = position;
            Distance = distance;
            ObjectLocationType = objectLocationType;
            Offset = offset;
            AttitudeTypes = attitudeTypes;
        }
    }
}