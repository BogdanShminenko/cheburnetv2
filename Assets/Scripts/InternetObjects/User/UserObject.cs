﻿using System;
using Assets.Scripts.MVC.UsersAttitude;
using System.Collections;
using UnityEngine;
using Event = AK.Wwise.Event;

namespace Assets.Scripts.InternetObjects.User
{
    public sealed class UserObject : InternetObject
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public Event _initEvent;
        }
        
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private Sprite _rebelSprite;
        [SerializeField] private Sprite _loyalSprite;
        [SerializeField] private Sprite _neutralSprite;
        public AttitudeTypes AttitudeTypesBelonging { get; private set; }
        [field: SerializeField] public float DistanceByServer { get; private set; }

        [SerializeField] private WwiseSound _wwiseSound;
        
        public void SetDistanceByServer(float distanceByServer)
        {
            DistanceByServer = distanceByServer;
        }

        private void Start()
        {
            if (Time.timeSinceLevelLoad > 0 && _wwiseSound._initEvent.IsValid())
            {
                _wwiseSound._initEvent.Post(gameObject);
            }
        }

        public void ChangeColorByAttitude(AttitudeTypes attitudeTypes)
        {
            if (attitudeTypes == AttitudeTypes.Loyal)
            {
                AttitudeTypesBelonging = attitudeTypes;
                _spriteRenderer.sprite = _loyalSprite;
            }
            else if (attitudeTypes == AttitudeTypes.Rebel)
            {
                AttitudeTypesBelonging = attitudeTypes;
                _spriteRenderer.sprite = _rebelSprite;
            }
            else if (attitudeTypes == AttitudeTypes.Neutral)
            {
                AttitudeTypesBelonging = attitudeTypes;
                _spriteRenderer.sprite = _neutralSprite;
            }
        }
    }
}