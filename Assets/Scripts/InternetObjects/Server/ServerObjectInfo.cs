﻿using Assets.Scripts.MVC.ServerMVC;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.InternetObjects.Servers
{
    [Serializable]
    public sealed class ServerObjectInfo
    {
        public Vector2 Position;
        public Server Server;
        public float Distance;
        public ObjectLocationType ObjectLocationType;
        public Vector2 Offset;
        public bool NotificationStatus;

        public ServerObjectInfo(Vector2 position, Server server, float distance,
            ObjectLocationType objectLocationType, Vector2 offset, bool notificationStatus)
        {
            Position = position;
            Server = server;
            Distance = distance;
            ObjectLocationType = objectLocationType;
            Offset = offset;
            NotificationStatus = notificationStatus;
        }
    }
}