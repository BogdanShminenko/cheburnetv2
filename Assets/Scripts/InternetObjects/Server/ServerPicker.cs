﻿using Assets.Scripts.InternetObjects.Servers;
using System.Collections;
using UnityEngine;

public class ServerPicker : MonoBehaviour
{
    [SerializeField] private LayerMask _serverLayerMask;

    private void TryPickServer()
    {
        var raycast = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 10, _serverLayerMask);
        if (raycast.transform != null)
        {
            if (raycast.transform.TryGetComponent(out ServerObject component))
            {
                component.Select();
            }
        }
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            TryPickServer();
        }
    }
}