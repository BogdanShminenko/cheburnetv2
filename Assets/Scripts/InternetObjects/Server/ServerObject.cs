﻿using System;
using Assets.Scripts.Data.GameData;
using Assets.Scripts.GameStates;
using Assets.Scripts.MVC.ServerMVC;
using Assets.Scripts.Utilities;
using System.Collections;
using UnityEngine;
using Zenject;
using Event = AK.Wwise.Event;

namespace Assets.Scripts.InternetObjects.Servers
{
    public sealed class ServerObject : InternetObject
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public Event _initEvent;
            public Event _clickEvent;
        }
        
        [SerializeField] private Sprite _captureSprite;
        [SerializeField] private Sprite _freeSprite;
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private SpriteRenderer _iconSprite;
        [SerializeField] private GameObject _notification;
        [SerializeField] private Animator _animator;
        
        private ServerController _serverController;
        private CheckUI _checkUI;
        private GameState _gameState;
        private bool _isHaveChanged;
        [field: SerializeField] public Server Server { get; private set; }
        
        [SerializeField] private WwiseSound _wwiseSound;
        public bool NotificationStatus => _notification.activeSelf;

        public void InitServer(GameState gameState, CheckUI checkUI, Server server, ServerController serverController)
        {
            _gameState = gameState;
            _checkUI = checkUI;
            Server = server;
            _serverController = serverController;
            Server.OnChangeOwnage += ChangeOwnageHandler;
            _gameState.OnGameStateChanged += ChangeGameState;
            if(server.IconSprite != null )
            {
                _iconSprite.color = Color.white;
                _iconSprite.sprite = server.IconSprite;
            }

            if(server.CurrentServerActionCase != null && server.CurrentServerActionCase.ServerActions.Count > 0 )
            {
                OnNotification();
            }
            server.OnAddedNewServerAction += OnNotification;
            
            if (Time.timeSinceLevelLoad > 0 && _wwiseSound._initEvent.IsValid())
            {
                _wwiseSound._initEvent.Post(gameObject);
            }
        }

        public void SetNotificationStatus(bool status)
        {
            _notification.SetActive(status);
        }

        public void OnNotification()
        {
            _notification.SetActive(true);
        }

        public void OffNotification()
        {
            _notification.SetActive(false);
        }

        private void ChangeOwnageHandler(Ownage ownage)
        {
            Server.Ownage = ownage;
            _isHaveChanged = true;
        }

        private void ChangeGameState()
        {
            if(_gameState.CurrentGameState == GameStates.GameStates.InternetMap)
            {
                if(_isHaveChanged == true)
                {
                    if (Server.Ownage == Ownage.User)
                    {
                        //_animator.CrossFade("ServerCapture", 0);
                        _animator.SetTrigger("StartCaptured");
                        _animator.SetTrigger("Captured");
                    }
                    _isHaveChanged = false;
                }
            }
        }

        public void OnEndAnimation()
        {
            ChangeSprite();
        }

        public void ChangeSprite()
        {
            if (Server.Ownage == Ownage.Free)
            {
                _spriteRenderer.sprite = _freeSprite;
            }
            else
            {
                _animator.SetTrigger("Captured");
                _spriteRenderer.sprite = _captureSprite;
            }
        }

        public void Select()
        {
            if (!_checkUI.IsPickedUI() && _gameState.CurrentGameState == GameStates.GameStates.InternetMap)
            {
                _serverController.SelectServer(Server , this);
                
                if (_wwiseSound._clickEvent.IsValid())
                {
                    _wwiseSound._clickEvent.Post(gameObject);
                }
            }
        }

    }
}