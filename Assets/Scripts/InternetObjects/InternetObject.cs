﻿using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Collections;
using UnityEngine;
using System;
using Assets.Scripts.MVC.UsersAttitude;

namespace Assets.Scripts.InternetObjects
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ObjectLocationType
    {
        Inner = 0,
        OuterUp = 1,
        OuterDown = 2
    }

    [Serializable]
    public abstract class InternetObject : MonoBehaviour
    {
        public float Distace;
        public ObjectLocationType ObjectLocationType;
        public Vector2 Position;
        public Vector2 Offset;
    }
}