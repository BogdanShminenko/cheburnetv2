﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.InternetObjects.Node
{
    [Serializable]
    public sealed class NodeObjectInfo
    {
        public Vector2 Position;
        public float Distance;
        public ObjectLocationType ObjectLocationType;

        public NodeObjectInfo(Vector2 position, float distance, ObjectLocationType objectLocationType)
        {
            Position = position;
            Distance = distance;
            ObjectLocationType = objectLocationType;
        }
    }
}