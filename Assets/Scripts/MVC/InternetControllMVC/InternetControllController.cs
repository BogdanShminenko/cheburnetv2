﻿using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.InternetControllMVC
{
    public class InternetControllController : MonoBehaviour
    {
        [Inject] private InternetControllModel _internetControllModel;
        [Inject] private GameSettingsData _gameSettingsData;

        private void Start()
        {
            IncreaseControllModel((int)_gameSettingsData.InternetControll);
        }

        public void IncreaseControllModel(int count)
        {
            _internetControllModel.IncreaseInternetControll(count);
        }

        public void DescreaseControllModel(int count)
        {
            _internetControllModel.DecreaseInternetControll(count);
        }

    }
}