﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MVC.InternetControllMVC
{
    public class InternetControllView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _internetControllValueText;
        [SerializeField] private Image _loadBar;

        [Inject] private InternetControllModel _internetControllModel;

        private void OnEnable()
        {
            _internetControllModel.OnChangeInternetControll += DisplayValue;
        }

        private void OnDisable()
        {
            _internetControllModel.OnChangeInternetControll -= DisplayValue;
        }

        private void DisplayValue(int value)
        {
            _internetControllValueText.text = value.ToString() + "%";
            _loadBar.fillAmount = value/100f;
        }

    }
}