﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.InternetControllMVC
{
    public class InternetControllModel
    {
        public event System.Action<int> OnChangeInternetControll;
        [SerializeField] private int _internetControll;

        public int InternetControll => _internetControll;

        public void SetInternetControll(int value)
        {
            _internetControll = value;
            OnChangeInternetControll?.Invoke(InternetControll);
        }

        public void IncreaseInternetControll(int internetControll)
        {
            _internetControll += internetControll;
            if(_internetControll < 0)
            {
                _internetControll = 0;
            }
            OnChangeInternetControll?.Invoke(InternetControll);
        }

        public void DecreaseInternetControll(int internetControll)
        {
            _internetControll -= internetControll;
            if (_internetControll < 0)
            {
                _internetControll = 0;
            }
            OnChangeInternetControll?.Invoke(InternetControll);
        }
    }
}