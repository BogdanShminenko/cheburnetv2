﻿using Assets.Scripts.Data.GameData.Configs;
using Newtonsoft.Json;
using System.Collections;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.MVC.SaveSystemMVC
{
    public class SaveSystemModel
    {
        public SaveFile SaveFile { get; private set; }
        private const string FILE_NAME = "Save.json";
        
        public void CreateSaveFile()
        {
            SaveFile = new SaveFile();
        }

        public void LoadSave()
        {
            ReadFile();
        }

        public async void Save()
        {
            await WriteFileAsync();
        }

        public async Task WriteFileAsync()
        {
            string path;

#if UNITY_EDITOR
            path = Path.Combine(Application.streamingAssetsPath, FILE_NAME);
#else
        path = Path.Combine(Application.dataPath, FILE_NAME);
#endif

            string jsonString = JsonConvert.SerializeObject(SaveFile, Formatting.Indented);

            // Используем асинхронный метод для записи файла
            using (StreamWriter writer = new StreamWriter(path))
            {
                await writer.WriteAsync(jsonString);
            }
        }

        public void ReadFile()
        {
            string path;

#if UNITY_EDITOR
            path = Path.Combine(Application.streamingAssetsPath, FILE_NAME);
#else
            path = Path.Combine(Application.dataPath, FILE_NAME);
#endif
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            var json = File.ReadAllText(path);
            SaveFile = JsonConvert.DeserializeObject<SaveFile>(json, settings);
        }

        public void WriteFile()
        {
            string path;

#if UNITY_EDITOR
            path = Path.Combine(Application.streamingAssetsPath, FILE_NAME);
#else
            path = Path.Combine(Application.dataPath, "FILE_NAME");
#endif

            string jsonString = JsonConvert.SerializeObject(SaveFile, Formatting.Indented);
            File.WriteAllText(path, jsonString);
        }
    }
}