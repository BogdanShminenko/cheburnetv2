﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.MVC.ProjectMVC;
using Assets.Scripts.MVC.LawsMVC;
using Assets.Scripts.MVC.EventMVC;
using Assets.Scripts.MVC.ServerMVC;
using Assets.Scripts.InternetObjects.Node;
using Assets.Scripts.InternetObjects.Servers;
using Assets.Scripts.InternetObjects.User;
using Assets.Scripts.MVC.QuestMVC;
using Assets.Scripts.TriggersAndOperationsTypes;
using Assets.Scripts.InternetMapGeneration;

namespace Assets.Scripts.MVC.SaveSystemMVC
{
    [Serializable]
    public class SaveFile
    {
#nullable enable
        public List<Project>? Projects;
        public List<Law>? Laws;
        public List<Law>? UnsuccesLaws;
        public List<EventMVC.Event>? Events;
        public List<EventMVC.Event>? HandeledEvents;
        public List<ServerActionCase>? ServerActionCasesQueue;
        public List<Server>? ServersTriggerQueue;
        public List<Server>? Servers;
        public List<ServerAction>? ServerActions;
        public List<NodeObjectInfo>? NodeObjectInfos;
        public List<ServerObjectInfo>? ServerObjectsInfo;
        public List<UserObjectInfo>? UserObjectsInfo;
        public List<Quest>? QuestsQueue;
        public List<Quest>? TriggeredQuests;
        public List<string>? ResultTriggers;
        public List<AttitudeResult>? AttitudeResultsSlow;
        public List<AttitudeResult>? AttitudeResultsNormal;
        public List<AttitudeResult>? AttitudeResultsFast ;

        public List<Cell>? CellsCircleMapGeneration;
        public List<Cell>? CellsQueueCircleMapGeneration;

        public List<Cell>? CellsSquareMapGeneration;
        public List<Cell>? CellsQueueSquareMapGeneration;

        public bool LawViewNotificationSatus;
        public bool QuestViewNotificationSatus;
        public bool EventViewNotificationSatus;
        public bool ProjectViewNotificationSatus;


        public uint CurrentYear;
        public uint CurrentMonth;
        public int MonthCount;
        public float TimeSinceYearPassed;

        public int LoyalUsersPercent;
        public int RebelUsersPercent;

        public int InternetControll;
        public int CountUsersInInnerInternet;
        public uint MoneyAmount;
        public uint MaxMoneyAmount;
        public uint MoneyRestoreAmount;
        public float MoneyRestoreTime;
        public bool MoneyIsRestoring;

        public uint WillAmount;
        public uint MaxWillAmount;
        public uint WillRestoreAmount;
        public float WillRestoreTime;
        public bool WillIsRestoring;
    }
}