﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Game;
using Assets.Scripts.GameStates;
using Assets.Scripts.InternetMapGeneration;
using Assets.Scripts.InternetObjects.Node;
using Assets.Scripts.InternetObjects.Servers;
using Assets.Scripts.InternetObjects.User;
using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.MVC.EventMVC;
using Assets.Scripts.MVC.InternetControllMVC;
using Assets.Scripts.MVC.InternetMVC;
using Assets.Scripts.MVC.LawsMVC;
using Assets.Scripts.MVC.LawsMVC.LawViews;
using Assets.Scripts.MVC.ProjectMVC;
using Assets.Scripts.MVC.QuestMVC;
using Assets.Scripts.MVC.ServerMVC;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.MVC.UsersAttitude;
using Assets.Scripts.Utilities;
using System.Collections;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.SaveSystemMVC
{
    public class SaveSystemController : MonoBehaviour
    {
        [Inject] private SaveSystemModel _saveSystemModel;
        [Inject] private ProjectModel _projectModel;
        [Inject] private QuestModel _questModel;
        [Inject] private LawModel _lawModel;
        [Inject] private UnsuccessfulState _unsuccessfulState;
        [Inject] private EventModel _eventModel;
        [Inject] private ServerModel _serverModel;
        [Inject] private CellsCircleMapGeneration _cellsCircleMapGeneration;
        [Inject] private CellSquareMapGeneration _cellSquareMapGeneration;
        [Inject] private RoundGeneration _roundGeneration;
        [Inject] private ServerController _serverController;
        [Inject] private InternetModel _internetModel;
        [Inject] private ProjectController _projectController;
        [Inject] private GameState _gameState;
        [Inject] private ProjectView _projectView;
        [Inject] private ServerObject _serverObjectPrefab;
        [Inject] private UserObject _userObjectPrefab;
        [Inject] private NodeObject _nodeObjectPrefab;

        [Inject] private TimerModel _timerModel;
        [Inject] private UsersAttitudeModel _usersAttitudeModel;
        [Inject] private InternetControllModel _internetControllModel;
        [Inject] private MoneyModel _moneyModel;
        [Inject] private WillModel _willModel;
        [Inject] private DateTriggerConventer _dateTriggerConventer;
        [Inject] private GameInfo _gameInfo;
        [Inject] private AccuralInLoop _accuralInLoop;
        [Inject] private AttitudeCheck _attitudeCheck;
        [Inject] private CheckUI _checkUI;
        [Inject] private OuterServerLineService _outerServerLineService;
        [Inject] private LawView _lawView;
        [Inject] private QuestView _questView;
        [Inject] private EventView _eventView;

        private void OnEnable()
        {
            _timerModel.OnMonthPassedCallback += CallSave;
        }

        private void OnDisable()
        {
            _timerModel.OnMonthPassedCallback -= CallSave;
        }

        private IEnumerator LoadSave()
        {
            _saveSystemModel.LoadSave();
            _timerModel.SetTimeAndStartTimerCycle(_saveSystemModel.SaveFile.CurrentYear, _saveSystemModel.SaveFile.CurrentMonth,
                _saveSystemModel.SaveFile.TimeSinceYearPassed, _saveSystemModel.SaveFile.MonthCount);
            _gameInfo.SetResultTriggers(_saveSystemModel.SaveFile.ResultTriggers);
            _accuralInLoop.SetAttitudeResults(_saveSystemModel.SaveFile.AttitudeResultsSlow,
                _saveSystemModel.SaveFile.AttitudeResultsNormal,
                _saveSystemModel.SaveFile.AttitudeResultsFast);

            _internetModel.SetCountUsersInInnerInternet(_saveSystemModel.SaveFile.CountUsersInInnerInternet);

            foreach (var item in _saveSystemModel.SaveFile.Laws)
                item.LoadIcon();

            foreach (var item in _saveSystemModel.SaveFile.Projects)
                item.LoadIcon();

            foreach (var item in _saveSystemModel.SaveFile.Events)
                item.LoadIcon();

            foreach (var item in _saveSystemModel.SaveFile.Servers)
                item.LoadIcon();

            foreach (var item in _saveSystemModel.SaveFile.ServerObjectsInfo)
                item.Server.LoadIcon();

            foreach (var item in _saveSystemModel.SaveFile.ServersTriggerQueue)
                item.LoadIcon();

            foreach (var item in _saveSystemModel.SaveFile.HandeledEvents)
                item.LoadIcon();


            _projectModel.SetProjects(_saveSystemModel.SaveFile.Projects);
            _projectView.UpdateCreatedProjects();
            _projectView.ProjectTreeItemsSubscribeOnProjects();
            foreach (var project in _projectModel.Projects)
                project.InitDependecies(_dateTriggerConventer);


            _projectController.PrepareProjects();

            foreach (var item in _saveSystemModel.SaveFile.QuestsQueue)
                item.InitDepedencies(_dateTriggerConventer);
            foreach (var item in _saveSystemModel.SaveFile.TriggeredQuests)
                item.InitDepedencies(_dateTriggerConventer);

            _questModel.SetQuestsFromSave(_saveSystemModel.SaveFile.QuestsQueue, _saveSystemModel.SaveFile.TriggeredQuests);

            foreach (var item in _saveSystemModel.SaveFile.Laws)
                item.InitDependecies(_accuralInLoop, _gameInfo, _dateTriggerConventer, _attitudeCheck);

            _lawModel.SetLaws(_saveSystemModel.SaveFile.Laws);
            _unsuccessfulState.SetLawsInTiming(_saveSystemModel.SaveFile.UnsuccesLaws);
            _eventModel.SetEvents(_saveSystemModel.SaveFile.Events);
            _eventModel.SetHandledEvents(_saveSystemModel.SaveFile.HandeledEvents);

            foreach (var item in _eventModel.Events)
                item.InitDependecies(_dateTriggerConventer);

            _serverModel.SetServers(_saveSystemModel.SaveFile.Servers);
            _serverModel.SetServerAction(_saveSystemModel.SaveFile.ServerActions);
            _serverModel.SetServerActionCasesQueue(_saveSystemModel.SaveFile.ServerActionCasesQueue);
            _serverModel.SetServerTriggerQueue(_saveSystemModel.SaveFile.ServersTriggerQueue);

            _cellsCircleMapGeneration.SetCellsFromSave(_saveSystemModel.SaveFile.CellsCircleMapGeneration,
                                                        _saveSystemModel.SaveFile.CellsQueueCircleMapGeneration);
            _cellSquareMapGeneration.SetCellsFromSave(_saveSystemModel.SaveFile.CellsSquareMapGeneration,
                                                        _saveSystemModel.SaveFile.CellsQueueSquareMapGeneration);

            foreach (var item in _serverModel.Servers)
                item.InitDependecies(_dateTriggerConventer);

            _serverModel.InitServers();
            _usersAttitudeModel.InitAttitudesPercent(_saveSystemModel.SaveFile.LoyalUsersPercent, _saveSystemModel.SaveFile.RebelUsersPercent);
            _internetControllModel.SetInternetControll(_saveSystemModel.SaveFile.InternetControll);
            _moneyModel.SetCurrencyInfo(_saveSystemModel.SaveFile.MoneyAmount, _saveSystemModel.SaveFile.MaxMoneyAmount,
                _saveSystemModel.SaveFile.MoneyRestoreAmount, _saveSystemModel.SaveFile.MoneyRestoreTime, _saveSystemModel.SaveFile.MoneyIsRestoring);
            _willModel.SetCurrencyInfo(_saveSystemModel.SaveFile.WillAmount, _saveSystemModel.SaveFile.MaxWillAmount,
             _saveSystemModel.SaveFile.WillRestoreAmount, _saveSystemModel.SaveFile.WillRestoreTime, _saveSystemModel.SaveFile.WillIsRestoring);

            _lawView.SetNotificationStatus(_saveSystemModel.SaveFile.LawViewNotificationSatus);
            _questView.SetNotificationStatus(_saveSystemModel.SaveFile.QuestViewNotificationSatus);
            _eventView.SetNotificationStatus(_saveSystemModel.SaveFile.EventViewNotificationSatus);
            _projectView.SetNotificationStatus(_saveSystemModel.SaveFile.ProjectViewNotificationSatus);
            _moneyModel.StartRestoring();
            _willModel.StartRestoring();
            SpawnObjects();
            yield return null;
        }

        public void CallLoadSave()
        {
            StartCoroutine(LoadSave());
        }

        public void CallSave()
        {
            PlayerPrefs.SetInt("IsHaveSave", 1);
            if (_saveSystemModel.SaveFile == null)
                _saveSystemModel.CreateSaveFile();
            _saveSystemModel.SaveFile.CountUsersInInnerInternet = _internetModel.CountUsersInInnerInternet;
            _saveSystemModel.SaveFile.Projects = _projectView.ProjectTreeItems.Select(item => item.Project).ToList();
            _saveSystemModel.SaveFile.Laws = _lawModel.Laws.ToList();
            _saveSystemModel.SaveFile.Events = _eventModel.Events.ToList();
            _saveSystemModel.SaveFile.HandeledEvents = _eventModel.HandeledEvents.ToList();
            _saveSystemModel.SaveFile.ServerActions = _serverModel.ServerActions.ToList();
            _saveSystemModel.SaveFile.ServerActionCasesQueue = _serverModel.ServerActionCasesQueue.ToList();
            _saveSystemModel.SaveFile.Servers = _serverModel.Servers.ToList();
            _saveSystemModel.SaveFile.ServersTriggerQueue = _serverModel.ServersTriggerQueue.ToList();
            _saveSystemModel.SaveFile.CurrentYear = _timerModel.CurrentYear;
            _saveSystemModel.SaveFile.CurrentMonth = _timerModel.CurrentMonth;
            _saveSystemModel.SaveFile.TimeSinceYearPassed = _timerModel.TimeSinceYearPassed;
            _saveSystemModel.SaveFile.MonthCount = _timerModel.TotalMonthCount;
            _saveSystemModel.SaveFile.UnsuccesLaws = _unsuccessfulState.LawsInTiming.ToList();
            _saveSystemModel.SaveFile.AttitudeResultsSlow = _accuralInLoop.AttitudeResultsSlow.ToList();
            _saveSystemModel.SaveFile.AttitudeResultsNormal = _accuralInLoop.AttitudeResultsNormal.ToList();
            _saveSystemModel.SaveFile.AttitudeResultsFast = _accuralInLoop.AttitudeResultsFast.ToList();

            _saveSystemModel.SaveFile.CellsCircleMapGeneration = _cellsCircleMapGeneration.Cells.ToList();
            _saveSystemModel.SaveFile.CellsQueueCircleMapGeneration = _cellsCircleMapGeneration.CellsQueue.ToList();

            _saveSystemModel.SaveFile.CellsSquareMapGeneration = _cellSquareMapGeneration.Cells.ToList();
            _saveSystemModel.SaveFile.CellsQueueSquareMapGeneration = _cellSquareMapGeneration.CellsQueue.ToList();

            _saveSystemModel.SaveFile.LawViewNotificationSatus = _lawView.NotificationStatus;
            _saveSystemModel.SaveFile.QuestViewNotificationSatus = _questView.NotificationStatus;
            _saveSystemModel.SaveFile.EventViewNotificationSatus = _eventView.NotificationStatus;
            _saveSystemModel.SaveFile.ProjectViewNotificationSatus = _projectView.NotificationStatus;

            _saveSystemModel.SaveFile.LoyalUsersPercent = (int)_usersAttitudeModel.LoyalUsersPercent;
            _saveSystemModel.SaveFile.RebelUsersPercent = (int)_usersAttitudeModel.RebelUsersPercent;
            _saveSystemModel.SaveFile.QuestsQueue = _questModel.QuestsQueue.ToList();
            _saveSystemModel.SaveFile.TriggeredQuests = _questModel.TriggeredQuests.ToList();
            _saveSystemModel.SaveFile.ResultTriggers = _gameInfo.ResultTrigger.ToList();
            _saveSystemModel.SaveFile.InternetControll = _internetControllModel.InternetControll;

            _saveSystemModel.SaveFile.MoneyAmount = _moneyModel.CurrentAmount;
            _saveSystemModel.SaveFile.MaxMoneyAmount = _moneyModel.MaxCurrencyAmount;
            _saveSystemModel.SaveFile.MoneyRestoreAmount = _moneyModel.CurrencyRestoreAmount;
            _saveSystemModel.SaveFile.MoneyRestoreTime = _moneyModel.CurrencyRestoreTimeCofficient;
            _saveSystemModel.SaveFile.MoneyIsRestoring = _moneyModel.IsRestoring;

            _saveSystemModel.SaveFile.WillAmount = _willModel.CurrentAmount;
            _saveSystemModel.SaveFile.MaxWillAmount = _willModel.MaxCurrencyAmount;
            _saveSystemModel.SaveFile.WillRestoreAmount = _willModel.CurrencyRestoreAmount;
            _saveSystemModel.SaveFile.WillRestoreTime = _willModel.CurrencyRestoreTimeCofficient;
            _saveSystemModel.SaveFile.WillIsRestoring = _willModel.IsRestoring;

            _saveSystemModel.SaveFile.NodeObjectInfos = _internetModel.GetNodeObjectInfos();
            _saveSystemModel.SaveFile.ServerObjectsInfo = _internetModel.GetServerObjectInfos();
            _saveSystemModel.SaveFile.UserObjectsInfo = _internetModel.GetUserObjectInfos();

            _saveSystemModel.Save();
        }

        private void SpawnObjects()
        {
            foreach(var item in _saveSystemModel.SaveFile.ServerObjectsInfo)
            {
                Debug.Log("item " + item.ObjectLocationType);
                if(_serverModel.TryGetServerByID(item.Server.ServerID, out Server serverData) && item.ObjectLocationType == InternetObjects.ObjectLocationType.Inner)
                {
                    if (serverData.Type == Data.GameData.ServerPerInternetType.Inner)
                    {
                        ServerObject server = _cellsCircleMapGeneration.GenerateInPosition(_serverObjectPrefab, item.Position);
                        server.gameObject.name = item.Server.ServerID;
                        server.InitServer(_gameState, _checkUI, serverData, _serverController);
                        server.SetNotificationStatus(item.NotificationStatus);
                        server.Offset = item.Offset;
                        server.ObjectLocationType = item.ObjectLocationType;
                        _internetModel.DistrebuteInternetObject(server);
                        server.ChangeSprite();
                    }
                }
                else
                {
                    ServerObject server = _cellSquareMapGeneration.GenerateInPosition(_serverObjectPrefab, item.Position, Vector2.zero);
                    server.gameObject.name = item.Server.ServerID;
                    server.InitServer(_gameState, _checkUI, item.Server, _serverController);
                    server.SetNotificationStatus(item.NotificationStatus);
                    server.Offset = item.Offset;
                    server.ObjectLocationType = item.ObjectLocationType;
                    _internetModel.DistrebuteInternetObject(server);
                    server.ChangeSprite();
                    _outerServerLineService.CreateServerObjectLine(server);
                }
            }

            foreach (var item in _saveSystemModel.SaveFile.UserObjectsInfo)
            {
                if (item.ObjectLocationType == InternetObjects.ObjectLocationType.Inner)
                {
                    UserObject userObject = _cellsCircleMapGeneration.GenerateInPosition(_userObjectPrefab, item.Position);
                    userObject.ChangeColorByAttitude(item.AttitudeTypes);
                    _internetModel.DistrebuteInternetObject(userObject);
                }
                else
                {
                    UserObject userObject = _cellSquareMapGeneration.GenerateInPosition(_userObjectPrefab, item.Position, item.Offset);
                    userObject.Offset = item.Offset;
                    userObject.ObjectLocationType = item.ObjectLocationType;
                    userObject.ChangeColorByAttitude(item.AttitudeTypes);
                    _internetModel.DistrebuteInternetObject(userObject);
                }
            }

            foreach (var item in _saveSystemModel.SaveFile.NodeObjectInfos)
            {
                NodeObject node = _roundGeneration.GenerateInPosition(_nodeObjectPrefab, item.Position);
                _internetModel.DistrebuteInternetObject(node);
            }
        }

    }
}