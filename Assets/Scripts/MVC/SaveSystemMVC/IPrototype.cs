﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.SaveSystemMVC
{
    public interface IPrototype
    {
        void SaveData();
    }
}