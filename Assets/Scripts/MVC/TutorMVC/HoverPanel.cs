﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MVC.TutorMVC
{
    public class HoverPanel : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private RectTransform _targetRectTransform;

        private Material imageMaterial;

        private void Start()
        {
            // Получаем Material изображения, чтобы внести в него изменения
            imageMaterial = _image.material;

            //// Устанавливаем режим рисования в Stencil Buffer
            //imageMaterial.SetInt("_StencilComp", (int)UnityEngine.Rendering.CompareFunction.Equal);
            //imageMaterial.SetInt("_Stencil", 1);
            //imageMaterial.SetInt("_StencilOp", (int)UnityEngine.Rendering.StencilOp.Replace);
            //imageMaterial.SetInt("_StencilWriteMask", 1);

            // Устанавливаем RectTransform для маски (дырки)
            //imageMaterial.SetVector("_MaskRect", new Vector4(
            //    _targetRectTransform.rect.x,
            //    _targetRectTransform.rect.y,
            //    _targetRectTransform.rect.width,
            //    _targetRectTransform.rect.height
            //));

            // Очищаем Stencil Buffer
            //imageMaterial.SetInt("_StencilOp", (int)UnityEngine.Rendering.StencilOp.Zero);

            // Применяем изменения к Material
            _image.SetMaterialDirty();
        }

    }
}