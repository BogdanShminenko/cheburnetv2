﻿using Assets.Scripts.GameStates;
using Assets.Scripts.MVC.ProjectMVC;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MVC.GameResultMVC
{
    public class GameResultView : MonoBehaviour
    {
        [SerializeField] private Sprite _winImage;
        [SerializeField] private Sprite _loseImage;
        [SerializeField] private GameObject _panel;
        [SerializeField] private TMP_Text _cloudWithSpeachText;
        [SerializeField] private TMP_Text _headerText;
        [SerializeField] private TMP_Text _text;
        [SerializeField] private Image _stagePanel;
        
        [Header("Events")]
        [SerializeField] private UnityEvent _onWinEvent;
        [SerializeField] private UnityEvent _onLoseEvent;
        
        [Inject] private GameState _gameState;
        [Inject] private ProjectView _projectView;

        private bool _isTriggered;

        private void Display(string speachText, string headerText , string text)
        {
            _cloudWithSpeachText.text = speachText;
            _headerText.text = headerText;
            _text.text = text;
        }

        
        public void OpenWin(string speachText, string headerText, string text)
        {
            if (_isTriggered)
            {
                return;
            }

            _isTriggered = true;
            Display(speachText, headerText, text);
            _stagePanel.sprite = _winImage;
            OpenPanel();

            if (_onWinEvent != null)
            {
                _onWinEvent.Invoke();
            }
        }

        public void OpenLose(string speachText, string headerText, string text)
        {
            if (_isTriggered)
            {
                return;
            }

            _isTriggered = true;
            Display(speachText, headerText, text);
            _stagePanel.sprite = _loseImage;
            OpenPanel();

            if (_onLoseEvent != null)
            {
                _onLoseEvent.Invoke();
            }
        }

        private void OpenPanel()
        {
            _panel.SetActive(true);
            if (_gameState.CurrentGameState == GameStates.GameStates.Projects)
            {
                _projectView.CloseTree();
            }
            _gameState.ChangeGameState(GameStates.GameStates.EndGame);
        }
    }
}