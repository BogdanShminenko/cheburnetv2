﻿using Assets.Scripts.MVC.InternetControllMVC;
using Assets.Scripts.MVC.InternetMVC;
using Assets.Scripts.MVC.PauseMenuMVC;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.MVC.UsersAttitude;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Assets.Scripts.MVC.GameResultMVC
{
    public class GameResultController : MonoBehaviour
    {
        [Inject] private InternetControllModel _internetControllModel;
        [Inject] private TimerModel _timerModel;
        [Inject] private UsersAttitudeModel _usersAttitudeModel;
        [Inject] private GameResultView _gameResultView;
        [Inject] private PauseMenuController _pauseMenuController;

        public void EndGame()
        {
            PlayerPrefs.DeleteKey("IsHaveSave");
            SceneManager.LoadScene(0);
        }

        public void CheckToResult()
        {
            if(_internetControllModel.InternetControll >= 100)
            {
                _pauseMenuController.PauseGame();
                _gameResultView.OpenWin("Хорошая работа. Благодаря нам с тобой, в интернете теперь порядок, никто глупостей не пишет. Получишь премию.",
                    "Начальство благодарит тебя", "Почетная грамота и 3 дополнительных дня отпуска");
            }
            else if(new System.DateTime((int)_timerModel.CurrentYear, (int)_timerModel.CurrentMonth,1) >= new System.DateTime(2025,1,1))
            {
                _pauseMenuController.PauseGame();
                _gameResultView.OpenLose("В интернете все пишут что хотят и возмущаются, отвратительно!",
                        "Не оправдал доверия", "Уволить! С занесением в личное дело");
            }
            else if(_usersAttitudeModel.RebelUsersPercent >= 100)
            {
                _pauseMenuController.PauseGame();
                _gameResultView.OpenLose("В интернете все пишут что хотят и возмущаются, отвратительно!",
                        "Не оправдал доверия", "Уволить! С занесением в личное дело");
            }
        }

    }
}