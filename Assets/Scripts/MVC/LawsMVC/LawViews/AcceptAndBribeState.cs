﻿using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.Utilities;
using System.Collections;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.LawsMVC.LawViews
{
    public class AcceptAndBribeState : MonoBehaviour, ILawState
    {
        [SerializeField] private GameObject _panel;
        [SerializeField] private TMP_Text _acceptPriceText;
        [SerializeField] private TMP_Text _chanceText;
        [SerializeField] private TMP_Text _bribePriceText;
        [SerializeField] private TMP_Text _increaseChanceText;

        [Inject] private WillModel _willModel;
        [Inject] private MoneyModel _moneyModel;
        private Law _currentLaw;
        private RandomSystem _randomSystem = new RandomSystem();


        public void EnterInState(Law law)
        {
            _currentLaw = law;
            _acceptPriceText.text =  $"Воля: {_currentLaw.Price1Will}";
            _chanceText.text = $"Шанс успеха: {_currentLaw.Chance1}%";
            _bribePriceText.text = $"$ {_currentLaw.Price3Money}";
            _increaseChanceText.text = $"+ {_currentLaw.Chance3}% к шансу успеха";
            _panel.SetActive(true);
        }

        public void ExitFromState()
        {
            _panel.SetActive(false);
        }

        public void Bribe()
        {
            if (_moneyModel.CurrentAmount >= _currentLaw.Price3Money && !_currentLaw.IsBribed)
            {
                _moneyModel.TrySpend((uint)_currentLaw.Price3Money);
                _currentLaw.PribeLaw(_currentLaw.Chance3);
                _chanceText.text = $"Шанс успеха: {_currentLaw.Chance3}%";
            }
        }

        public void Accept()
        {
            if(_willModel.CurrentAmount >= _currentLaw.Price1Will)
            {
                _willModel.TrySpend((uint)_currentLaw.Price1Will);
                if (_randomSystem.CheckRandom(_currentLaw.Chance1))
                {
                    _currentLaw.ChangeState(LawState.Accepted);
                }
                else
                {
                    _currentLaw.ChangeState(LawState.Unsucces);
                }
            }
        }
    }
}