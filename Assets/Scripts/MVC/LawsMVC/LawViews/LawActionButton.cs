﻿using Assets.Scripts.Accrual;
using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.MVC.UsersAttitude;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MVC.LawsMVC.LawViews
{
    public class LawActionButton : MonoBehaviour , IPointerClickHandler
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public AK.Wwise.Event _clickEvent;
            public AK.Wwise.Event _disabledEvent;
        }
        
        [SerializeField] private Sprite _activeImage;
        [SerializeField] private Sprite _inactiveImage;
        [SerializeField] private Image _image;
        [SerializeField] private TMP_Text _priceText;
        [SerializeField] private TMP_Text _chanceText;
        [Inject] private WillModel _willModel;
        [Inject] private MoneyModel _moneyModel;
        public event Action OnClick;
        private CurrencyType _currencyType;
        private int _price;
        [SerializeField] private WwiseSound _wwiseSound;
        
        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick?.Invoke();

            if (_wwiseSound._clickEvent.IsValid() && _image.sprite == _activeImage)
            {
                _wwiseSound._clickEvent.Post(gameObject);
            } else if (_wwiseSound._disabledEvent.IsValid() && _image.sprite == _inactiveImage)
            {
                _wwiseSound._disabledEvent.Post(gameObject);
            }
        }

        public void OnButton()
        {
            gameObject.SetActive(true);
        }

        public void OffButton()
        {
            gameObject.SetActive(false);
        }

        public void Init(int price, string chance, CurrencyType currencyType)
        {
            if(currencyType == CurrencyType.Will)
            {
                if(_willModel.CurrentAmount >= price)
                {
                    _image.sprite = _activeImage;
                }
                else
                {
                    _image.sprite = _inactiveImage;
                }
            }
            else if (currencyType == CurrencyType.Money)
            {
                if (_moneyModel.CurrentAmount >= price)
                {
                    _image.sprite = _activeImage;
                }
                else
                {
                    _image.sprite = _inactiveImage;
                }
            }
            _moneyModel.OnCurrencyUpdate += UpdateButtonsStatus;
            _willModel.OnCurrencyUpdate += UpdateButtonsStatus;
            _price = price;
            _currencyType = currencyType;
            _priceText.text = price.ToString();
            _chanceText.text = chance;
        }

        private void UpdateButtonsStatus()
        {
            if (_currencyType == CurrencyType.Will)
            {
                if (_willModel.CurrentAmount >= _price)
                {
                    _image.sprite = _activeImage;
                }
                else
                {
                    _image.sprite = _inactiveImage;
                }
            }
            else if (_currencyType == CurrencyType.Money)
            {
                if (_moneyModel.CurrentAmount >= _price)
                {
                    _image.sprite = _activeImage;
                }
                else
                {
                    _image.sprite = _inactiveImage;
                }
            }
        }

        public void UpdateChance(string chance)
        {
            _chanceText.text = chance;
        }
    }
}