﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.LawsMVC.LawViews
{
    public interface ILawState
    {
        void EnterInState(Law law);
        void ExitFromState();
    }
}
