﻿using Assets.Scripts.Accrual;
using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.Utilities;
using System.Collections;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Assets.Scripts.MVC.LawsMVC.LawViews
{
    public class LawActionButtonsState : MonoBehaviour, ILawState
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public AK.Wwise.Event _successEvent;
            public AK.Wwise.Event _unsuccessEvent;
        }
        
        [SerializeField] private GameObject _panel;
        [SerializeField] private LawActionButton _acceptButton;
        [SerializeField] private LawActionButton _pushButton;
        [SerializeField] private LawActionButton _bribeButton;

        [SerializeField] private WwiseSound _wwiseSound;
        
        [Inject] private WillModel _willModel;
        [Inject] private MoneyModel _moneyModel;
        private Law _currentLaw;
        private RandomSystem _randomSystem = new RandomSystem();

        private void OnEnable()
        {
            _acceptButton.OnClick += Accept;
            _pushButton.OnClick += Push;
            _bribeButton.OnClick += Bribe;
        }

        private void OnDisable()
        {
            _acceptButton.OnClick -= Accept;
            _pushButton.OnClick -= Push;
            _bribeButton.OnClick -= Bribe;
        }

        public void EnterInState(Law law)
        {
            _currentLaw = law;
            Debug.Log("_currentLaw " + _currentLaw.LawID + " _currentLaw.Chance1 " + _currentLaw.Chance1);
            if (_currentLaw.Price1Will != 0)
            {
                _acceptButton.Init(_currentLaw.Price1Will, $"Шанс успеха: {_currentLaw.Chance1}%", CurrencyType.Will);
                _acceptButton.OnButton();
            }
            else
                _acceptButton.OffButton();

            if (_currentLaw.Price2Will != 0)
            {
                _pushButton.Init(_currentLaw.Price2Will, $"Шанс успеха: {_currentLaw.Chance2}%", CurrencyType.Will);
                _pushButton.OnButton();
            }
            else
                _pushButton.OffButton();

            if (_currentLaw.Price3Money != 0)
            {
                _bribeButton.Init(_currentLaw.Price3Money, $"+{_currentLaw.Chance3}% К шансу успеха", CurrencyType.Money);
                _bribeButton.OnButton();
            }
            else
                _bribeButton.OffButton();

            _panel.SetActive(true);
        }

        public void ExitFromState()
        {
            _panel.SetActive(false);

        }

        private void Bribe()
        {
            if (_moneyModel.CurrentAmount >= _currentLaw.Price3Money && !_currentLaw.IsBribed)
            {
                _moneyModel.TrySpend((uint)_currentLaw.Price3Money);
                _currentLaw.PribeLaw(_currentLaw.Chance3);
                _acceptButton.UpdateChance($"Шанс успеха: {_currentLaw.Chance1}%");
                _bribeButton.OffButton();
            }
        }

        private void Push()
        {
            if (_willModel.CurrentAmount >= _currentLaw.Price2Will)
            {
                _willModel.TrySpend((uint)_currentLaw.Price2Will);
                if (_randomSystem.CheckRandom(_currentLaw.Chance2))
                {
                    _currentLaw.ChangeState(LawState.Accepted);
                    _wwiseSound._successEvent.Post(gameObject);
                }
                else
                {
                    _currentLaw.ChangeState(LawState.Unsucces);
                    _wwiseSound._unsuccessEvent.Post(gameObject);
                }
            }
        }

        private void Accept()
        {
            if (_willModel.CurrentAmount >= _currentLaw.Price1Will)
            {
                _willModel.TrySpend((uint)_currentLaw.Price1Will);
                if (_randomSystem.CheckRandom(_currentLaw.Chance1))
                {
                    _currentLaw.ChangeState(LawState.Accepted);
                    _wwiseSound._successEvent.Post(gameObject);
                }
                else
                {
                    _currentLaw.ChangeState(LawState.Unsucces);
                    _wwiseSound._unsuccessEvent.Post(gameObject);
                }
            }
        }
    }
}