﻿using Assets.Scripts.GameStates;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MVC.LawsMVC.LawViews
{
    public class LawPanel : MonoBehaviour
    {
        [SerializeField] private GameObject[] _panels;
        [SerializeField] private GameObject _panel;
        [SerializeField] private Image _iconStatus;
        [SerializeField] private Image _icon;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _description;
        [SerializeField] private Sprite _canAcceptSprite;
        [SerializeField] private Sprite _lockedSprite;
        [SerializeField] private Sprite _acceptedSprite;

        private ILawState _currentLawState;

        [Inject] private AcceptAndBribeState _acceptAndBribeState;
        [Inject] private AcceptAndPushState _acceptAndPushState;
        [Inject] private AcceptedState _acceptedState;
        [Inject] private UnsuccessfulState _unsuccessfulState;
        [Inject] private LawActionButtonsState _lawActionButtonsState;
        [Inject] private GameState _gameState;

        private Law _currentLaw;

        public void SetLaw(Law law)
        {
            _currentLaw = law;
            _title.text = law.Title;
            _description.text = law.Description + "\n" + law.TextExpected;
            _icon.sprite = law.IconSprite;
            _icon.color = Color.white;
            ChangeIconByLawState();
            _currentLaw.OnChangeState += ChangeStateHandler;
        }

        private void ChangeIconByLawState()
        {
            if (_currentLaw.State == LawState.Accept)
            {
                _iconStatus.sprite = _canAcceptSprite;
            }
            else if (_currentLaw.State == LawState.Locked)
            {
                _iconStatus.sprite = _lockedSprite;
            }
            else if (_currentLaw.State == LawState.Accepted)
            {
                _iconStatus.sprite = _acceptedSprite;
            }
            else if (_currentLaw.State == LawState.Unsucces)
            {
                _iconStatus.sprite = _canAcceptSprite;
            }
        }

        public void OpePanel()
        {
            ChangeStateHandler(_currentLaw.State);
            _gameState.ChangeGameState(GameStates.GameStates.EventPanel);
            _panel.SetActive(true);
        }

        public void ClosePanel()
        {
            _panel.SetActive(false);
            _currentLaw.OnChangeState -= ChangeStateHandler;
            _gameState.ChangeGameState(_gameState.PreviousGameStates);
        }

        public void SetState(ILawState lawState)
        {
            if(_currentLawState != null)
                _currentLawState.ExitFromState();

            _currentLawState = lawState;
            _currentLawState.EnterInState(_currentLaw);
        }

        private void OffStatesPanel()
        {
            foreach(var item in _panels)
                item.SetActive(false);
        }

        public void ChangeStateHandler(LawState lawState)
        {
            OffStatesPanel();
            if (lawState == LawState.Accept)
            {
                SetState(_lawActionButtonsState);

            }else if (lawState == LawState.Accepted)
            {
                SetState(_acceptedState);
            }else if(lawState == LawState.Unsucces)
            {
                SetState(_unsuccessfulState);
            }
            ChangeIconByLawState();
        }
    }
}