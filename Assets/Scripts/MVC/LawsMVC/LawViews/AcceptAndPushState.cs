﻿using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.Utilities;
using System.Collections;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.LawsMVC.LawViews
{
    public class AcceptAndPushState : MonoBehaviour, ILawState
    {
        [SerializeField] private GameObject _panel;
        [SerializeField] private TMP_Text _acceptPriceText;
        [SerializeField] private TMP_Text _chanceText;
        [SerializeField] private TMP_Text _pushPriceText;
        [SerializeField] private TMP_Text _pushChanceText;

        [Inject] private WillModel _willModel;
        private Law _currentLaw;
        private RandomSystem _randomSystem = new RandomSystem();

        public void EnterInState(Law law)
        {
            _currentLaw = law;
            _acceptPriceText.text = $"Воля: {_currentLaw.Price1Will}";
            _chanceText.text = $"Шанс успеха: {_currentLaw.Chance1}%";
            _pushPriceText.text = $"Воля: {_currentLaw.Price2Will}";
            _pushChanceText.text = $"Шанс успеха: {_currentLaw.Chance2}%";
            _panel.SetActive(true);
        }

        public void ExitFromState()
        {
            _panel.SetActive(false);
        }

        public void Push()
        {
            if (_willModel.CurrentAmount >= _currentLaw.Price2Will)
            {
                _willModel.TrySpend((uint)_currentLaw.Price2Will);
                if (_randomSystem.CheckRandom(_currentLaw.Chance2))
                {
                    _currentLaw.ChangeState(LawState.Accepted);
                }
                else
                {
                    _currentLaw.ChangeState(LawState.Unsucces);
                }
            }
        }

        public void Accept()
        {
            if (_willModel.CurrentAmount >= _currentLaw.Price1Will)
            {
                _willModel.TrySpend((uint)_currentLaw.Price1Will);
                if (_randomSystem.CheckRandom(_currentLaw.Chance1))
                {
                    _currentLaw.ChangeState(LawState.Accepted);
                }
                else
                {
                    _currentLaw.ChangeState(LawState.Unsucces);
                }
            }
        }
    }
}