﻿using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Event = AK.Wwise.Event;

namespace Assets.Scripts.MVC.LawsMVC.LawViews
{
    public class UnsuccessfulState : MonoBehaviour, ILawState
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public Event _progressPanelHidedEvent;
        }
        
        [SerializeField] private GameObject _panel;
        [SerializeField] private TMP_Text _text;
        [SerializeField] private Image _loadBar;
        [SerializeField] private WwiseSound _wwiseSound;
        
        [Inject] private GameSettingsData _gameSettingsData;
        [Inject] private LawView _lawView;

        private List<Law> _lawsInTiming = new List<Law>();
        private Law _currentLaw;
        private Coroutine _holdTimeForAcceptLawCoroutine;

        public IEnumerable<Law> LawsInTiming => _lawsInTiming;

        public void SetLawsInTiming(List<Law> laws)
        {
            _lawsInTiming = laws;
        }

        private void Start()
        {
            _holdTimeForAcceptLawCoroutine = StartCoroutine(HoldTimeForAcceptLaw());
        }

        public void EnterInState(Law law)
        {
            _currentLaw = law;
            _loadBar.fillAmount = 1;
            if (!_lawsInTiming.Contains(law))
            {
                _lawsInTiming.Add(_currentLaw);
            }
            _text.text = $"Вы можете попытаться принять его еще раз через {_currentLaw.HoldTimeRemeaning} секунд";
            _panel.SetActive(true);
        }

        public void Pause()
        {
            if (_holdTimeForAcceptLawCoroutine != null)
                StopCoroutine(_holdTimeForAcceptLawCoroutine);
        }

        public void Continue()
        {
            _holdTimeForAcceptLawCoroutine = StartCoroutine(HoldTimeForAcceptLaw());
        }

       

        private IEnumerator HoldTimeForAcceptLaw()
        {
            while (true)
            {
                for (int i = 0; i < _lawsInTiming.Count; i++)
                {
                    _lawsInTiming[i].HoldTimeRemeaning -= 0.01f;
                    if (_lawsInTiming[i] == _currentLaw)
                    {
                        _loadBar.fillAmount = (float)((float)_currentLaw.HoldTimeRemeaning / (float)_currentLaw.HoldTime);
                        int month = (int)System.Math.Ceiling(_currentLaw.HoldTimeRemeaning);
                        _text.text = $"Вы можете попытаться еще раз\r\nчерез {month} {MonthConverter.GetMonthNumeral(month)}";
                    }
                    
                    if (_lawsInTiming[i].HoldTimeRemeaning <= 0)
                    {
                        if (_lawsInTiming[i] == _currentLaw)
                        {
                            _lawsInTiming[i].ChangeState(LawState.Accept);
                            _wwiseSound._progressPanelHidedEvent.Post(gameObject);
                        }
                        else
                        {
                            _lawsInTiming[i].ChangeStateWithoutNotify(LawState.Accept);
                        }
                        _lawView.NotifyOnChangeLaw();
                        _lawsInTiming.Remove(_lawsInTiming[i]);
                    }
                }
                
                yield return new WaitForSeconds(_gameSettingsData.Tick / 100f);
            }
        }

        public void ExitFromState()
        {
            _panel.SetActive(false);
        }
    }
}