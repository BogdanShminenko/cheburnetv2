﻿using Assets.Scripts.Utilities;
using System.Collections;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.LawsMVC.LawViews
{
    public class AcceptedState : MonoBehaviour, ILawState
    {
        [SerializeField] private GameObject _panel;
        [SerializeField] private TMP_Text _text;
        [SerializeField] private TMP_Text _resultText;

        [Inject] private AttitudeOperationToStringConverter _attitudeOperationToStringConverter;
        private Law _currentLaw;

        public void EnterInState(Law law)
        {
            _currentLaw = law;
            _text.text = $"Закон успешно принят";
            _resultText.text = law.TextSuccess;
            _panel.SetActive(true);
        }

        public void ExitFromState()
        {
            _panel.SetActive(false);
        }
    }
}