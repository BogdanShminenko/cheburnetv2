﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Data.GameData;
using Assets.Scripts.Game;
using Assets.Scripts.MVC.ProjectMVC;
using Assets.Scripts.MVC.UsersAttitude;
using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.LawsMVC
{
    public class LawModel : MonoBehaviour
    {
        private List<Law> _laws = new List<Law>();

        public event System.Action<Law> OnSelectedLaw;
        public event System.Action<IEnumerable<Law>> OnInnitedLaws;

        [Inject] private DateTriggerConventer _dateTriggerConventer;
        [Inject] private GameInfo _gameInfo;
        [Inject] private AccuralInLoop _accuralInLoop;
        [Inject] private AttitudeCheck _attitudeCheck;

        public IEnumerable<Law> Laws { get {  return _laws; } }

        public void Init(List<LawData> lawDatas)
        {
            foreach(var lawData in lawDatas)
            {
                Law law = new Law(_accuralInLoop,_gameInfo,_dateTriggerConventer, _attitudeCheck);
                law.InitLaw(lawData);
                _laws.Add(law);
            }
            OnInnitedLaws?.Invoke(_laws);
        }

        public void SetLaws(List<Law> laws)
        {
            _laws = laws;
            OnInnitedLaws?.Invoke(_laws);
            foreach (var law in _laws)
                law.ChangeState(law.State);
        }

        public void SelectLaw(Law law)
        {
            OnSelectedLaw?.Invoke(law);
        }

        public bool TriggerLaws(int currentYear, int currentMounth)
        {
            for (int i = 0; i < _laws.Count; i++)
            {
                if (!_laws[i].IsTriggered && _laws[i].TryTriggerLawByAttitudeAndDate(currentYear, currentMounth) && _laws[i].CheckTriggerByTriggerSignal())
                {
                    _laws[i].ChangeState(LawState.Accept);
                    _laws[i].IsTriggered = true;
                    return true;
                }
            }
            return false;
        }


    }
}