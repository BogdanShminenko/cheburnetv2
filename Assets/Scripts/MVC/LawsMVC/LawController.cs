﻿using Assets.Scripts.Data.GameData.Configs;
using Assets.Scripts.MVC.TimerMVC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.LawsMVC
{
    public class LawController : MonoBehaviour
    {
        [Inject] private TimerModel _timerModel;
        [Inject] private LawModel _lawModel;
        [Inject] private LawConfig _lawConfig;
        [Inject] private LawView _lawView;
        [Inject] private WillModel _willModel;

        private void Awake()
        {
            _lawModel.OnInnitedLaws += InitedLawsHandler;
        }

        public void InitLaws()
        {
            _lawModel.Init(_lawConfig.Laws);
        }

        private void InitedLawsHandler(IEnumerable<Law> laws)
        {
            foreach (var item in laws)
                item.OnChangeState += HandleLawChangeState;
        }

        private void HandleLawChangeState(LawState lawState)
        {
            if(lawState == LawState.Accepted)
            {
                _willModel.IncreaseMaxWill();
                _willModel.IncreaseWillRestoreTime();
            }
        }

        public void OpenLawsPanel()
        {
            _lawView.OpenLawsPanel();
        }

        public void CloseLawsPanel()
        {
            _lawView.CloseLawsPanel();
        }

        public void CallTriggerLaws()
        {
            if (_lawModel.TriggerLaws((int)_timerModel.CurrentYear, (int)_timerModel.CurrentMonth))
            {
                _lawView.NotifyOnChangeLaw();
            }
        }

    }
}