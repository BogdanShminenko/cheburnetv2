﻿using Assets.Scripts.GameStates;
using Assets.Scripts.MVC.LawsMVC.LawViews;
using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.LawsMVC
{
    public class LawView : MonoBehaviour
    {
        [SerializeField] private GameObject _notificationPanel;
        [SerializeField] private Transform _parent;
        [SerializeField] private GameObject _lawsPanel;

        [Inject] private LawUIItem _item;
        [Inject] private LawPanel _lawPanel;
        [Inject] private LawModel _model;
        [Inject] private AttitudeOperationToStringConverter _attitudeOperationToStringConverter;
        [Inject] private GameState _gameState;
        [Inject] private ButtonsMenu _buttonsMenu;

        public bool NotificationStatus => _notificationPanel.activeSelf;


        private void OnEnable()
        {
            _model.OnSelectedLaw += CallOpenLawPanel;
            _model.OnInnitedLaws += InitLawItems;
        }

        public void SetNotificationStatus(bool notificationStatus)
        {
            _notificationPanel.SetActive(notificationStatus);
        }

        public void OpenLawsPanel()
        {
            _gameState.ChangeGameState(GameStates.GameStates.Laws);
            _notificationPanel.SetActive(false);
            _lawsPanel.SetActive(true);
        }

        public void CloseLawsPanel()
        {
            _gameState.ChangeGameState(GameStates.GameStates.InternetMap);
            _lawsPanel.SetActive(false);
            _buttonsMenu.ResetButtonsWithOutOffPanels();
        }

        public void NotifyOnChangeLaw()
        {
            if(!_lawsPanel.activeSelf)
                _notificationPanel.SetActive(true);
        }

        private void CallOpenLawPanel(Law law)
        {
            if (law.State == LawState.Locked)
                return;
            _lawPanel.SetLaw(law);
            _lawPanel.OpePanel();
        }

        public void InitLawItems(IEnumerable<Law> laws)
        {
            foreach(var law in laws)
            {
                LawUIItem item = Instantiate(_item, _parent);
                item.Init(_attitudeOperationToStringConverter, _model);
                item.InitLawUI(law);
                item.ChangeLawState(law.State);
            }
        }
    }
}