﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Data.GameData;
using Assets.Scripts.Game;
using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.MVC.ProjectMVC;
using Assets.Scripts.MVC.UsersAttitude;
using Assets.Scripts.TriggersAndOperationsTypes;
using Assets.Scripts.Utilities;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.LawsMVC
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum LawState
    {
        Accept = 0,
        Accepted = 1,
        Unsucces = 2,
        Locked = 3
    }

    [Serializable]

    public class Law
    {
        [field: SerializeField] public LawState State { get; set; } = LawState.Locked;
        public string LawID { get; set; }
        public List<string> TriggersSignal = new List<string>();
        public string Title { get; set; }
        public string Description { get; set; }
        public string TextExpected { get; set; }
        public int Price1Will { get; set; }
        public int Chance1 { get; set; }
        public int Price2Will { get; set; }
        public int Chance2 { get; set; }
        public int Price3Money { get; set; }
        public int Chance3 { get; set; }
        public string TextFail { get; set; }
        public int HoldTime { get; set; }
        public string TextSuccess { get; set; }
        public string Icon { get; set; }
        public AttitudeResult ResultRebel { get; set; }
        public AttitudeResult ResultLoyal { get; set; }
        public AttitudeResult ResultControl { get; set; }
        public string ResultTrigger { get; set; }
        [JsonIgnore] public Sprite IconSprite { get; set; }

        public DateTrigger DateTrigger;
        public AttitudeOperation TriggerRebelOver { get; set; }

        private DateTriggerConventer _dateTriggerConventer;
        private AttitudeCheck _attitudeCheck;
        private GameInfo _gameInfo;
        private AccuralInLoop _accuralInLoop;
        public float HoldTimeRemeaning { get; set; }
        public float HoldTimeRemeaningCount { get; set; }
        public bool IsBribed { get; set; }
        public bool IsActiveAction { get; set; }
        public event Action<LawState> OnChangeState;
        public bool IsTriggered { get; set; }

        public Law(AccuralInLoop accuralInLoop,GameInfo gameInfo,DateTriggerConventer dateTriggerConventer, AttitudeCheck attitudeCheck)
        {
            _accuralInLoop = accuralInLoop;
            _gameInfo = gameInfo;
            _dateTriggerConventer = dateTriggerConventer;
            _attitudeCheck = attitudeCheck;
        }

        public void InitDependecies(AccuralInLoop accuralInLoop, GameInfo gameInfo, DateTriggerConventer dateTriggerConventer, AttitudeCheck attitudeCheck)
        {
            _accuralInLoop = accuralInLoop;
            _gameInfo = gameInfo;
            _dateTriggerConventer = dateTriggerConventer;
            _attitudeCheck = attitudeCheck;
        }

        public void InitLaw(LawData data)
        {
            LawID = data.LawID;

            Title = data.Title;
            TextExpected = data.TextExpected;
            Description = data.Description;
            Price1Will = data.Price1Will;
            Chance1 = data.Chance1;
            Price2Will = data.Price2Will;
            Chance2 = data.Chance2;
            Price3Money = data.Price3Money;
            Chance3 = data.Chance3;
            TextFail = data.TextFail;
            TextSuccess = data.TextSuccess;
            ResultTrigger = data.ResultTrigger;
            HoldTime = data.HoldTime;
            Icon = data.Icon;
            if(data.ResultLoyal != 0)
            {
                if (data.ResultLoyal > 0)
                    ResultLoyal = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultLoyalSpeed), data.ResultLoyal, Accrual.CurrencyType.Loyal, Accrual.OperationType.Increase);
                else
                    ResultLoyal = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultLoyalSpeed), data.ResultLoyal, Accrual.CurrencyType.Loyal, Accrual.OperationType.Decrease);
            }
            if (data.ResultRebel != 0)
            {
                if (data.ResultRebel >= 0)
                    ResultRebel = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultRebelSpeed), data.ResultRebel, Accrual.CurrencyType.Rebel, Accrual.OperationType.Increase);
                else
                    ResultRebel = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultRebelSpeed), data.ResultRebel, Accrual.CurrencyType.Rebel, Accrual.OperationType.Decrease);
            }
            if (data.ResultControl != 0)
            {
                if (data.ResultControl >= 0)
                    ResultControl = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultControlSpeed), data.ResultControl, Accrual.CurrencyType.InternetControl, Accrual.OperationType.Increase);
                else
                    ResultControl = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultControlSpeed), data.ResultControl, Accrual.CurrencyType.InternetControl, Accrual.OperationType.Decrease);
            }

            if (_dateTriggerConventer.TryConvertStringToDateTrigger(data.TriggerDate, out DateTrigger dateTrigger))
            {
                DateTrigger = dateTrigger;
            }
            if (data.TriggerRebelOver != 0)
            {
                TriggerRebelOver = new AttitudeOperation(Accrual.OperationType.Increase, Accrual.CurrencyType.Rebel,data.TriggerRebelOver);
            }

            if (Icon != null && Icon.Length > 0)
                IconSprite = Resources.Load<Sprite>($"Sprites/Laws/{Icon.Replace(".png", "")}");

            if (data.TriggerSignal == "" || data.TriggerSignal.Length == 0)
            {
                TriggersSignal.Add("null");
            }
            else
            {
                string[] triggers = data.TriggerSignal.Split(',');
                TriggersSignal = triggers.ToList();
            }
        }

        public void LoadIcon()
        {
            if (Icon != null && Icon.Length > 0)
                IconSprite = Resources.Load<Sprite>($"Sprites/Laws/{Icon.Replace(".png", "")}");
        }

        private ValueCangeType GetValueChangeBySymbol(string symbol)
        {
            if (symbol == "M")
                return ValueCangeType.Moment;
            else if (symbol == "S")
                return ValueCangeType.Slow;
            else if (symbol == "N")
                return ValueCangeType.Normal;
            return ValueCangeType.Fast;
        }

        public void ChangeStateWithoutNotify(LawState state)
        {
            State = state;
            if (State == LawState.Accepted)
            {
                _gameInfo.AddTrigger(ResultTrigger);
                if (ResultRebel != null)
                    _accuralInLoop.HandleAttitudeResult(ResultRebel);
                if (ResultLoyal != null)
                    _accuralInLoop.HandleAttitudeResult(ResultLoyal);
                if (ResultControl != null)
                    _accuralInLoop.HandleAttitudeResult(ResultControl);
            }
        }

        public void ChangeState(LawState state)
        {
            State = state;
            if (State == LawState.Accepted)
            {
                _gameInfo.AddTrigger(ResultTrigger);
                if (ResultRebel != null)
                    _accuralInLoop.HandleAttitudeResult(ResultRebel);
                if (ResultLoyal != null)
                    _accuralInLoop.HandleAttitudeResult(ResultLoyal);
                if (ResultControl != null)
                    _accuralInLoop.HandleAttitudeResult(ResultControl);
            }
            if (State == LawState.Unsucces)
            {
                HoldTimeRemeaning = HoldTime;
                HoldTimeRemeaningCount = HoldTime;
            }
            OnChangeState?.Invoke(state);
        }

        public bool TryTriggerLawByAttitudeAndDate(int currentYear, int currentMounth)
        {
            if(TryTriggerProjectByDate(currentYear, currentMounth) &&
                _attitudeCheck.CheckByAttitude(TriggerRebelOver))
            {
                return true;
            }

            return false;
        }

        public bool CheckTriggerByTriggerSignal()
        {
            int count = 0;
            foreach (string signal in TriggersSignal)
            {
                if (_gameInfo.CheckTrigger(signal) || signal == "null")
                {
                    count++;
                }
            }
            if (count == TriggersSignal.Count)
            {
                return true;
            }
            return false;
        }

        public bool TryTriggerProjectByDate(int currentYear, int currentMounth)
        {
            if (DateTrigger != null)
            {
                if (new DateTime(DateTrigger.Year, DateTrigger.Month, 1) <= new DateTime(currentYear, currentMounth, 1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public void PribeLaw(int chance)
        {
            IsBribed = true;
            Chance1 += chance;
        }
    }
}