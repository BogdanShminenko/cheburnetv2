﻿using Assets.Scripts.Utilities;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Event = AK.Wwise.Event;

namespace Assets.Scripts.MVC.LawsMVC
{
    public class LawUIItem : MonoBehaviour
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public Event _clickEvent;
            public Event _disabledEvent;
        }
        
        [SerializeField] private Image _icon;
        [SerializeField] private Image _iconButton;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _description;
        [SerializeField] private TMP_Text _textResult;
        [SerializeField] private Image _lawStatusImage;
        [SerializeField] private TMP_Text _lawStatusText;
        [SerializeField] private Sprite _canAcceptSprite;
        [SerializeField] private Sprite _lockedSprite;
        [SerializeField] private Sprite _acceptedSprite;
        [field: SerializeField] public Law CurrentLaw { get; private set; }

        [SerializeField] private WwiseSound _wwiseSound;
        
        [Inject] private AttitudeOperationToStringConverter _attitudeOperationToStringConverter;
        [Inject] private LawModel _lawModel;



        public void CallOpenLawPanel()
        {
            _lawModel.SelectLaw(CurrentLaw);

            if (_iconButton.sprite == _lockedSprite)
            {
                _wwiseSound._disabledEvent.Post(gameObject);
            }
            else
            {
                _wwiseSound._clickEvent.Post(gameObject);
            }
        }

        public void Init(AttitudeOperationToStringConverter attitudeOperationToStringConverter, LawModel lawModel)
        {
            _attitudeOperationToStringConverter = attitudeOperationToStringConverter;
            _lawModel = lawModel;
        }

        public void InitLawUI(Law law)
        {
            CurrentLaw = law;
            _title.text = law.Title;
            _description.text = law.Description;

            _textResult.text = law.TextSuccess;
            _icon.sprite = law.IconSprite;
            _icon.color = Color.white;
            ChangeLawState(CurrentLaw.State);
            CurrentLaw.OnChangeState += ChangeLawState;
        }

        public void ChangeLawState(LawState lawState)
        {
            if (lawState == LawState.Accept)
            {
                _lawStatusImage.sprite = _canAcceptSprite;
                _iconButton.sprite = _canAcceptSprite;
                //_lawStatusText.text = "Принять закон";
            }
            else if (lawState == LawState.Locked)
            {
                _iconButton.sprite = _lockedSprite;
                _lawStatusImage.sprite = _lockedSprite;
                //_lawStatusText.text = "Принять закон";
            }
            else if (CurrentLaw.State == LawState.Accepted)
            {
                //_lawStatusText.text = "Закон принят";
                _iconButton.sprite = _acceptedSprite;
                _lawStatusImage.sprite = _acceptedSprite;
            }
            else if (CurrentLaw.State == LawState.Unsucces)
            {
                //_lawStatusText.text = "Закон принят";
                _iconButton.sprite = _canAcceptSprite;
                _lawStatusImage.sprite = _canAcceptSprite;
            }
        }
    }
}