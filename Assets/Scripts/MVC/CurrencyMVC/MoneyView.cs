﻿using System.Collections;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.CurrencyMVC
{
    public sealed class MoneyView : MonoBehaviour
    {

        [SerializeField] private TMP_Text _moneyTextValue;

        [Inject] private MoneyModel _moneyModel;

        private void OnEnable()
        {
            _moneyModel.OnCurrencyUpdate += DisplayWill;
        }
        private void OnDisable()
        {
            _moneyModel.OnCurrencyUpdate -= DisplayWill;
        }

        private void DisplayWill()
        {
            _moneyTextValue.text = _moneyModel.CurrentAmount.ToString();
        }
    }
}