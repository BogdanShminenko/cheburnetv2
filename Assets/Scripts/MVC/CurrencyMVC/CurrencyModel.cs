using Assets.Scripts.MVC.TimerMVC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public abstract class CurrencyModel : MonoBehaviour
{
    [Inject] private GameSettingsData _gameSettingsData;
    public Action<uint> OnCurrencySpent { get; set; }
    public Action<uint> OnCurrencyRestored { get; set; }
    public Action OnCurrencyUpdate { get; set; }

    [SerializeField] private uint _currentAmount;
    [SerializeField] private uint _maxCurrencyAmount;
    [SerializeField] private float _currencyRestoreTimeCofficient;

    public uint MaxCurrencyAmount => _maxCurrencyAmount;
    public uint CurrentAmount => _currentAmount;
    public uint CurrencyRestoreAmount { get; private set; }
    public float CurrencyRestoreTimeCofficient => _currencyRestoreTimeCofficient;
    public bool IsRestoring { get; private set; }
    public GameSettingsData GameSettingsData => _gameSettingsData;

    public bool TrySpend(uint spendAmount)
    {
        if (CurrentAmount >= spendAmount)
        {
            _currentAmount -= spendAmount;
            OnCurrencySpent?.Invoke(spendAmount);
            OnCurrencyUpdate?.Invoke();
            if (IsRestoring == false)
                StartCoroutine(CurrencyRestoreHandler());
            return true;
        }
        return false;
    }

    public void StartRestoring()
    {
        StartCoroutine(CurrencyRestoreHandler());
    }

    public virtual void GameCycleUpdatedHandler()
    {
        _currentAmount = MaxCurrencyAmount;
        OnCurrencyUpdate?.Invoke();
    }

    public void SetMaxCurrencyAmount(int maxCurrencyAmount)
    {
        _maxCurrencyAmount = (uint)maxCurrencyAmount;
        StartCoroutine(CurrencyRestoreHandler());
    }

    public void SetCurrencyRestoreTimeCofficient(float currencyRestoreTime)
    {
        _currencyRestoreTimeCofficient = currencyRestoreTime;
    }

    public void SetCurrencyInfo(uint currentAmount, uint maxCurrencyAmount, 
        uint currencyRestoreAmount, float currencyRestoreTime , bool isRestoring)
    {
        IsRestoring = isRestoring;
        _currentAmount = currentAmount;
        _maxCurrencyAmount = maxCurrencyAmount;
        CurrencyRestoreAmount = currencyRestoreAmount;
        _currencyRestoreTimeCofficient = currencyRestoreTime;
        OnCurrencyUpdate?.Invoke();
    }

    public void InitCurrency(uint maxCurrencyAmount, uint currencyRestoreAmount, float currencyRestoreTime)
    {
        _maxCurrencyAmount = maxCurrencyAmount;
        CurrencyRestoreAmount = currencyRestoreAmount;
        _currencyRestoreTimeCofficient = currencyRestoreTime;
        _currentAmount = MaxCurrencyAmount;
        OnCurrencyUpdate?.Invoke();
    }

    public void Add(uint addAmount)
    {
        uint startValue = CurrentAmount;
        _currentAmount = (uint)Mathf.Clamp(CurrentAmount + addAmount, 0, MaxCurrencyAmount);
        OnCurrencyUpdate?.Invoke();
        if (addAmount > 0)
            OnCurrencyRestored?.Invoke(CurrentAmount - startValue);
    }

    private IEnumerator CurrencyRestoreHandler()
    {
        IsRestoring = true;
        yield return new WaitForSeconds(_gameSettingsData.Tick * CurrencyRestoreTimeCofficient);
        Add(CurrencyRestoreAmount);
        IsRestoring = false;
        if (CurrentAmount < MaxCurrencyAmount)
            StartCoroutine(CurrencyRestoreHandler());
    }
}
