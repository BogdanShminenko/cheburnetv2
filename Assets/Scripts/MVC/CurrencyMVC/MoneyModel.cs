using Assets.Scripts.MVC.CutsceneMVC;
using Assets.Scripts.MVC.TimerMVC;
using System;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.CurrencyMVC
{
    public sealed class MoneyModel : CurrencyModel
    {
        [Inject] private TimerModel _timerModel;
        [Inject] private TimerController _timerController;

        private void OnEnable()
        {
            _timerModel.OnGameNewCycle += base.GameCycleUpdatedHandler;
        }
        private void OnDisable()
        {
            _timerModel.OnGameNewCycle -= base.GameCycleUpdatedHandler;
        }

        public void IncreaseMaxMoney()
        {
            int maxNew = (int)(MaxCurrencyAmount * (1 + GameSettingsData.MoneyIncreaseForQuests * _timerController.QuestComplitedForCycle + GameSettingsData.MoneyIncreaseDefault));
            SetMaxCurrencyAmount(maxNew);
        }

        public void IncreaseMoneyRestoreTime()
        {
            float restoreTime = CurrencyRestoreTimeCofficient / (1 + GameSettingsData.MoneyIncreaseForQuests * _timerController.QuestComplitedForCycle + GameSettingsData.MoneyIncreaseDefault);
            SetCurrencyRestoreTimeCofficient(restoreTime);
        }
        public override void GameCycleUpdatedHandler()
        {
            base.GameCycleUpdatedHandler();
        }

    }
}
