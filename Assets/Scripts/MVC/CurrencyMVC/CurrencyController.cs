using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.CurrencyMVC
{
    public sealed class CurrencyController : MonoBehaviour
    {
        [Inject] private WillModel _willModel;
        [Inject] private MoneyModel _moneyModel;

        public void SpendMoney(int count)
        {
            _moneyModel.TrySpend((uint)count);
        }

        public void SpendWill(int count)
        {
            _willModel.TrySpend((uint)count);
        }

        public void AddMoney(int count)
        {
            _moneyModel.Add((uint)count);
        }

        public void AddWill(int count)
        {
            _willModel.Add((uint)count);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                SpendMoney(50);
            }
            if (Input.GetKeyDown(KeyCode.W))
            {
                SpendWill(50);
            }
        }
    }
}
