using Assets.Scripts.MVC.TimerMVC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Zenject.Asteroids;

public sealed class WillModel : CurrencyModel
{

    [Inject] private TimerModel _timerModel;

    private void OnEnable()
    {
        _timerModel.OnGameNewCycle += base.GameCycleUpdatedHandler;
    }
    private void OnDisable()
    {
        _timerModel.OnGameNewCycle -= base.GameCycleUpdatedHandler;
    }

    public void IncreaseMaxWill()
    {
        int maxNew = (int)(MaxCurrencyAmount * (1 + GameSettingsData.WillMaxIncrease));
        SetMaxCurrencyAmount(maxNew);
    }

    public void IncreaseWillRestoreTime()
    {
        float restoreTime = CurrencyRestoreTimeCofficient / (1 + GameSettingsData.WillRestoreIncrease);
        SetCurrencyRestoreTimeCofficient(restoreTime);
    }

    public override void GameCycleUpdatedHandler()
    {
        base.GameCycleUpdatedHandler();
    }

}
