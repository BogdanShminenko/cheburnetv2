﻿using System.Collections;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.CurrencyMVC
{
    public sealed class WillView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _willValueText;

        [Inject] private WillModel _willModel;

        private void OnEnable()
        {
            _willModel.OnCurrencyUpdate += DisplayWill;
        }
        private void OnDisable()
        {
            _willModel.OnCurrencyUpdate -= DisplayWill;
        }

        private void DisplayWill()
        {
            _willValueText.text = _willModel.CurrentAmount.ToString();
        }


    }
}