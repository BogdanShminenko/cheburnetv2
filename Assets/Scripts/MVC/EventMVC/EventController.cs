﻿using Assets.Scripts.Game;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.MVC.UsersAttitude;
using Assets.Scripts.TriggersAndOperationsTypes;
using Assets.Scripts.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.EventMVC
{
    public class EventController : MonoBehaviour
    {
        [Inject] private EventModel _eventModel;
        [Inject] private TimerModel _timerModel;
        [Inject] private AttitudeCheck _attitudeCheck;
        [Inject] private GameInfo _gameInfo;

        public void InitEvents()
        {
            _eventModel.InitEvents();
        }

        public void TryOpenSimpleEvent()
        {
            _eventModel.TryHandleSimpleEvent();
        }

        // Пытаемся затригерить события по времени
        public bool CheckTriggerEventByDate(Event triggerEvent)
        {
            if (triggerEvent.TriggerDate == null)
                return true;

            if (new DateTime(triggerEvent.TriggerDate.Year, triggerEvent.TriggerDate.Month, 1) <= new DateTime((int)_timerModel.CurrentYear, (int)_timerModel.CurrentMonth, 1))
            {
                return true;
            }
            return false;
        }

        private bool CheckTriggerByTriggerSignal(IReadOnlyCollection<string> triggersSignals)
        {
            int count = 0;
            foreach (string signal in triggersSignals)
            {
                if (_gameInfo.CheckTrigger(signal) || signal == "null")
                {
                    count++;
                }
            }
            if (count == triggersSignals.Count)
            {
                return true;
            }
            return false;
        }

        // Пытаемся затригерить события по отношению юзеров
        public bool TryTriggerByRelationship(Event triggeredEvent)
        {
            if(_attitudeCheck.CheckByAttitude(triggeredEvent.TriggerLoyalOver) &&
                _attitudeCheck.CheckByAttitude(triggeredEvent.TriggerLoyalUnder) &&
                _attitudeCheck.CheckByAttitude(triggeredEvent.TriggerRebelOver)&&
                _attitudeCheck.CheckByAttitude(triggeredEvent.TriggerRebelUnder))
            {
                return true;
            }
            return false;
        }

        public void CallTriggerEvent()
        {
            for (int i = 0; i < _eventModel.Events.Count; i++)
            {
                if (TryTriggerByRelationship(_eventModel.Events[i]) && CheckTriggerEventByDate(_eventModel.Events[i])
                    && CheckTriggerByTriggerSignal(_eventModel.Events[i].TriggerSignals))
                {
                    _eventModel.CallPublishEvent(_eventModel.Events[i]);
                }
            }
        }


    }
}