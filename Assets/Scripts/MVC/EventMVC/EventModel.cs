﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Data.GameData.Configs;
using Assets.Scripts.Game;
using Assets.Scripts.GameStates;
using Assets.Scripts.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.EventMVC
{
    public class EventModel : MonoBehaviour
    {
        [Inject] private EventConfig _eventConfig;
        [Inject] private GameState _gameState;
        [Inject] private AccuralInLoop _accuralInLoop;
        [Inject] private GameInfo _gameInfo;

        [Inject] private DateTriggerConventer _dateTriggerConventer;
        [Inject] private EventsPanel _eventsPanel;


        [SerializeField] private List<Event> _events = new List<Event>();
        private List<Event> _triggeredExpressEvents = new List<Event>();
        private List<Event> _triggeredSimpleEvents = new List<Event>();
        private List<Event> _handeledEvents = new List<Event>();

        public event Action<Event> OnPublishingExpressEvent;
        public event Action<Event> OnPublishingSimpleEvent;
        public event Action<Event> OnHandlePublishingSimpleEvent;
        public event Action<Event> OnHandledEvent;
       
        public int TriggeredSimpleEvents => _triggeredSimpleEvents.Count;
        public IReadOnlyList<Event> HandeledEvents => _handeledEvents;

        public IReadOnlyList<Event> Events => _events;
        private Coroutine _coroutineHandleExpressEvent;

        private void OnEnable()
        {
            _gameState.OnGameStateChanged += HandleExpressEvent;
        }

        private void OnDisable()
        {
            _gameState.OnGameStateChanged -= HandleExpressEvent;
        }

        public void InitEvents()
        {
            foreach(var item in _eventConfig.Events)
            {
                Event eventItem = new Event(_gameInfo, _dateTriggerConventer);
                eventItem.InitEvent(item);
                _events.Add(eventItem);
            }
        }

        public void SetHandledEvents(List<Event> events)
        {
            _handeledEvents = events;
            OnHandledEvent?.Invoke(_handeledEvents[0]);
            _eventsPanel.DisplayItems();
        }

        public void SetEvents(List<Event> events)
        {
            _events = events;
        }

        // Пытаемся получить простой евент
        public void TryHandleSimpleEvent()
        {
            if(_triggeredSimpleEvents.Count > 0)
            {
                var simpleEvent = _triggeredSimpleEvents[0];
                _triggeredSimpleEvents.Remove(simpleEvent);
                OnHandlePublishingSimpleEvent?.Invoke(simpleEvent);
            }
        }

        // Открываем экспрес евент
        public void HandleExpressEvent()
        {
            if(_gameState.CurrentGameState == GameStates.GameStates.InternetMap)
            {
                if (_triggeredExpressEvents.Count > 0)
                {
                    _coroutineHandleExpressEvent = StartCoroutine(HandleEventAffterOneSecond());
                }
            }
            else
            {
                if(_coroutineHandleExpressEvent != null)
                {
                    StopCoroutine(_coroutineHandleExpressEvent);
                    _coroutineHandleExpressEvent = null;
                }
            }
        }

        private IEnumerator HandleEventAffterOneSecond()
        {
            Debug.Log("Handeled event");
            int count = 0;
            while (true)
            {
                //if (_gameState.CurrentGameState == GameStates.GameStates.InternetMap
                //    || _gameState.CurrentGameState == GameStates.GameStates.Events)
                //{
                //    Debug.Log("BrakedEvents");
                //    break;
                //}
                if (count == 1)
                    break;
                count++;
                yield return new WaitForSeconds(1);
            }
            if (_gameState.CurrentGameState == GameStates.GameStates.InternetMap 
                || _gameState.CurrentGameState == GameStates.GameStates.Events)
            {
                Event triggeredEvent = _triggeredExpressEvents[0];
                _triggeredExpressEvents.Remove(triggeredEvent);
                HandleEvent(triggeredEvent);
                OnPublishingExpressEvent?.Invoke(triggeredEvent);
            }
        }

        // Публикуем событие
        public void CallPublishEvent(Event triggeredEvent)
        {
            StartCoroutine(PublishEvent(triggeredEvent));
            _events.Remove(triggeredEvent);
        }
        
        // Обрабатываем результат события
        private void HandleEvent(Event triggeredEvent)
        {
            if (_handeledEvents.Contains(triggeredEvent))
                return;
            _handeledEvents.Add(triggeredEvent);
            _gameInfo.AddTrigger(triggeredEvent.ResultTrigger);
            if (triggeredEvent.ResultRebel != null)
                _accuralInLoop.HandleAttitudeResult(triggeredEvent.ResultRebel);
            if (triggeredEvent.ResultLoyal != null)
                _accuralInLoop.HandleAttitudeResult(triggeredEvent.ResultLoyal);
            if (triggeredEvent.ResultControl != null)
                _accuralInLoop.HandleAttitudeResult(triggeredEvent.ResultControl);

            OnHandledEvent?.Invoke(triggeredEvent);
        }

        private IEnumerator PublishEvent(Event triggeredEvent)
        {
            int count = UnityEngine.Random.Range(0, (int)triggeredEvent.Range);
            while (true)
            {
                if (count == 0)
                    break;
                count--;
                yield return new WaitForSeconds(1);
            }
            if (triggeredEvent.EventType == Data.GameData.TypeOfEvent.Express)
            {
                if (_gameState.CurrentGameState == GameStates.GameStates.InternetMap
                    || _gameState.CurrentGameState == GameStates.GameStates.Events)
                {
                    HandleEvent(triggeredEvent);
                    OnPublishingExpressEvent?.Invoke(triggeredEvent);
                }
                else
                {
                    HandleEvent(triggeredEvent);
                    _triggeredExpressEvents.Add(triggeredEvent);
                }
            }
            else
            {
                HandleEvent(triggeredEvent);
                _triggeredSimpleEvents.Add(triggeredEvent);
                OnPublishingSimpleEvent?.Invoke(triggeredEvent);
            }
        }

    }
}