﻿using Assets.Scripts.GameStates;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MVC.EventMVC
{
    public class EventPanel : MonoBehaviour
    {
        [SerializeField] private GameObject _panel;
        [SerializeField] private Image _icon;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _text;
        
        [Header("Events")]
        [SerializeField] private UnityEvent _onOpenExpressEvent;
        [SerializeField] private UnityEvent _onCloseExpressEvent;
        
        [Inject] private GameState _gameState;

        private bool _isExpress;
        
        public void SetEvent(Event currentEvent, string spriteSource , string title, string text)
        {
            currentEvent.IsNew = false;

            _isExpress = currentEvent.EventType == Data.GameData.TypeOfEvent.Express;
            _icon.sprite = currentEvent.IconSprite;
            _icon.color = Color.white;
            _title.text = title;
            _text.text = text;
        }

        public void OpenPanel()
        {
            _gameState.ChangeGameState(GameStates.GameStates.Events);
            _panel.SetActive(true);
            
            if (_isExpress && _onOpenExpressEvent != null)
            {
                _onOpenExpressEvent.Invoke();
            }
        }

        public void Close()
        {
            _gameState.ChangeGameState(GameStates.GameStates.InternetMap);
            _panel.SetActive(false);
            
            if (_isExpress && _onCloseExpressEvent != null)
            {
                _onCloseExpressEvent.Invoke();
            }
        }

        public void Complete()
        {
            _gameState.ChangeGameState(GameStates.GameStates.InternetMap);
            _panel.SetActive(false);
            
            if (_isExpress && _onCloseExpressEvent != null)
            {
                _onCloseExpressEvent.Invoke();
            }
        }
    }
}