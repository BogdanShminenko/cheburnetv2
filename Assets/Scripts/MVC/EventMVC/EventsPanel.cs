﻿using Assets.Scripts.GameStates;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.EventMVC
{
    public class EventsPanel : MonoBehaviour
    {
        [SerializeField] private GameObject _panel;
        [SerializeField] private EventItem _item;
        [SerializeField] private Transform _parent;
        [Inject] private EventModel _model;
        [Inject] private EventView _view;
        [Inject] private GameState _gameState;
        [Inject] private ButtonsMenu _buttonsMenu;

        private List<EventItem> _eventItems = new List<EventItem>();

        private void Start()
        {
            _model.OnHandledEvent += DisplayItems;
        }

        public void OpenPanel()
        {
            _gameState.ChangeGameState(GameStates.GameStates.Events);
            _panel.SetActive(true);
            _view.HideNotificatifications();
            DisplayItems();
        }

        public void ClosePanel()
        {
            _gameState.ChangeGameState(GameStates.GameStates.InternetMap);
            _panel.SetActive(false);
            _buttonsMenu.ResetButtonsWithOutOffPanels();
        }

        public void DisplayItems(Event @event)
        {
            DisplayItems();
        }

        public void DisplayItems()
        {
            var events = new List<Event>(_model.HandeledEvents);
            //var events = new List<Event>(_model.HandeledEvents.Where(item => !item.IsNew).ToArray());
            //events.AddRange(_model.HandeledEvents.Where(item => item.IsNew).ToArray());
            if (_eventItems.Count > 0 )
            {
                foreach (var item in _eventItems)
                {
                    item.OnClick -= _view.DisplayEvent;
                    Destroy(item.gameObject);
                }
                _eventItems.Clear();
            }

            foreach(var item in events)
            {
                EventItem eventItem = Instantiate(_item, _parent);
                eventItem.Init(item);
                eventItem.transform.SetSiblingIndex(0);
                eventItem.OnClick += _view.DisplayEvent;
                _eventItems.Add(eventItem);
            }
        }
    }
}