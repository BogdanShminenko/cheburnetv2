﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.MVC.EventMVC
{
    public class EventItem : MonoBehaviour , IPointerClickHandler
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public AK.Wwise.Event _clickEvent;
        }
        
        [SerializeField] private Image _icon;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _description;
        [SerializeField] private GameObject _newIcon;

        [SerializeField] private Event _currentEvent;

        [SerializeField] private WwiseSound _wwiseSound;
        
        public event Action<Event> OnClick;

        public void Init(Event currentEvent)
        {
            _currentEvent = currentEvent;
            _title.text = _currentEvent.Title;
            _description.text = _currentEvent.Description;
            _icon.sprite = _currentEvent.IconSprite;
            _icon.color = Color.white;

            if (_currentEvent != null && _currentEvent.IsNew && _currentEvent.EventType != Data.GameData.TypeOfEvent.Express)
            {
                OnNewIcon();
            }
            else
            {
                OffNewIcon();
            }
        }

        public void OffNewIcon()
        {
            _newIcon.SetActive(false);
        }

        public void OnNewIcon()
        {
            _newIcon.SetActive(true);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OffNewIcon();
            OnClick?.Invoke(_currentEvent);

            if (_wwiseSound._clickEvent.IsValid())
            {
                _wwiseSound._clickEvent.Post(gameObject);
            }
        }
    }
}