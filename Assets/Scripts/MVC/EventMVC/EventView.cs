﻿using Assets.Scripts.Data.GameData;
using System.Collections;
using UnityEngine;
using Zenject;
using static UnityEngine.EventSystems.EventTrigger;

namespace Assets.Scripts.MVC.EventMVC
{
    public class EventView : MonoBehaviour
    {
        [SerializeField] private GameObject _eventNotification;

        [Inject] private EventModel _eventModel;
        [Inject] private EventPanel _eventPanel;

        public bool NotificationStatus => _eventNotification.activeSelf;

        public void OnEnable()
        {
            _eventModel.OnPublishingExpressEvent += DisplayEvent;
            _eventModel.OnPublishingSimpleEvent += PublishingSimpleEventHandler;
            _eventModel.OnHandlePublishingSimpleEvent += DisplayEvent;
            _eventModel.OnHandledEvent += DisplayNotificationEvent;
        }


        public void OnDisable()
        {
            _eventModel.OnPublishingExpressEvent -= DisplayEvent;
            _eventModel.OnPublishingSimpleEvent -= PublishingSimpleEventHandler;
            _eventModel.OnHandlePublishingSimpleEvent -= DisplayEvent;
            _eventModel.OnHandledEvent -= DisplayNotificationEvent;
        }

        public void SetNotificationStatus(bool notificationStatus)
        {
            _eventNotification.SetActive(notificationStatus);
        }

        public void PublishingSimpleEventHandler(Event triggeredEvent)
        {
            DisplayNotificationEvent(triggeredEvent);
        }

        public void DisplayNotificationEvent(Event handledEvent)
        {
            if (handledEvent.EventType != TypeOfEvent.Express)
                _eventNotification.SetActive(true);
        }

        public void HideNotificatifications()
        {
            _eventNotification.SetActive(false);
        }

        public void DisplayEvent(Event triggeredEvent)
        {
            if(triggeredEvent.EventType != TypeOfEvent.Express)
                _eventNotification.SetActive(false);
            _eventPanel.SetEvent(triggeredEvent, "", triggeredEvent.Title, triggeredEvent.Description);
            _eventPanel.OpenPanel();
        }


    }
}