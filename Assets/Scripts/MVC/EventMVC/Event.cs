﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Data.GameData;
using Assets.Scripts.Game;
using Assets.Scripts.TriggersAndOperationsTypes;
using Assets.Scripts.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.EventMVC
{

    [Serializable]
    public class Event
    {
        public string EventID { get; set; }
        public TypeOfEvent EventType { get; set; }
        public TypeOfResusable Reusable { get; set; }
        public DateTrigger TriggerDate { get; set; }
        public AttitudeOperation TriggerRebelOver { get; set; }
        public AttitudeOperation TriggerRebelUnder { get; set; }
        public AttitudeOperation TriggerLoyalOver { get; set; }
        public AttitudeOperation TriggerLoyalUnder { get; set; }
        public List<string> TriggerSignals { get; set; } = new List<string>();
        public float Range { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ResultTrigger { get; set; }
        public string Icon { get; set; }

        public AttitudeResult ResultRebel { get; set; }
        public AttitudeResult ResultLoyal { get; set; }
        public AttitudeResult ResultControl { get; set; }
        [field: SerializeField] public bool IsNew { get; set; }

        private DateTriggerConventer _dateTriggerConventer;
        [JsonIgnore] public Sprite IconSprite { get; set; }
        private GameInfo _gameInfo;

        public Event(GameInfo gameInfo,DateTriggerConventer dateTriggerConventer)
        {
            _gameInfo = gameInfo;
            _dateTriggerConventer = dateTriggerConventer;
        }

        public void InitDependecies(DateTriggerConventer dateTriggerConventer)
        {
            _dateTriggerConventer = dateTriggerConventer;
        }


        //Инициализируем и конвертируем евент
        public void InitEvent(EventData eventData)
        {
            IsNew = true;
            EventID = eventData.EventID;
            Range = eventData.Range;
            Title = eventData.Title;
            Description = eventData.Description;
            ResultTrigger = eventData.ResultTrigger;
            Icon = eventData.Icon;
            if (_dateTriggerConventer.TryConvertStringToDateTrigger(eventData.TriggerDate, out DateTrigger dateTrigger))
                TriggerDate = dateTrigger;

            if (eventData.EventType == "E")
                EventType = TypeOfEvent.Express;
            else if (eventData.EventType == "S")
                EventType = TypeOfEvent.Simple;

            if (eventData.Reusable == "R")
                Reusable = TypeOfResusable.Resusability;
            else if (eventData.Reusable == "D")
                Reusable = TypeOfResusable.Disposability;

            if (eventData.TriggerRebelOver != 0)
            {
                TriggerRebelOver = new AttitudeOperation(Accrual.OperationType.Increase, Accrual.CurrencyType.Rebel, eventData.TriggerRebelOver);
            }
            if (eventData.TriggerRebelUnder != 0)
            {
                TriggerRebelUnder = new AttitudeOperation(Accrual.OperationType.Decrease, Accrual.CurrencyType.Rebel, eventData.TriggerRebelUnder);
            }
            if (eventData.TriggerLoyalOver != 0)
            {
                TriggerLoyalOver = new AttitudeOperation(Accrual.OperationType.Increase, Accrual.CurrencyType.Loyal, eventData.TriggerLoyalOver);
            }
            if (eventData.TriggerLoyalUnder != 0)
            {
                TriggerLoyalUnder = new AttitudeOperation(Accrual.OperationType.Decrease, Accrual.CurrencyType.Loyal, eventData.TriggerLoyalUnder);
            }

            if (eventData.ResultLoyal != 0)
            {
                if (eventData.ResultLoyal >= 0)
                    ResultLoyal = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(eventData.ResultLoyalSpeed), eventData.ResultLoyal, Accrual.CurrencyType.Loyal, Accrual.OperationType.Increase);
                else
                    ResultLoyal = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(eventData.ResultLoyalSpeed), eventData.ResultLoyal, Accrual.CurrencyType.Loyal, Accrual.OperationType.Decrease);
            }
            if (eventData.ResultRebel != 0)
            {
                if (eventData.ResultRebel >= 0)
                    ResultRebel = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(eventData.ResultRebelSpeed), eventData.ResultRebel, Accrual.CurrencyType.Rebel, Accrual.OperationType.Increase);
                else
                    ResultRebel = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(eventData.ResultRebelSpeed), eventData.ResultRebel, Accrual.CurrencyType.Rebel, Accrual.OperationType.Decrease);
            }
            if (eventData.ResultControl != 0)
            {
                if (eventData.ResultControl >= 0)
                    ResultControl = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(eventData.ResultControlSpeed), eventData.ResultControl, Accrual.CurrencyType.InternetControl, Accrual.OperationType.Increase);
                else
                    ResultControl = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(eventData.ResultControlSpeed), eventData.ResultControl, Accrual.CurrencyType.InternetControl, Accrual.OperationType.Decrease);
            }

            if (Icon != null && Icon.Length > 0)
                IconSprite = Resources.Load<Sprite>($"Sprites/Events/{Icon.Replace(".png", "")}");

            if (eventData.TriggerSignal == "" || eventData.TriggerSignal.Length == 0)
            {
                TriggerSignals.Add("null");
            }
            else
            {
                string[] triggers = eventData.TriggerSignal.Split(',');
                TriggerSignals = triggers.ToList();
            }
        }

        public void LoadIcon()
        {
            if (Icon != null && Icon.Length > 0)
                IconSprite = Resources.Load<Sprite>($"Sprites/Events/{Icon.Replace(".png", "")}");
        }

        private ValueCangeType GetValueChangeBySymbol(string symbol)
        {
            if (symbol == "M")
                return ValueCangeType.Moment;
            else if (symbol == "S")
                return ValueCangeType.Slow;
            else if (symbol == "N")
                return ValueCangeType.Normal;
            return ValueCangeType.Fast;
        }
    }
}