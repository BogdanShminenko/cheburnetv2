﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.Game;
using Assets.Scripts.InternetMapGeneration;
using Assets.Scripts.InternetObjects;
using Assets.Scripts.InternetObjects.Node;
using Assets.Scripts.InternetObjects.Servers;
using Assets.Scripts.InternetObjects.User;
using Assets.Scripts.MVC.ServerMVC;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.MVC.UsersAttitude;
using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.InternetMVC
{
    public class InternetController : MonoBehaviour
    {
        [Inject] private InternetModel  _internetModel;
        [Inject] private CellsCircleMapGeneration _cellsGeneration;
        [Inject] private RoundGeneration _roundGeneration;
        [Inject] private UserObject _userPrefab;
        [Inject] private NodeObject _nodePrefab;
        [Inject] private InternetMapSettingsData _internetMapSettingsData;
        [Inject] private CellSquareMapGeneration _cellSquareMapGeneration;
        [Inject] private InternetMapPositions _internetMapPositions;
        [Inject] private CalculateUsersGrow _calculateUsersGrow;
        [Inject] private TimerModel _timerModel;
        [Inject] private UsersAttitudeModel _usersAttitudeModel;
        [Inject] private OuterServerLineService _outerServerLineService;

        private int _lastSquareGenerationPosition = 1;

        private void OnEnable()
        {
            _timerModel.OnMonthPassedCallback += CalculateUsers;
        }

        private void OnDisable()
        {
            _timerModel.OnMonthPassedCallback -= CalculateUsers;
        }

        public void CreateInternetMapObject<T>(T prefab, ServerPerInternetType serverPerInternetType, out T server) where T : InternetObject
        {
            //server = _cellsGeneration.Generate(prefab);
            if (serverPerInternetType == ServerPerInternetType.Inner)
            {
                server = _cellsGeneration.Generate(prefab);
                if (server is ServerObject serverObject)
                {
                    _internetModel.AddServer(serverObject);
                    if(_internetModel.CheckServerCollisionWithUsers(out List<UserObject> userObjects , server.transform.position))
                    {
                        foreach(var item in userObjects)
                        {
                            _cellsGeneration.RepositionUserObject(item);
                        }
                    }
                }
            }
            else if (serverPerInternetType == ServerPerInternetType.Outer)
            {
                if (_lastSquareGenerationPosition % 2 == 0)
                {
                    server = _cellSquareMapGeneration.Generate(prefab, _internetMapPositions.UpPosition);
                    if(server != null)
                        server.ObjectLocationType = ObjectLocationType.OuterUp;

                }
                else
                {
                    server = _cellSquareMapGeneration.Generate(prefab, _internetMapPositions.DownPosition);
                    if (server != null)
                        server.ObjectLocationType = ObjectLocationType.OuterDown;
                }
                if (server != null && server is ServerObject serverObject)
                {
                    _internetModel.AddServer(serverObject);
                    _outerServerLineService.CreateServerObjectLine(serverObject);
                }
                _lastSquareGenerationPosition++;
            }
            else
            {
                Debug.LogError("Server default");
                server = default;
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.H))
            {
                for(int i = 0; i < 10; i++)
                {
                    CreateInternetMapObject(_userPrefab, ServerPerInternetType.Inner, out UserObject userObject);
                }
            }
        }

        public void CalculateUsers()
        {
            int countTotal = _calculateUsersGrow.GetTotalUsersNumber();
            int count = countTotal - _internetModel.CountUsersInInnerInternet;
            List<UserObject> createdUsers = new List<UserObject>();
            _internetModel.IncresetCountInnerInternet(count);
            for(int i = 0; i < count; i++)
            {
                CreateInternetMapObject(_userPrefab, ServerPerInternetType.Inner, out UserObject userObject);
                userObject.ChangeColorByAttitude(AttitudeTypes.Neutral);
                createdUsers.Add(userObject);
                _internetModel.AddUser(userObject);
            }
            //Debug.Log("createdUsers " + createdUsers.Count);
            //foreach(var item in createdUsers)
            //{
            //    if(_internetModel.CheckUsersCollisionWithCollision(item.transform.position))
            //    {
            //        Debug.Log("Colided");
            //        _cellsGeneration.RepositionUsersObjects(item);
            //    }
            //}
            ChangeBelongingUsers();
        }

        private void ChangeBelongingUsers()
        {
            int countRebelToColor = _usersAttitudeModel.CountOfUsersByType(AttitudeTypes.Rebel, _internetModel.CountUsersInInnerInternet);
            int countLoyalToColor = _usersAttitudeModel.CountOfUsersByType(AttitudeTypes.Loyal, _internetModel.CountUsersInInnerInternet);
            int countNeutralToColor = _usersAttitudeModel.CountOfUsersByType(AttitudeTypes.Neutral, _internetModel.CountUsersInInnerInternet);
            
            int totalRebelUsers = _internetModel.UserObjects.Where(item => item.AttitudeTypesBelonging == AttitudeTypes.Rebel).ToArray().Length;
            int totalLoyalUsers = _internetModel.UserObjects.Where(item => item.AttitudeTypesBelonging == AttitudeTypes.Loyal).ToArray().Length;
            int totalNeutralUsers = _internetModel.UserObjects.Where(item => item.AttitudeTypesBelonging == AttitudeTypes.Neutral).ToArray().Length;

            List<UserObject> usersObjectsRebelForSort = _internetModel.UserObjects.Where(item => item.AttitudeTypesBelonging == AttitudeTypes.Neutral).ToList();
            usersObjectsRebelForSort.AddRange(_internetModel.UserObjects.Where(item => item.AttitudeTypesBelonging != AttitudeTypes.Neutral));
            List<UserObject> usersObjectsRebel = _internetModel.GetUserObjectsByServerOrentation(usersObjectsRebelForSort, Ownage.Free);
            ChangeUserObjectsColor(ref usersObjectsRebel, countRebelToColor - totalRebelUsers, AttitudeTypes.Rebel);


            List<UserObject> usersObjectsLoyalForSort = _internetModel.UserObjects.Where(item => item.AttitudeTypesBelonging == AttitudeTypes.Neutral).ToList();
            List<UserObject> usersObjectsLoyal = _internetModel.GetUserObjectsByServerOrentation(usersObjectsLoyalForSort, Ownage.User);
            
            ChangeUserObjectsColor(ref usersObjectsLoyal, countLoyalToColor - totalLoyalUsers, AttitudeTypes.Loyal);

            if(countNeutralToColor >= 0)
            {
                List<UserObject> usersObjectsNeutralForSort = _internetModel.UserObjects.Where(item => item.AttitudeTypesBelonging != AttitudeTypes.Neutral).ToList();
                ChangeUserObjectsColor(ref usersObjectsNeutralForSort, countNeutralToColor - totalNeutralUsers, AttitudeTypes.Neutral);
            }
            else
            {
                List<UserObject> usersObjectsNeutralForSort = _internetModel.UserObjects.Where(item => item.AttitudeTypesBelonging == AttitudeTypes.Neutral).ToList();
                if(countRebelToColor > countLoyalToColor)
                    ChangeUserObjectsColor(ref usersObjectsNeutralForSort, usersObjectsNeutralForSort.Count, AttitudeTypes.Rebel);
                else
                    ChangeUserObjectsColor(ref usersObjectsNeutralForSort, usersObjectsNeutralForSort.Count, AttitudeTypes.Loyal);
            }
        }

        private void ChangeUserObjectsColor(ref List<UserObject> userObjects , int count, AttitudeTypes attitudeType)
        {
            List<UserObject> colloredUsers = new List<UserObject>();

            if (count <= 0)
            {
                return;
            }
            for(int i = 0; i < count; i++)
            {
                if(i < userObjects.Count)
                {
                    userObjects[i].ChangeColorByAttitude(attitudeType);
                    colloredUsers.Add(userObjects[i]);
                }
                else
                {
                    foreach(var item in colloredUsers)
                        userObjects.Remove(item);
                    break;
                }
            }
            foreach (var item in colloredUsers)
                userObjects.Remove(item);
        }

        public void GenerateMap()
        {
            //_cellsGeneration.Generate(_serverPrefab, _internetMapSettingsData.ServerInnerCount);
            //_internetModel.DistrebuteInternetObjects(_cellsGeneration.Generate(_userPrefab, _internetMapSettingsData.UsersInnerCount));

            //_internetModel.DistrebuteInternetObjects(_roundGeneration.Generate(_nodePrefab, _internetMapSettingsData.NodeCount));

            //_internetModel.AddUsers(_cellSquareMapGeneration.Generate(_userPrefab, _internetMapSettingsData.UsersOuterCount, _internetMapPositions.UpPosition , ObjectLocationType.OuterUp));
            //_internetControllModel.AddServers(_cellSquareMapGeneration.Generate(_serverPrefab, _internetMapSettingsData.ServerOuterCount, _internetMapPositions.UpPosition));
            //_internetModel.AddUsers(_cellSquareMapGeneration.Generate(_userPrefab, _internetMapSettingsData.UsersOuterCount, _internetMapPositions.DownPosition, ObjectLocationType.OuterDown));
            //_internetControllModel.AddServers(_cellSquareMapGeneration.Generate(_serverPrefab, _internetMapSettingsData.ServerOuterCount, _internetMapPositions.DownPosition));
        }

        public void InitInternet()
        {
            GenerateMap();
        }
    }
}