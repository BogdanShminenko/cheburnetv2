﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.InternetObjects;
using Assets.Scripts.InternetObjects.Node;
using Assets.Scripts.InternetObjects.Servers;
using Assets.Scripts.InternetObjects.User;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.InternetMVC
{

    public class InternetModel : MonoBehaviour
    {
        [SerializeField] private List<ServerObject> _servers = new List<ServerObject>();
        [SerializeField] private List<NodeObject> _nodes = new List<NodeObject>();
        [SerializeField] private List<UserObject> _users = new List<UserObject>();

        [field:SerializeField] public int CountUsersInInnerInternet { get; private set; }
        public IEnumerable<UserObject> UserObjects => _users;

        public IEnumerable<ServerObject> Servers => _servers;


        public void SetCountUsersInInnerInternet(int count)
        {
            CountUsersInInnerInternet = count;
        }

        public bool TryGetUserByPosition(Vector2 position, out UserObject userObject)
        {
            userObject = _users.FirstOrDefault(x => x.Position == position);
            if (userObject != null)
            {
                return true;
            }
            return false;
        }

        public List<UserObject> GetUserObjectsByServerOrentation(List<UserObject> users,Ownage ownage)
        {
            List<ServerObject> serverObjects = _servers.Where(item => item.Server.Ownage == ownage).ToList();
            if (serverObjects == null || serverObjects.Count == 0)
            {
                users = users.OrderByDescending(item => item.DistanceByServer).ToList();
                return users;
            }

            ServerObject serverObjectTarget = serverObjects[Random.Range(0, serverObjects.Count)];

            foreach (var userObject in users)
            {
                float distance = Vector3.Distance(userObject.transform.position, serverObjectTarget.transform.position);
                userObject.SetDistanceByServer(distance);
            }

            users = users.OrderBy(item => item.DistanceByServer).ToList();
            
            for(int i = 0; i < users.Count; i++)
            {
                users[i].name = i.ToString();
            }

            return users;
        }


        public List<ServerObjectInfo> GetServerObjectInfos()
        {
            List<ServerObjectInfo> serverObjectInfos = new List<ServerObjectInfo>();

            foreach(var item in _servers)
            {
                ServerObjectInfo serverObjectInfo = new ServerObjectInfo(item.transform.position, item.Server, item.Distace,item.ObjectLocationType, item.Offset, item.NotificationStatus);
                serverObjectInfos.Add(serverObjectInfo);
            }

            return serverObjectInfos;
        }

        public List<NodeObjectInfo> GetNodeObjectInfos()
        {
            List<NodeObjectInfo> nodeObjects = new List<NodeObjectInfo>();

            foreach (var item in _nodes)
            {
                NodeObjectInfo nodeObject = new NodeObjectInfo(item.Position, item.Distace, item.ObjectLocationType);
                nodeObjects.Add(nodeObject);
            }

            return nodeObjects;
        }
        
        public List<UserObjectInfo> GetUserObjectInfos()
        {
            List<UserObjectInfo> usersObjects = new List<UserObjectInfo>();

            foreach (var item in _users)
            {
                UserObjectInfo userObject = new UserObjectInfo(item.Position, item.Distace, item.ObjectLocationType, item.Offset, item.AttitudeTypesBelonging);
                usersObjects.Add(userObject);
            }

            return usersObjects;
        }

        public bool CheckServerCollisionWithUsers(out List<UserObject> userObjects , Vector3 point)
        {
            userObjects = new List<UserObject>();
            List<Collider2D> collider2Ds = Physics2D.OverlapBoxAll(point, new Vector2(2, 2), 0).ToList();
            foreach(var item in collider2Ds)
            {
                if(item.TryGetComponent(out UserObject userObject))
                {
                    userObjects.Add(userObject);
                }
            }
            if(userObjects.Count > 0)
                return true;
            return false;
        }

        public void IncresetCountInnerInternet(int count)
        {
            CountUsersInInnerInternet += count;
        }

        public void AddUser(UserObject user)
        {
            _users.Add(user);
        }

        public void AddNode(NodeObject node)
        {
            _nodes.Add(node);   
        }

        public void AddServer(ServerObject server)
        {
            _servers.Add(server);
        }

        public void AddUsers(List<UserObject> users)
        {
            _users.AddRange(users);
        }

        public void AddNodes(List<NodeObject> nodes)
        {
            _nodes.AddRange(nodes);
        }

        public void AddServers(List<ServerObject> servers)
        {
            _servers.AddRange(servers);
        }

        public void DistrebuteInternetObject(InternetObject item)
        {
            if (item is UserObject userObject)
                AddUser(userObject);
            if (item is NodeObject nodeObject)
                AddNode(nodeObject);
            if (item is ServerObject serverObject)
                AddServer(serverObject);
        }

        public void DistrebuteInternetObjects(List<InternetObject> internetObjects)
        {
            foreach (var item in internetObjects)
            {
                DistrebuteInternetObject(item);
            }
        }

        public class UserObjectPerDistanceToServer
        {
            public UserObject UserObject { get; set; }
            public float Distance { get; set; } 

            public UserObjectPerDistanceToServer()
            {

            }

            public UserObjectPerDistanceToServer(UserObject userObject, float distance)
            {
                UserObject = userObject;
                Distance = distance;
            }
        }


    }
}