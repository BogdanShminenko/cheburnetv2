﻿using Assets.Scripts.Game;
using Assets.Scripts.MVC.MenuMVC.View;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Assets.Scripts.MVC.MenuMVC
{
    public class MenuController : MonoBehaviour
    {
        [Inject] private GameSettings _gameSettings;
        [Inject] private PrivacyPolicyView _privacyPolicyView;

        public void PlayWithSave()
        {
            _gameSettings.IsStartWithSave = true;
            SceneManager.LoadScene("Game");
        }

        public void PlayWithoutSave()
        {
            _gameSettings.IsStartWithSave = false;
            SceneManager.LoadScene("Game");
        }


        public void OpenURLPrivacyPolicy()
        {
            Application.OpenURL("https://noesis.games/confidentiality");
        }
    }
}