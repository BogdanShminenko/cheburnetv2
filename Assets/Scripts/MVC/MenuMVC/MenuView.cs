﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MVC.MenuMVC
{
    public class MenuView : MonoBehaviour
    {
        [SerializeField] private Button _continueButton;
        [SerializeField] private GameObject _startWitoutSavePanel;
        [SerializeField] private GameObject _startGamePanel;
        [SerializeField] private GameObject _aboutGame;

        private void Start()
        {
            if (PlayerPrefs.HasKey("IsHaveSave"))
            {
                _continueButton.gameObject.SetActive(true);
            }
            else
            {
                _continueButton.gameObject.SetActive(false);
            }
        }

        public void OpenAboutGamePanel()
        {
            _aboutGame.SetActive(true);
        }

        public void CloseAboutGamePanel()
        {
            _aboutGame.SetActive(false);
        }


        public void StartNewGame()
        {
            if (PlayerPrefs.HasKey("IsHaveSave"))
            {
                _startWitoutSavePanel.SetActive(true);
            }
            else
            {
                _startGamePanel.SetActive(true);
            }
        }

    }
}