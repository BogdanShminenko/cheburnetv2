﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using State = AK.Wwise.State;

namespace Assets.Scripts.MVC.PauseMenuMVC
{
    public class SoundButton : PauseMenuButton
    {
        [SerializeField] private string _key = "IsOnSound";
        [SerializeField] private Image _image;
        [SerializeField] private Sprite _onSprite;
        [SerializeField] private Sprite _offSprite;

        [SerializeField] private bool _isOn = true;
        
        private void Awake()
        {
            if (PlayerPrefs.HasKey(_key))
            {
                if(PlayerPrefs.GetInt(_key) == 0)
                {
                    _image.sprite = _offSprite;
                    _isOn = false;
                }
                else
                {
                    _image.sprite = _onSprite;
                    _isOn = true;
                }
            }
        }

        public override void Click()
        {
            if (_isOn)
            {
                _image.sprite = _offSprite; 
                _isOn = false;
                PlayerPrefs.SetInt(_key, 0);
            }
            else
            {
                _image.sprite = _onSprite;
                _isOn = true;
                PlayerPrefs.SetInt(_key, 1);
            }
            
            base.Click();
        }
    }
}