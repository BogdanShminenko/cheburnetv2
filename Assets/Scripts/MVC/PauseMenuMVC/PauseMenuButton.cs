﻿using Assets.Scripts.MVC.SaveSystemMVC;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Assets.Scripts.MVC.PauseMenuMVC
{
    public class PauseMenuButton : MonoBehaviour
    {
        public event System.Action OnClick;

        public virtual void Click()
        {
            OnClick?.Invoke();
        }

    }
}