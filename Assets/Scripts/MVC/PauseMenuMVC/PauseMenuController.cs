﻿using Assets.Scripts.Game;
using Assets.Scripts.MVC.LawsMVC.LawViews;
using Assets.Scripts.MVC.ProjectMVC;
using Assets.Scripts.MVC.TimerMVC;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.PauseMenuMVC
{
    public class PauseMenuController : MonoBehaviour
    {
        [Inject] private TimerModel _timerModel;
        [Inject] private GameLoop _gameLoop;
        [Inject] private ProjectModel _projectModel;
        [Inject] private UnsuccessfulState _unsuccessfulState;

        public void PauseGame()
        {
            _timerModel.PauseTimer();
            _gameLoop.Pause();
            foreach(var item in _projectModel.DevelopProcesses)
                item.Pause();
            _unsuccessfulState.Pause();
        }

        public void ContinueGame()
        {
            _timerModel.Continue();
            _gameLoop.Continue();
            foreach (var item in _projectModel.DevelopProcesses)
                item.Continue();
            _unsuccessfulState.Continue();
        }
    }
}