﻿using Assets.Scripts.GameStates;
using System.Collections;
using UnityEngine;
using Zenject;
using Zenject.Asteroids;

namespace Assets.Scripts.MVC.PauseMenuMVC
{
    public class PauseMenuView : MonoBehaviour
    {
        [SerializeField] private GameObject _panel;
        [Inject] private GameState _gameState;
        [Inject] private PauseMenuController _pauseMenuController;

        public void OpenPanel()
        {
            _gameState.ChangeGameState(GameStates.GameStates.Pause);
            _panel.SetActive(true);
            _pauseMenuController.PauseGame();
        }

        public void ClosePanel()
        {
            _gameState.ChangeGameState(GameStates.GameStates.InternetMap);
            _panel.SetActive(false);
            _pauseMenuController.ContinueGame();
        }

    }
}