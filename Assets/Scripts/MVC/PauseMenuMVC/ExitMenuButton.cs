using Assets.Scripts.MVC.PauseMenuMVC;
using Assets.Scripts.MVC.SaveSystemMVC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class ExitMenuButton : PauseMenuButton
{
    [Inject] private SaveSystemController _saveSystemController;

    public override void Click()
    {
        if (_saveSystemController != null)
            _saveSystemController.CallSave();
        SceneManager.LoadScene(0);
        base.Click();
    }
}
