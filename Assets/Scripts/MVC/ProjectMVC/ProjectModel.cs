﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.Game;
using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Assets.Scripts.MVC.ProjectMVC
{
    public sealed class ProjectModel : MonoBehaviour
    {
        [Inject] private DevelopProcess _developProcessPrefab;
        [SerializeField] private List<Project> _projects = new List<Project>();
        private List<DevelopProcess> _developProcessList = new List<DevelopProcess>();

        public event System.Action<Project> OnSelectedProject;
        public event System.Action OnProjectsInited;

        public IEnumerable<Project> Projects => _projects;
        public IEnumerable<DevelopProcess> DevelopProcesses => _developProcessList;

        [Inject] private GameSettingsData _gameSettingsData;
        [SerializeField] private DateTriggerConventer _dateTriggerConventer;
        [SerializeField] private GameInfo _gameInfo;

        public void SetProjects(List<Project> projects)
        {
            _projects = projects;
            OnProjectsInited?.Invoke();
        }

        public void InitProjects(IEnumerable<ProjectData> projectDatas)
        {
            _projects.Clear();
            List<ProjectData> projects = new List<ProjectData>(projectDatas);
            projects.Reverse();
            foreach(var projectData in projects)
            {
                Project project = new Project(_gameInfo, _dateTriggerConventer);
                project.InitProject(projectData);
                _projects.Add(project);
            }
            OnProjectsInited?.Invoke();
        }

        public void InitProjects(IEnumerable<Project> projects)
        {
            _projects.Clear();
            _projects.AddRange(projects);
            OnProjectsInited?.Invoke();
        }


        public void SetCurrentProject(Project project)
        {
            OnSelectedProject?.Invoke(project);
        }

        public bool TryGetProjectByID(string id, out Project project)
        {
            project = _projects.Where(item => item.ProjectID == id).FirstOrDefault();
            if (project != null)
            {
                return true;
            }
            return false;
        }

        public bool TryGetDevelopProcess(Project project, out DevelopProcess developProcess)
        {
            developProcess = _developProcessList.Where(item => item.CurrentProject == project).FirstOrDefault();
            if(developProcess != null)
            {
                return true;
            }
            return false;
        }

        public void EndDevelop(DevelopProcess developProcess)
        {
            _developProcessList?.Remove(developProcess);
            developProcess.CurrentProject.ChangeProjectState(ProjectState.ReadyToInplements);
            Destroy(developProcess.gameObject);
        }

        public bool CheckToTriggerProjects(int currentYear, int currentMounth)
        {
            for(int i = 0; i < _projects.Count; i++)
            {
                if (!_projects[i].IsTriggeredByDate && _projects[i].TryTriggerProjectByDate(currentYear, currentMounth))
                {
                    _projects[i].ChangeProjectState(ProjectState.NotEnoughResourcesToDevelop);
                    _projects[i].IsTriggeredByDate = true;
                    return true;
                }
            }
            return false;
        }

        public bool TryTriggerBySignal()
        {
            for (int i = 0; i < _projects.Count; i++)
            {
                if (_projects[i].IsTriggeredByDate && !_projects[i].IsTriggeredBySignal && CheckTriggerByTriggerSignal(_projects[i].TriggersSignal))
                {
                    _projects[i].ChangeProjectState(ProjectState.ReadyToDevelop);
                    _projects[i].IsTriggeredBySignal = true;
                    return true;
                }
            }
            return false;
        }

        private bool CheckTriggerByTriggerSignal(IReadOnlyCollection<string> triggersSignals)
        {
            int count = 0;
            foreach (string signal in triggersSignals)
            {
                if (_gameInfo.CheckTrigger(signal) || signal == "null")
                {
                    count++;
                }
            }
            if (count == triggersSignals.Count)
            {
                return true;
            }
            return false;
        }

        public DevelopProcess StartDevelopProcess(Project project)
        {
            DevelopProcess developProcess = Instantiate(_developProcessPrefab);
            developProcess.InitProcess(project, _gameSettingsData);
            _developProcessList.Add(developProcess);
            developProcess.OnEndedDevelop += EndDevelop;
            project.ChangeProjectState(ProjectState.InProcess);
            return developProcess;
        }
        public DevelopProcess StartDevelopProcessFromSave(Project project, float seconds, float startSeconds)
        {
            DevelopProcess developProcess = Instantiate(_developProcessPrefab);
            developProcess.InitProcessFromSave(project, _gameSettingsData, seconds, startSeconds);
            _developProcessList.Add(developProcess);
            developProcess.OnEndedDevelop += EndDevelop;
            project.ChangeProjectState(ProjectState.InProcess);
            return developProcess;
        }


    }
}