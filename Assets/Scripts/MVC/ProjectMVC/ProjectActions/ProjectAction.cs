﻿using Assets.Scripts.Data.GameData;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.ProjectMVC.ProjectActions
{
    public class ProjectAction 
    {
        public ActionGrayCase NotAwailableDevelopAction;
        public ActionYellowCase AvailableToDevelopAction;
        public ActionBlueCase AvailableToDevelopAndBuyAction;
        public ActionGreenCase AvailableToImplementAction;
        public ActionCaseProcess ProcessAction;
        public ActionCompleteCase CompleteAction;
        public bool IsCanBuyProject { get; private set; }

        public void InitProjectAction(ProjectActionsData projectActionsData)
        {
            NotAwailableDevelopAction = new ActionGrayCase();
            NotAwailableDevelopAction.Text = projectActionsData.ActionGreenCase.Text;

            if (projectActionsData.ActionYellowCase != null && projectActionsData.ActionYellowCase.Text.Length > 0)
            {
                AvailableToDevelopAction = new ActionYellowCase();
                AvailableToDevelopAction.Text = projectActionsData.ActionYellowCase.Text;
                AvailableToDevelopAction.Button = projectActionsData.ActionYellowCase.Button;
                AvailableToDevelopAction.Price = projectActionsData.ActionYellowCase.Price;
                AvailableToDevelopAction.TimeYear = projectActionsData.ActionYellowCase.TimeYear;
                AvailableToDevelopAction.TimeMonth = projectActionsData.ActionYellowCase.TimeMonth;
                AvailableToDevelopAction.Result = projectActionsData.ActionYellowCase.Result;
            }
            if (projectActionsData.ActionBlueCase != null && projectActionsData.ActionBlueCase.Text.Length > 0)
            {
                AvailableToDevelopAndBuyAction = new ActionBlueCase();
                AvailableToDevelopAndBuyAction.Text = projectActionsData.ActionBlueCase.Text;
                AvailableToDevelopAndBuyAction.Button = projectActionsData.ActionBlueCase.Button;
                AvailableToDevelopAndBuyAction.DevelopPrice = projectActionsData.ActionBlueCase.DevelopPrice;
                AvailableToDevelopAndBuyAction.BuyPrice = projectActionsData.ActionBlueCase.BuyPrice;
                AvailableToDevelopAndBuyAction.TimeYear = projectActionsData.ActionBlueCase.TimeYear;
                AvailableToDevelopAndBuyAction.TimeMonth = projectActionsData.ActionBlueCase.TimeMonth;
                AvailableToDevelopAndBuyAction.Result = projectActionsData.ActionBlueCase.Result;
                IsCanBuyProject = true;
            }

            AvailableToImplementAction = new ActionGreenCase();
            AvailableToImplementAction.Text = projectActionsData.ActionGreenCase.Text;
            AvailableToImplementAction.Button = projectActionsData.ActionGreenCase.Button;
            AvailableToImplementAction.Result = projectActionsData.ActionGreenCase.Result;

            ProcessAction = new ActionCaseProcess();
            ProcessAction.Text = projectActionsData.ProcessActionCase.Text;
            ProcessAction.PBar = projectActionsData.ProcessActionCase.PBar;
            ProcessAction.TimeYear = projectActionsData.ProcessActionCase.TimeYear;
            ProcessAction.TimeMonth = projectActionsData.ProcessActionCase.TimeMonth;
            ProcessAction.Result= projectActionsData.ProcessActionCase.Result;

            CompleteAction = new ActionCompleteCase();
            CompleteAction.Text = projectActionsData.ActionCompleteCase.Text;
            CompleteAction.Result = projectActionsData.ActionCompleteCase.Result;
    }
    }

    [Serializable]
    public class ActionGrayCase
    {
        public string Text;
    }
    [Serializable]
    public class ActionYellowCase
    {
        public string Text;
        public string Button;
        public int Price;
        public int TimeYear;
        public int TimeMonth;
        public string[] Result;
    }
    [Serializable]
    public class ActionCaseProcess
    {
        public string Text;
        public string PBar;
        public int TimeYear;
        public int TimeMonth;
        public string[] Result;
    }
    [Serializable]
    public class ActionBlueCase
    {
        public string Text;
        public string Button;
        public int BuyPrice;
        public int DevelopPrice;
        public int TimeYear;
        public int TimeMonth;
        public string[] Result;
    }
    [Serializable]
    public class ActionGreenCase
    {
        public string Text;
        public string Button;
        public int Result;
    }
    [Serializable]
    public class ActionCompleteCase
    {
        public string Text;
        public string[] Result;
    }
}