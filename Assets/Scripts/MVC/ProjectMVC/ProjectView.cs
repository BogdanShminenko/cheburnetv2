﻿using Assets.Scripts.GameStates;
using Assets.Scripts.MVC.ProjectMVC.Views;
using Assets.Scripts.TriggersAndOperationsTypes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TechnoBabelGames;
using UIWidgets;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Zenject;

namespace Assets.Scripts.MVC.ProjectMVC
{
    public class ProjectsPack
    {
        private List<Project> _projects = new List<Project>();

        public IReadOnlyList<Project> Projects => _projects;

        public void AddProject(Project project)
        {
            _projects.Add(project);
        }
        
        public void AddProjects(List<Project> projects)
        {
            _projects.AddRange(projects);
        }
        
    }

    public class ProjectTreeItemProsition
    {
        public ProjectTreeItem ProjectTreeItem { get; set; }
        public Vector3 Position { get; set; }

        public ProjectTreeItemProsition(ProjectTreeItem projectTreeItem, Vector3 position)
        {
            ProjectTreeItem = projectTreeItem;
            Position = position;
        }
    }

    public sealed class ProjectView : MonoBehaviour
    {
        [field: SerializeField] private List<Project> _projects = new List<Project>();
        [SerializeField] private DynamicScrollFitter _scrollFitter;
        [SerializeField] private RectTransform _startPoint;
        [SerializeField] private float _widhtProject;
        [SerializeField] private float _horizontalSpasing;
        [SerializeField] private float _verticalSpasing;
        [SerializeField] private ProjectLineSettings _projectLineSettings;
        [SerializeField] private GameObject _upperProjectsPanel;
        [SerializeField] private Canvas _rootCanvas;
        [SerializeField] private ScrollRect _scrollRect;
        [SerializeField] private float _thickness = 3;
        [SerializeField] private ConnectorType _connectorType;
        [SerializeField] private Transform _treeParent;
        [SerializeField] private GameObject _panel;
        [SerializeField] private GameObject _notification;
        [SerializeField] private Scrollbar _scrollbar;
        [SerializeField] private ProjectModel _projectModel;
        [SerializeField] private ProjectsItemsLine _projectsItemsLine;
        [SerializeField] private ProjectTreeItem _projectTreeItemPrefab;
        [SerializeField] private Line _line;
        [SerializeField] private ProjectController _projectController;

        [Inject] private ProjectLine _projectLinePrefab;
        [Inject] private ProjectInfoPanel _projectInfoPanel;
        [Inject] private GameState _gameState;
        [Inject] private ButtonsMenu _buttonsMenu;

        [SerializeField] private List<ProjectTreeItem> _projectTreeItems = new List<ProjectTreeItem>();
        private List<ProjectTreeItem> _projectTreeItemToConnect = new List<ProjectTreeItem>();
        [SerializeField] private List<Line> _lines = new List<Line>();
        private float _yPosition;
        public bool NotificationStatus => _notification.activeSelf;

        public IEnumerable<ProjectTreeItem> ProjectTreeItems => _projectTreeItems;

        public void OnEnable()
        {
            _projectModel.OnSelectedProject += OpenProjectInfoPanel;
        }

        public void OnDisable()
        {
            _projectModel.OnSelectedProject -= OpenProjectInfoPanel;
        }

        public void SetNotificationStatus(bool notificationStatus)
        {
            _notification.SetActive(notificationStatus);
        }

        private void Awake()
        {
            _panel.SetActive(true);
            //Canvas.ForceUpdateCanvases();
            //UIX.UpdateLayout(_scrollRect.transform);
            //_scrollRect.verticalNormalizedPosition = 0;
            _panel.SetActive(false);
        }

        public void OpenTree()
        {
            _gameState.ChangeGameState(GameStates.GameStates.Projects);
            _notification.SetActive(false);
            _upperProjectsPanel.SetActive(true);
            _panel.SetActive(true);
        }

        public void CloseTreeWithChangeGameState()
        {
            _gameState.ChangeGameState(GameStates.GameStates.InternetMap);
            CloseTree();
        }

        public void CloseTree()
        {
            _upperProjectsPanel.SetActive(false);
            _panel.SetActive(false);
            _buttonsMenu.ResetButtonsWithOutOffPanels();
        }

        public void OnNotification(Project project)
        {
            if ((project.CurrentProjectState == ProjectState.ReadyToInplements ||
                project.CurrentProjectState == ProjectState.ReadyToDevelop) && !_panel.activeSelf)
            {
                _notification.SetActive(true);
            }

        }

        public void OnNotification()
        {
            if(!_panel.activeSelf)
                _notification.SetActive(true);
        }

        public void UpdateLines()
        {
            foreach(var line  in _lines)
            {
                line.SetProjectLineSettings(_projectLineSettings);
                line.UpdateLineSettings();
            }
        }

        public void OpenProjectInfoPanel(Project project)
        {
            _projectInfoPanel.SetCurrentProject(project);
            _projectInfoPanel.OpenProjectInfoPanel();
        }

        private List<Project> GetProjectsByDate(DateTrigger dateTrigger, List<Project> projects)
        {
            return projects.Where(item => item.DateTrigger.Year == dateTrigger.Year &&
                    item.DateTrigger.Month == dateTrigger.Month).ToList();
        }

        private void SetupConnections()
        {
            foreach(var item in _projectTreeItems)
            {
                ConnectByTrigger(item, item.Project.ResultTriggerComplete);
                ConnectByTrigger(item, item.Project.ResultTriggerImplemented);
            }
            _scrollFitter.UpdateSize();
            Debug.Log("End Setup connections");
        }



        public Vector2 GetChildPosition(RectTransform rectTransformGrid , RectTransform rectTransformChild)
        {
            var localPositionGrid = (Vector2)rectTransformGrid.localPosition;
            var sizeDeltaGrid = rectTransformGrid.sizeDelta;
            var deltaGridFromCenterToLeftTop = new Vector2(-0.5f * sizeDeltaGrid.x, 0.5f * sizeDeltaGrid.y);

            var anchoredPositionChild = rectTransformChild.anchoredPosition;
            var childPosition = localPositionGrid + anchoredPositionChild + deltaGridFromCenterToLeftTop;
            return childPosition;
        }

        private bool CheckProjectItemsToConnetionInLines(ProjectTreeItem firstProjectTreeItem, ProjectTreeItem secondProjectTreeItem)
        {
            foreach(var item in _lines)
            {
                if(item != null && item.ProjectsTreeItems.Contains(firstProjectTreeItem) && item.ProjectsTreeItems.Contains(secondProjectTreeItem))
                {
                    return true;
                }
            }
            return false;
        }

        private void ConnectByTrigger(ProjectTreeItem projectTreeItem , string trigger)
        {
            if (TryGetProjectTreeItem(trigger, out List<ProjectTreeItem> projectItems))
            {
                foreach (var projectItem in projectItems)
                {
                    if(!CheckProjectItemsToConnetionInLines(projectTreeItem, projectItem))
                    {

                        Vector3 posEndPoint = Camera.main.WorldToViewportPoint(Camera.main.ViewportToWorldPoint(projectItem.RectTransform.position));
                        Vector3 posStartPoint = Camera.main.WorldToViewportPoint(Camera.main.ViewportToWorldPoint(projectTreeItem.RectTransform.position));

                        Line line = Instantiate(_line, posStartPoint, Quaternion.identity, projectTreeItem.transform);
                        line.SetProjectLineSettings(_projectLineSettings);
                        line.UpdateLineSettings();
                        line.SetPoints(posStartPoint, posEndPoint);
                        line.SetProjectTreeItems(projectItem, projectTreeItem);
                        _lines.Add(line);
                    }
                }
            }
        }

        private bool TryGetProjectTreeItem(string trigger , out List<ProjectTreeItem> projectTreeItems)
        {
            projectTreeItems = new List<ProjectTreeItem>();

            projectTreeItems = _projectTreeItems.Where(item => item.Project.TriggersSignal.Contains(trigger)).ToList();

            if(projectTreeItems.Count > 0)
            {
                return true;
            }
            return false;
        }

        public void InitProjectsGame()
        {
            foreach(var item in _projectTreeItems)
            {
                item.SubscribeOnEvents();
            }            
        }

        public void CallCreateProjects()
        {
            Invoke(nameof(CreateProjectTree), 1f);
        }

        public void UpdateCreatedProjects()
        {
            foreach(var item in _projectTreeItems)
            {
                if (item.Project != null && _projectModel.TryGetProjectByID(item.Project.ProjectID, out Project project))
                {
                    item.InitProject(project, _projectModel);
                }
            }
        }

        public void ProjectTreeItemsSubscribeOnProjects()
        {
            foreach (var item in _projectTreeItems)
                item.SubscribeOnEvents();
        }

        private List<Vector3> GetPositionsForPackHorizontal(ProjectsPack projectsPack,float yPosition)
        {
            List <Vector3> positions = new List<Vector3>();
            if (projectsPack.Projects.Count != 1)
            {
                if(projectsPack.Projects.Count % 2 == 0)
                {
                    List<Vector3> positionsPositive = new List<Vector3>();
                    float lastXPoint = 0;
                    for(int i = 0; i < projectsPack.Projects.Count / 2; i++)
                    {
                        float xPosition = lastXPoint + _horizontalSpasing + (_widhtProject / 2);
                        lastXPoint += xPosition;
                        positionsPositive.Add(new Vector3(xPosition, yPosition, 0));
                    }
                    foreach(var item in positionsPositive)
                    {
                        positions.Add(item);
                        positions.Add(new Vector3(item.x * -1 ,item.y,0));
                    }
                }
                else
                {
                    List<Vector3> positionsPositive = new List<Vector3>();
                    positions.Add(new Vector3(0, yPosition, 0));
                    float lastXPoint = _horizontalSpasing + (_widhtProject / 2);
                    for (int i = 0; i < (projectsPack.Projects.Count - 1) / 2; i++)
                    {
                        float xPosition = lastXPoint + _horizontalSpasing + (_widhtProject / 2);
                        lastXPoint += xPosition;
                        positionsPositive.Add(new Vector3(xPosition, yPosition, 0));
                    }
                    foreach (var item in positionsPositive)
                    {
                        positions.Add(item);
                        positions.Add(new Vector3(item.x * -1, item.y, 0));
                    }
                }
            }
            else
            {
                positions.Add(new Vector3(0, yPosition,0));
            }

            return positions;
        }

        public void CreateProjectTree()
        {
            _projectTreeItems = _projectTreeItems.Where(x => x != null).ToList();
            _lines = _lines.Where(x => x != null).ToList();
            List<ProjectTreeItemProsition> projectTreeItemPrositions = new List<ProjectTreeItemProsition>();
            foreach (var item in _projectTreeItems)
            {
                Vector3 position = Camera.main.WorldToViewportPoint(Camera.main.ViewportToWorldPoint(item.RectTransform.position));
                ProjectTreeItemProsition projectTreeItemProsition = new ProjectTreeItemProsition(item, position);
                projectTreeItemPrositions.Add(projectTreeItemProsition);
            }
            UpdateCreatedProjects();
            _projects.Clear();
            float itemHeight = _projectTreeItemPrefab.GetComponent<RectTransform>().sizeDelta.y;
            List<string> createdProjects = new List<string>();
            if (_projectTreeItems.Count > 0 )
                createdProjects = new List<string>(_projectTreeItems.Where(x => x.Project != null).Select(item => item.Project.ProjectID));
            else
                _projectController.ResizeTreePanel();
            foreach(var item in _projectModel.Projects)
            {
                if (!createdProjects.Contains(item.ProjectID))
                {
                    _projects.Add(item);
                }
            }
            _projects = _projects.OrderByDescending(item => item.DateTrigger.DateTime).ToList();

            if (_projectTreeItems.Count > 0 && _projects.Count > 0)
            {
                float verticalToDown = _projects.Count * (_verticalSpasing);

                foreach (var item in _projectTreeItems)
                {
                    RectTransform rectTransform = item.GetComponent<RectTransform>();
                    Vector2 newPosition = rectTransform.anchoredPosition;
                    newPosition.y -= verticalToDown;
                    rectTransform.anchoredPosition = newPosition;
                }
            }
            List<ProjectsPack> projectsPacks = new List<ProjectsPack>();
            List<DateTrigger> handledDateTriggers = new List<DateTrigger>();

            foreach(var project in _projects)
            {
                if (!handledDateTriggers.Contains(project.DateTrigger))
                {
                    var projectPack = new ProjectsPack();
                    var sortedProjects = GetProjectsByDate(project.DateTrigger, _projects);
                    projectPack.AddProjects(sortedProjects);
                    handledDateTriggers.Add(project.DateTrigger);
                    projectsPacks.Add(projectPack);
                }
            }
            _yPosition = _treeParent.GetComponent<RectTransform>().anchoredPosition.y - itemHeight;


            foreach(var item in projectsPacks)
            {
                List<Vector3> positions = GetPositionsForPackHorizontal(item, _yPosition);

                for(int i = 0; i < positions.Count; i++)
                {
                    ProjectTreeItem projectTreeItem = Instantiate(_projectTreeItemPrefab, positions[i], Quaternion.identity);
                    projectTreeItem.transform.SetParent(_treeParent, false);
                    projectTreeItem.transform.localScale = new Vector3(1, 1, 1);
                    projectTreeItem.InitProject(item.Projects[i], _projectModel);
                    _projectTreeItems.Add(projectTreeItem);
                }
                _yPosition -= _verticalSpasing;
                //projectItemsLine.CallOffGrid();
            }

            //foreach(var item in projectTreeItemPrositions)
            //{
            //    item.ProjectTreeItem.transform.position = item.Position;
            //}

            Invoke(nameof(CallSetupConnections), 1f);
        }

        private void CallSetupConnections()
        {
            //_treeParent.gameObject.GetComponent<GridLayoutGroup>().enabled = false;
            //_treeParent.gameObject.GetComponent<ContentSizeFitter>().enabled = false;
            SetupConnections();
        }
    }
}