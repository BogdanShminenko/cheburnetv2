﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Data.GameData.Configs;
using Assets.Scripts.Data.GameData.FileLoaders;
using Assets.Scripts.Game;
using Assets.Scripts.MVC.EventMVC;
using Assets.Scripts.MVC.ProjectMVC.Views;
using Assets.Scripts.MVC.ServerMVC;
using Assets.Scripts.MVC.TimerMVC;
using System.Collections;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.ProjectMVC
{
    public class ProjectController : MonoBehaviour
    {
        [SerializeField] private ProjectModel _projectModel;
        [SerializeField] private DynamicScrollFitter _scrollFitter;
        [SerializeField] private ProjectConfig _projectConfig;
        [SerializeField] private TimerModel _timerModel;
        [SerializeField] private ProjectView _projectView;
        [SerializeField] private GameInfo _gameInfo;
        [Inject] private AccuralInLoop _accuralInLoop;
        [SerializeField] private ConfigFileReader _configFileReader;

        public void InitProjects()
        {
            _projectModel.InitProjects(_projectView.ProjectTreeItems.Select(item => item.Project));
            PrepareProjects();
            _projectView.InitProjectsGame();
        }

        public void UpdateLines()
        {
            _projectView.UpdateLines();
        }

        public void LoadProjects()
        {
            _configFileReader.LoadProjects();
            _projectModel.InitProjects(_projectConfig.Projects);
            _projectView.CallCreateProjects();
        }

        public void ResizeTreePanel()
        {
            _scrollFitter.UpdateSize();
        }

        public void PrepareProjects()
        {
            foreach (var project in _projectView.ProjectTreeItems)
            {
                project.OnClick += TryOpenProject;
                project.Project.OnChangeProjectState += _projectView.OnNotification;
                project.Project.OnChangeProjectState += ChangeProjectStateHandler;
            }
            foreach (var project in _projectModel.Projects)
                project.ChangeProjectState(project.CurrentProjectState);
        }

        public void ChangeProjectStateHandler(Project project)
        {
            if(project.CurrentProjectState == ProjectState.ReadyToInplements)
            {
                _gameInfo.AddTrigger(project.ResultTriggerComplete);
            }
            else if(project.CurrentProjectState == ProjectState.Researched)
            {
                _gameInfo.AddTrigger(project.ResultTriggerImplemented);

                if (project.ResultRebel != null)
                    _accuralInLoop.HandleAttitudeResult(project.ResultRebel);
                if (project.ResultLoyal != null)
                    _accuralInLoop.HandleAttitudeResult(project.ResultLoyal);
                if (project.ResultControl != null)
                    _accuralInLoop.HandleAttitudeResult(project.ResultControl);
            }
        }

        public void TryOpenProject(ProjectTreeItem project)
        {
            if(project.Project.IsTriggeredByDate)
                _projectModel.SetCurrentProject(project.Project);
        }

        public void CallTriggerProjects()
        {
            if (_projectModel.CheckToTriggerProjects((int)_timerModel.CurrentYear, (int)_timerModel.CurrentMonth))
            {
                _projectView.OnNotification();
            }
            if (_projectModel.TryTriggerBySignal())
            {
                _projectView.OnNotification();
            }
        }


    }
}