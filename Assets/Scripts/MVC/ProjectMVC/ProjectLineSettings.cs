﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.ProjectMVC
{
    [CreateAssetMenu(fileName = "Project line settings" , menuName = "ScriptableObjects/ProjectLineSettings")]
    public class ProjectLineSettings : ScriptableObject
    {
        [SerializeField] private Gradient _gradient;
        [SerializeField] private AnimationCurve _widthCurve;
        [SerializeField] private Texture[] _textures;
        [SerializeField] private int _minTime;
        [SerializeField] private int _maxTime;
        [SerializeField] private float _frameTime;

        public Gradient Gradient { get { return _gradient; } }
        public AnimationCurve WidthCurve { get { return _widthCurve; } }
        public Texture[] Textures { get { return _textures; } }
        public int MinTime { get { return _minTime; } }
        public int MaxTime { get { return _maxTime;} }
        public float FrameTime { get { return _frameTime; } }

    }
}