﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.MVC.ProjectMVC.Views.InfoStatePanels
{
    public class NotAvailableResearchState : MonoBehaviour, IProjectState
    {
        [SerializeField] private GameObject _panel;
        [SerializeField] private TMP_Text _text;

        public void EnterInState(Project project)
        {
            _text.text = project.TextDisabled;
            _panel.SetActive(true);
        }

        public void ExitFromState()
        {
            _panel.SetActive(false);
        }
    }
}