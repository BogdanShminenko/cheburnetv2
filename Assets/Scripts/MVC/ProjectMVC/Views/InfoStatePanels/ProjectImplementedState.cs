﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.MVC.ProjectMVC.Views.InfoStatePanels
{
    public class ProjectImplementedState : MonoBehaviour, IProjectState
    {
        [SerializeField] private GameObject _panel;
        [SerializeField] private TMP_Text _text;
        public Project _currentProject;

        public void Inplement()
        {
            _currentProject.ChangeProjectState(ProjectState.Researched);
        }

        public void EnterInState(Project project)
        {
            _currentProject = project;
            _text.text = project.TextComplete;
            _panel.SetActive(true);
        }

        public void ExitFromState()
        {
            _panel.SetActive(false);
        }
    }
}