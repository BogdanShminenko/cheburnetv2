﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.ProjectMVC.Views.InfoStatePanels
{
    public interface IProjectState
    {
        void EnterInState(Project project);
        void ExitFromState();
    }
}