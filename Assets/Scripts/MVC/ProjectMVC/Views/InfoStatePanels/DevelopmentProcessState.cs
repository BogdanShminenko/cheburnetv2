﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Event = AK.Wwise.Event;

namespace Assets.Scripts.MVC.ProjectMVC.Views.InfoStatePanels
{
    public class DevelopmentProcessState : MonoBehaviour, IProjectState
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public Event _progressPanelHidedEvent;
        }
        
        [SerializeField] private GameObject _panel;
        [SerializeField] private TMP_Text _text;
        [SerializeField] private Image _loadBar;
        [SerializeField] private WwiseSound _wwiseSound;
        
        [Inject] private ProjectModel _projectModel;

        private Project _currentProject;
        private DevelopProcess _currentProcess;

        public void EnterInState(Project project)
        {
            _currentProject = project;
            _text.text = project.TextProcess;
            if(_projectModel.TryGetDevelopProcess(_currentProject , out _currentProcess))
            {
                _currentProcess.OnTickSeconds += UpdateLoadBar;
                _currentProcess.OnEndedDevelop += EndDevelop;
            }
            _loadBar.fillAmount = 1;
            _panel.SetActive(true);
        }

        public void UpdateLoadBar(float seconds)
        {
            _loadBar.fillAmount = (float)seconds / (float)_currentProcess.StartSeconds;
        }

        public void EndDevelop(DevelopProcess developProcess)
        {
            _currentProcess.OnEndedDevelop -= EndDevelop;
            _currentProcess.OnTickSeconds -= UpdateLoadBar;
            _currentProcess = null;
        }

        public void ExitFromState()
        {
            if(_currentProcess != null)
            {
                _currentProcess.OnEndedDevelop -= EndDevelop;
                _currentProcess.OnTickSeconds -= UpdateLoadBar;
            }
            _panel.SetActive(false);
            _wwiseSound._progressPanelHidedEvent.Post(gameObject);
        }
    }
}