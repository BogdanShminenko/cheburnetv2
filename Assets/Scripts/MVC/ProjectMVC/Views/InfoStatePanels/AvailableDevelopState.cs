﻿using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.MVC.ProjectMVC.ProjectActions;
using Assets.Scripts.Utilities;
using System.Collections;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.ProjectMVC.Views.InfoStatePanels
{
    public class AvailableDevelopState : MonoBehaviour, IProjectState
    {
        [SerializeField] private TMP_Text _descriptionText;
        [SerializeField] private TMP_Text _developPriceText;
        [SerializeField] private GameObject _panel;

        [Inject] private DateToStringConverter _dateToStringConverter;
        [Inject] private MoneyModel _moneyModel;
        [Inject] private CurrencyController _currencyController;
        [Inject] private ProjectModel _projectModel;

        private Project _currentProject;

        public void EnterInState(Project project)
        {
            _currentProject = project;
            //_currentAction = project.ProjectAction.AvailableToDevelopAction;
            _descriptionText.text = _currentProject.Description;
            _developPriceText.text = $"Запустить разроботку \n {_dateToStringConverter.ConvertDate(_currentProject.Years, _currentProject.Month)}  {_currentProject.PriceDevelop}$";
            _panel.SetActive(true);
        }

        public void Develop()
        {

            if (_currentProject != null)
            {
                if(_moneyModel.CurrentAmount >= _currentProject.PriceDevelop)
                {
                    _currencyController.SpendMoney(_currentProject.PriceDevelop);
                    _projectModel.StartDevelopProcess(_currentProject);
                }
            }
        }

        public void ExitFromState()
        {
            _panel.SetActive(false);
        }
    }
}