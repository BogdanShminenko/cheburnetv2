﻿using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.MVC.ProjectMVC.ProjectActions;
using Assets.Scripts.MVC.ServerMVC;
using Assets.Scripts.Utilities;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Event = AK.Wwise.Event;

namespace Assets.Scripts.MVC.ProjectMVC.Views.InfoStatePanels
{
    public class AvailableDevelopAndBuyState : MonoBehaviour, IProjectState
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public Event _clickEvent;
            public Event _disabledEvent;
        }
        
        [SerializeField] private Sprite _activeImage;
        [SerializeField] private Sprite _inactiveImage;
        [SerializeField] private GameObject _buyButton;
        [SerializeField] private GameObject _developButton;
        [SerializeField] private TMP_Text _descriptionText;
        [SerializeField] private TMP_Text _developPriceText;
        [SerializeField] private TMP_Text _developInfoText;
        [SerializeField] private TMP_Text _buyInfoText;
        [SerializeField] private TMP_Text _buyPriceText;
        [SerializeField] private GameObject _panel;

        [SerializeField] private WwiseSound _wwiseSound;
        
        [Inject] private DateToStringConverter _dateToStringConverter;
        [Inject] private MoneyModel _moneyModel;
        [Inject] private CurrencyController _currencyController;
        [Inject] private ProjectModel _projectModel;

        private Project _currentProject;

        public void EnterInState(Project project)
        {
            _descriptionText.text = project.TextAvailable;

            if(project.PriceBuy != 0)
            {
                _buyPriceText.text = project.PriceBuy.ToString();
                _buyInfoText.text = $"Купить готовую \n технологию";
                if (_moneyModel.CurrentAmount >= project.PriceBuy)
                {
                    _buyButton.GetComponent<Image>().sprite = _activeImage;
                }
                else
                {
                    _buyButton.GetComponent<Image>().sprite = _inactiveImage;
                }
                _buyButton.SetActive(true);
            }
            else
            {
                _buyButton.SetActive(false);
            }


            if (project.PriceDevelop != 0)
            {
                _developPriceText.text = project.PriceDevelop.ToString();
                _developInfoText.text = $"Запустить разработку \n {_dateToStringConverter.ConvertDate(project.Years, project.Month)}";
                if (_moneyModel.CurrentAmount >= project.PriceDevelop)
                {
                    _developButton.GetComponent<Image>().sprite = _activeImage;
                }
                else
                {
                    _developButton.GetComponent<Image>().sprite = _inactiveImage;
                }
                _developButton.SetActive(true);
            }
            else
            {
                _developButton.SetActive(false);
            }
            _moneyModel.OnCurrencyUpdate += UpdateButtonsStatus;
            _currentProject = project;
            _panel.SetActive(true);
        }

        private void UpdateButtonsStatus()
        {
            if (_developButton.activeSelf)
            {
                if (_moneyModel.CurrentAmount >= _currentProject.PriceDevelop)
                {
                    _developButton.GetComponent<Image>().sprite = _activeImage;
                }
                else
                {
                    _developButton.GetComponent<Image>().sprite = _inactiveImage;
                }
            }
            if (_buyButton.activeSelf)
            {
                if (_moneyModel.CurrentAmount >= _currentProject.PriceBuy)
                {
                    _buyButton.GetComponent<Image>().sprite = _activeImage;
                }
                else
                {
                    _buyButton.GetComponent<Image>().sprite = _inactiveImage;
                }
            }
        }

        public void Develop()
        {
            var isDeveloped = false;
            
            if (_currentProject != null)
            {
                if (_moneyModel.CurrentAmount >= _currentProject.PriceDevelop)
                {
                    _currencyController.SpendMoney(_currentProject.PriceDevelop);
                    _projectModel.StartDevelopProcess(_currentProject);
                    
                    _wwiseSound._clickEvent.Post(gameObject);
                    isDeveloped = true;
                }
            }
            
            if (!isDeveloped)
            {
                _wwiseSound._disabledEvent.Post(gameObject);
            }
        }

        public void Buy()
        {
            var isBuyed = false;
            
            if (_currentProject != null)
            {
                if (_moneyModel.CurrentAmount >= _currentProject.PriceBuy)
                {
                    _currencyController.SpendMoney(_currentProject.PriceBuy);
                    _currentProject.ChangeProjectState(ProjectState.ReadyToInplements);

                    _wwiseSound._clickEvent.Post(gameObject);
                    isBuyed = true;
                }
            }

            if (!isBuyed)
            {
                _wwiseSound._disabledEvent.Post(gameObject);
            }
        }

        public void ExitFromState()
        {
            _moneyModel.OnCurrencyUpdate -= UpdateButtonsStatus;
            _panel.SetActive(false);
        }
    }
}