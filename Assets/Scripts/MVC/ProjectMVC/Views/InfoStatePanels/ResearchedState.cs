﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.MVC.ProjectMVC.Views.InfoStatePanels
{
    public class ResearchedState : MonoBehaviour, IProjectState
    {
        [SerializeField] private GameObject _panel;
        [SerializeField] private TMP_Text _researchedText;

        public void EnterInState(Project project)
        {
            _researchedText.text = project.TextImplemented;
            _panel.SetActive(true);
        }

        public void ExitFromState()
        {
            _panel.SetActive(false);
        }
    }
}