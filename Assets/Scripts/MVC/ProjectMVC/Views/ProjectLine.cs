﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.ProjectMVC.Views
{
    [RequireComponent(typeof(LineRenderer))]
    public class ProjectLine : MonoBehaviour
    {
        [SerializeField] private LineRenderer _lineRenderer;
        private ProjectTreeItem _projectTreeItem1;
        private ProjectTreeItem _projectTreeItem2;

        private void Awake()
        {
            _lineRenderer = GetComponent<LineRenderer>();
        }

        public void InitProjectsItems(ProjectTreeItem projectTreeItem1, ProjectTreeItem projectTreeItem2)
        {
            _projectTreeItem1 = projectTreeItem1;
            _projectTreeItem2 = projectTreeItem2;
            UpdatePoints();
        }

        public void UpdatePoints()
        {
            RectTransform rectTransform1 = _projectTreeItem1.GetComponent<RectTransform>();
            RectTransform rectTransform2 = _projectTreeItem1.GetComponent<RectTransform>();
            Vector3 rect1 = rectTransform1.TransformPoint(rectTransform1.rect.center);
            Vector3 rect2 = rectTransform2.TransformPoint(rectTransform2.rect.center);
            SetPoints(rect1, rect2);
        }

        private void SetPoints(Vector3 start, Vector3 end)
        {
            _lineRenderer.SetPosition(0, start);
            _lineRenderer.SetPosition(1, end);
        }
    }
}