﻿using System.Collections;
using TMPro;
using UIWidgets;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.MVC.ProjectMVC.Views
{
    public class ProjectTreeItem : MonoBehaviour , IPointerClickHandler
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public AK.Wwise.Event _clickEvent;
        }

        [SerializeField] private Transform _position;
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private MultipleConnector _multipleConnector;
        [SerializeField] private Sprite _baseIcon;
        [SerializeField] private Sprite _orangeIcon;
        [SerializeField] private Sprite _orangeLoadIcon;
        [SerializeField] private Sprite _greenIcon;
        [SerializeField] private Sprite _blueIcon;
        [SerializeField] private GameObject _processBarBackGround;
        [SerializeField] private SpriteRenderer _icon;
        [SerializeField] private TMP_Text _name;
        [SerializeField] private SpriteRenderer _projectIcon;
        [SerializeField] private SpriteRenderer _notificationItem;
        [SerializeField] private FillEffect _processBar;
        [SerializeField] private GameObject _lock;
        [SerializeField] private ProjectModel _projectModel;
        public event System.Action<ProjectTreeItem> OnClick;
        private DevelopProcess _developProcess;
        public MultipleConnector MultipleConnector { get { return _multipleConnector; } }
        [field: SerializeField] public Project Project { get; private set; }
        public RectTransform RectTransform => _rectTransform;
        public Transform Position => _position;

        [SerializeField] private WwiseSound _wwiseSound;
        
        public void InitProject(Project project , ProjectModel projectModel)
        {
            _projectModel = projectModel;
            _name.text = project.ProjectName;
            Project = project;
            Project.LoadIcon();
            _icon.sprite = project.IconSprite;
            _icon.color = Color.white;
            ChangeStateHandler(Project);
        }

        public void SubscribeOnEvents()
        {
            Project.OnChangeProjectState += ChangeStateHandler;
            ChangeStateHandler(Project);
        }

        public void ChangeStateHandler(Project project)
        {
            if (Project.CurrentProjectState == ProjectState.InProcess)
                _processBarBackGround.SetActive(true);
            else
                _processBarBackGround.SetActive(false);

            if (Project.CurrentProjectState == ProjectState.Researched)
            {
                _projectIcon.sprite = _blueIcon;
                _lock.SetActive(false);
                _icon.gameObject.SetActive(true);
            }
            else if (Project.CurrentProjectState == ProjectState.NotTriggered)
            {
                _projectIcon.sprite = _baseIcon;
                _lock.SetActive(true);
                _icon.gameObject.SetActive(false);
            }
            else if (Project.CurrentProjectState == ProjectState.ReadyToDevelop)
            {
                _projectIcon.sprite = _orangeIcon;
                _lock.SetActive(false);
                _icon.gameObject.SetActive(true);
            }
            else if (Project.CurrentProjectState == ProjectState.ReadyToInplements)
            {
                _projectIcon.sprite = _greenIcon;
                _lock.SetActive(false);
                _icon.gameObject.SetActive(true);
            }
            else if (Project.CurrentProjectState == ProjectState.NotEnoughResourcesToDevelop)
            {
                _projectIcon.sprite = _baseIcon;
                _lock.SetActive(false);
                _icon.gameObject.SetActive(true);
            }
            else if (Project.CurrentProjectState == ProjectState.InProcess)
            {
                _projectIcon.sprite = _orangeLoadIcon;
                _lock.SetActive(false);
                _icon.gameObject.SetActive(true);
                _processBar.gameObject.SetActive(true);
                if(_projectModel.TryGetDevelopProcess(Project, out DevelopProcess developProcess))
                {
                    _developProcess = developProcess;
                    _developProcess.OnEndedDevelop += EndDevelopProcess;
                    _developProcess.OnTickSeconds += UpdateLoadBar;
                }
                else
                {
                    _developProcess = _projectModel.StartDevelopProcessFromSave(project, project.ProcesSeconds, project.ProcesStartSeconds);
                    _developProcess.OnEndedDevelop += EndDevelopProcess;
                    _developProcess.OnTickSeconds += UpdateLoadBar;
                }
            }
            else
            {
                _projectIcon.sprite = _blueIcon;
                _lock.SetActive(false);
                _icon.gameObject.SetActive(true);
            }

            if (Project.CurrentProjectState == ProjectState.ReadyToInplements)
            {
                _notificationItem.gameObject.SetActive(true);
                _notificationItem.color = Color.red;
                _lock.SetActive(false);
                _icon.gameObject.SetActive(true);
            }
            else if (Project.CurrentProjectState == ProjectState.ReadyToDevelop && Project.IsCanBuyProject)
            {
                _notificationItem.gameObject.SetActive(true);
                _notificationItem.color = Color.blue;
                _lock.SetActive(false);
                _icon.gameObject.SetActive(true);
            }
            else
            {
                _notificationItem.gameObject.SetActive(false);
            }
        }

        public void EndDevelopProcess(DevelopProcess developProcess)
        {
            if(_developProcess != null)
            {
                _developProcess.OnEndedDevelop -= EndDevelopProcess;
                _developProcess.OnTickSeconds -= UpdateLoadBar;
                _developProcess = null;
            }
            _processBar.gameObject.SetActive(false);
        }

        public void UpdateLoadBar(float seconds)
        {
            _processBar.UpdateFill((float)seconds / (float)_developProcess.StartSeconds);
        }


        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick?.Invoke(this);
            
            if (_wwiseSound._clickEvent.IsValid())
            {
                _wwiseSound._clickEvent.Post(gameObject);
            }
        }
    }
}