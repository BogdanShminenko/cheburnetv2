﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MVC.ProjectMVC.Views
{
    public class ProjectsItemsLine : MonoBehaviour
    {
        [SerializeField] private Transform _parent;
        
        private List<ProjectTreeItem> _projectTreeItems = new List<ProjectTreeItem>();

        public void CallOffGrid()
        {
            Invoke(nameof(OffGrid), 0.1f);
        }

        public void OffGrid()
        {
            if (gameObject.TryGetComponent(out GridLayoutGroup group))
            {
                group.enabled = false;
            }
        }

        public void AddProjectTreeItem(ProjectTreeItem projectTreeItem)
        {
            _projectTreeItems.Add(projectTreeItem);
            projectTreeItem.transform.SetParent(_parent);
        }
        
    }
}