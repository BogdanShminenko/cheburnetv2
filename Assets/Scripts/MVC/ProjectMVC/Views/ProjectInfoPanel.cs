﻿using Assets.Scripts.MVC.ProjectMVC.Views.InfoStatePanels;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MVC.ProjectMVC.Views
{
    public sealed class ProjectInfoPanel : MonoBehaviour
    {
        [SerializeField] private Image _projectIcon;
        [SerializeField] private Image _projectIconStatus;
        [SerializeField] private Sprite _baseIcon;
        [SerializeField] private Sprite _orangeIcon;
        [SerializeField] private Sprite _greenIcon;
        [SerializeField] private Sprite _blueIcon;
        [SerializeField] private GameObject _panel;
        [SerializeField] private TMP_Text _projectName;
        [SerializeField] private TMP_Text _projectDescription;

        [Inject] private ResearchedState _researchedState;
        [Inject] private AvailableDevelopAndBuyState _availableDevelopAndBuyState;
        [Inject] private AvailableDevelopState _availableDevelopState;
        [Inject] private DevelopmentProcessState _developmentProcessState;
        [Inject] private NotAvailableResearchState _notAvailableResearchState;
        [Inject] private ProjectImplementedState _projectImplementedState;

        private IProjectState _currentProjectState;
        private Project _currentProject;

        public void SetCurrentProject(Project project)
        {
            _currentProject = project;
            project.LoadIcon();
            _projectName.text = project.Title;
            _projectDescription.text = project.Description;
            _projectIcon.sprite = project.IconSprite;
            _projectIcon.color = Color.white;
        }

        public void OpenProjectInfoPanel()
        {
            _currentProject.OnChangeProjectState += ChangeProjectStateHandler;
            ChangeProjectStateHandler(_currentProject);
            _panel.SetActive(true);
        }

        public void CloseProjectInfoPanel()
        {
            _currentProject.OnChangeProjectState -= ChangeProjectStateHandler;
            _panel.SetActive(false);
        }

        public void ChangeProjectStateHandler(Project project)
        {
            if (project.CurrentProjectState == ProjectState.NotEnoughResourcesToDevelop)
            {
                SetProjectState(_notAvailableResearchState);
            } 
            else if (project.CurrentProjectState == ProjectState.ReadyToDevelop)
            {
                SetProjectState(_availableDevelopAndBuyState);

                //if (project.PriceBuy == 0)
                //{
                //    SetProjectState(_availableDevelopState);
                //}
                //else if (project.PriceBuy != 0)
                //{
                //    SetProjectState(_availableDevelopAndBuyState);
                //}
            }
            else if (project.CurrentProjectState == ProjectState.InProcess)
            {
                SetProjectState(_developmentProcessState);
            }
            else if (project.CurrentProjectState == ProjectState.ReadyToInplements)
            {
                SetProjectState(_projectImplementedState);
            }
            else if (project.CurrentProjectState == ProjectState.Researched)
            {
                SetProjectState(_researchedState);
            }

            if (project.CurrentProjectState == ProjectState.Researched)
            {
                _projectIconStatus.sprite = _blueIcon;
            }
            else if (project.CurrentProjectState == ProjectState.NotTriggered)
            {
                _projectIconStatus.sprite = _baseIcon;
            }
            else if (project.CurrentProjectState == ProjectState.ReadyToDevelop)
            {
                _projectIconStatus.sprite = _orangeIcon;
            }
            else if (project.CurrentProjectState == ProjectState.ReadyToInplements)
            {
                _projectIconStatus.sprite = _greenIcon;
            }
            else if (project.CurrentProjectState == ProjectState.NotEnoughResourcesToDevelop)
            {
                _projectIconStatus.sprite = _baseIcon;
            }
            else if (project.CurrentProjectState == ProjectState.InProcess)
            {
                _projectIconStatus.sprite = _orangeIcon;
            }
            else
            {
                _projectIconStatus.sprite = _blueIcon;
            }

        }

        private void SetProjectState(IProjectState projectState)
        {
            if(_currentProjectState != null)
            {
                _currentProjectState.ExitFromState();
            }
            _currentProjectState = projectState;
            _currentProjectState.EnterInState(_currentProject);
        }

    }
}