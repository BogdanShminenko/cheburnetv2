﻿using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.ProjectMVC
{
    public class DevelopProcess : MonoBehaviour
    {
        public Project CurrentProject { get; private set; }
        public float Seconds { get; private set; }
        public float StartSeconds { get; private set; }
        public event System.Action<DevelopProcess> OnEndedDevelop;
        public event System.Action<float> OnTickSeconds;

        private Coroutine _processCoroutine;


        public void InitProcess(Project project, GameSettingsData gameSettingsData)
        {
            CurrentProject = project;
            Seconds = (int)(project.Time * gameSettingsData.Tick);
            StartSeconds = Seconds;
            CurrentProject.ProcesStartSeconds = StartSeconds;
            CurrentProject.ProcesSeconds = Seconds;

            _processCoroutine = StartCoroutine(Process());
        }

        public void Pause()
        {
            if(_processCoroutine != null)
                StopCoroutine(_processCoroutine);
        }

        public void Continue()
        {
            _processCoroutine = StartCoroutine(Process());
        }

        public void InitProcessFromSave(Project project, GameSettingsData gameSettingsData, float seconds, float startSeconds)
        {
            CurrentProject = project;
            Seconds = seconds * 100;
            StartSeconds = startSeconds;

            CurrentProject.ProcesStartSeconds = StartSeconds;
            CurrentProject.ProcesSeconds = Seconds;

            _processCoroutine = StartCoroutine(Process());
        }

        public IEnumerator Process()
        {
            float count = 0;
            while (true)
            {
                if(count >= Seconds)
                {
                    OnEndedDevelop?.Invoke(this);
                    break;
                }
                OnTickSeconds?.Invoke(count);
                CurrentProject.ProcesSeconds = count;
                count += 0.01f;
                yield return new WaitForSeconds(0.01f);
            }
        }
    }
}