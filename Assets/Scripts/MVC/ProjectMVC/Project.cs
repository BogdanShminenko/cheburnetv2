﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.Game;
using Assets.Scripts.MVC.ProjectMVC.ProjectActions;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.TriggersAndOperationsTypes;
using Assets.Scripts.Utilities;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;
using static UnityEngine.EventSystems.EventTrigger;

namespace Assets.Scripts.MVC.ProjectMVC
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProjectState
    {
        NotEnoughResourcesToDevelop = 0,
        ReadyToDevelop = 1,
        ReadyToInplements = 2,
        InProcess = 3,
        Researched = 4,
        NotTriggered = 5
    }

    [Serializable]
    public class Project
    {
#nullable enable
        [field: SerializeField] public ProjectState CurrentProjectState { get; set; }
        [field: SerializeField] public string ProjectName { get; set; }
        [field: SerializeField] public DateTrigger? DateTrigger { get; set; }
        [field: SerializeField] public string ProjectID { get; set; }
        [field: SerializeField] public string Logo { get; set; }
        [field: SerializeField] public string Title { get; set; }
        [field: SerializeField] public string TextDisabled { get; set; }
        [field: SerializeField] public string TextAvailable { get; set; }
        [field: SerializeField] public int PriceDevelop { get; set; }
        [field: SerializeField] public int PriceBuy { get; set; }
        [field: SerializeField] public int Time { get; set; }
        [field: SerializeField] public string TextProcess { get; set; }
        [field: SerializeField] public string TextComplete { get; set; }
        [field: SerializeField] public string ResultTriggerComplete { get; set; }
        [field: SerializeField] public string TextImplemented { get; set; }
        [field: SerializeField] public string ResultTriggerImplemented { get; set; }
        [field: SerializeField] public AttitudeResult? ResultRebel { get; set; }
        [field: SerializeField] public AttitudeResult? ResultLoyal { get; set; }
        [field: SerializeField] public AttitudeResult? ResultControl { get; set; }
        [JsonIgnore] public Sprite? IconSprite { get; set; }
        [field: SerializeField] public string Icon { get; set; }
        [field: SerializeField] public int Years { get; set; }
        [field: SerializeField] public int Month { get; set; }
        [field: SerializeField] public string Description { get; set; }
        private DateTriggerConventer _dateTriggerConventer;
        private GameInfo _gameInfo;
        public event System.Action<Project> OnChangeProjectState;
        public bool IsCanBuyProject { get; set; }
        public List<string> TriggersSignal = new List<string>();
        [field: SerializeField] public bool IsTriggeredByDate { get; set; }
        [field: SerializeField] public bool IsTriggeredBySignal { get; set; }

        [field: SerializeField] public float ProcesSeconds { get; set; }
        [field: SerializeField] public float ProcesStartSeconds { get; set; }

        public Project(GameInfo gameInfo,DateTriggerConventer dateTriggerConventer)
        {
            _gameInfo = gameInfo;
            _dateTriggerConventer = dateTriggerConventer;
        }

        public void InitDependecies(DateTriggerConventer dateTriggerConventer)
        {
            _dateTriggerConventer = dateTriggerConventer;
        }

        public void InitProject(ProjectData projectData)
        {
            ProjectName = projectData.Title;
            ProjectID = projectData.ProjectID;
            Logo = projectData.Logo;
            Title = projectData.Title;
            Description = projectData.Description;
            TextDisabled = projectData.TextDisabled;
            TextAvailable = projectData.TextAvailable;
            PriceDevelop = projectData.PriceDevelop;
            PriceBuy = projectData.PriceBuy;
            Time = projectData.Time;
            TextProcess = projectData.TextProcess;
            TextComplete = projectData.TextComplete;
            ResultTriggerComplete = projectData.ResultTriggerComplete;
            TextImplemented = projectData.TextImplemented;
            ResultTriggerImplemented = projectData.ResultTriggerImplemented;
            Icon = projectData.Icon;
            if (projectData.ResultLoyal != 0)
            {
                if (projectData.ResultLoyal > 0)
                    ResultLoyal = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(projectData.ResultLoyalSpeed), projectData.ResultLoyal, Accrual.CurrencyType.Loyal, Accrual.OperationType.Increase);
                else
                    ResultLoyal = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(projectData.ResultLoyalSpeed), projectData.ResultLoyal, Accrual.CurrencyType.Loyal, Accrual.OperationType.Decrease);
            }
            if (projectData.ResultRebel != 0)
            {
                if (projectData.ResultRebel >= 0)
                    ResultRebel = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(projectData.ResultRebelSpeed), projectData.ResultRebel, Accrual.CurrencyType.Rebel, Accrual.OperationType.Increase);
                else
                    ResultRebel = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(projectData.ResultRebelSpeed), projectData.ResultRebel, Accrual.CurrencyType.Rebel, Accrual.OperationType.Decrease);
            }
            if (projectData.ResultControl != 0)
            {
                if (projectData.ResultControl >= 0)
                    ResultControl = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(projectData.ResultControlSpeed), projectData.ResultControl, Accrual.CurrencyType.InternetControl, Accrual.OperationType.Increase);
                else
                    ResultControl = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(projectData.ResultControlSpeed), projectData.ResultControl, Accrual.CurrencyType.InternetControl, Accrual.OperationType.Decrease);
            }

            Years = (int)Mathf.Floor(Time / 12);
            Month = Time - (Years * 12);

            if (Icon != null && Icon.Length > 0)
                IconSprite = Resources.Load<Sprite>($"Sprites/Projects/{Icon.Replace(".png", "")}");

            if (_dateTriggerConventer.TryConvertStringToDateTrigger(projectData.TriggerDate, out DateTrigger dateTrigger))
            {
                DateTrigger = dateTrigger;
            }

            if (projectData.TriggerSignal == "" || projectData.TriggerSignal.Length == 0)
            {
                TriggersSignal.Add("null");
            }
            else
            {
                string[] triggers = projectData.TriggerSignal.Split(',');
                TriggersSignal = triggers.ToList();
            }

            ChangeProjectState(ProjectState.NotTriggered);
        }

        private ValueCangeType GetValueChangeBySymbol(string symbol)
        {
            if (symbol == "M")
                return ValueCangeType.Moment;
            else if (symbol == "S")
                return ValueCangeType.Slow;
            else if (symbol == "N")
                return ValueCangeType.Normal;
            return ValueCangeType.Fast;
        }

        public void ChangeProjectState(ProjectState projectState)
        {
            CurrentProjectState = projectState;
            OnChangeProjectState?.Invoke(this);
        }

        public void LoadIcon()
        {
            if (Icon != null && Icon.Length > 0)
                IconSprite = Resources.Load<Sprite>($"Sprites/Projects/{Icon.Replace(".png", "")}");
        }

        public bool TryTriggerProjectByDate(int currentYear, int currentMounth)
        {
            if (DateTrigger != null)
            {
                if (new DateTime(DateTrigger.Year, DateTrigger.Month, 1) <= new DateTime((int)currentYear, (int)currentMounth, 1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
    }
}