﻿using System;
using Assets.Scripts.GameStates;
using Assets.Scripts.MVC.PauseMenuMVC;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using Zenject;
using Zenject.Asteroids;
using Assets.Scripts.MVC.ProjectMVC;
using Assets.Scripts.MVC.TimerMVC;

namespace Assets.Scripts.MVC.CutsceneMVC
{
    public class CutSceneView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _cutSceneText;
        [SerializeField] private TMP_Text _increaseMoneyText;
        [SerializeField] private TMP_Text _increaseWillText;

        [SerializeField] private GameObject _panel;
        
        [Header("Events")]
        [SerializeField] private UnityEvent _onOpenEvent;
        [SerializeField] private UnityEvent _onCloseEvent;
        
        [Inject] private GameState _gameState;
        [Inject] private PauseMenuController _pauseMenuController;
        [Inject] private ProjectView _projectView;
        [Inject] private TimerController _timerController;

        public void Init(string text, int increaseMoney , int increaseWill)
        {
            _cutSceneText.text = text;
            _increaseMoneyText.text = increaseMoney.ToString();
            _increaseWillText.text = increaseWill.ToString();
        }

        public void Open()
        {
            if(_gameState.CurrentGameState == GameStates.GameStates.Projects)
            {
                _projectView.CloseTree();
            }
            _gameState.ChangeGameState(GameStates.GameStates.CutScene);
            _panel.SetActive(true);
            _pauseMenuController.PauseGame();
            
            if (_onOpenEvent != null)
            {
                _onOpenEvent.Invoke();
            }
        }

        public void Close()
        {
            if(_gameState.PreviousGameStates == GameStates.GameStates.Projects)
            {
                _projectView.OpenTree();
            }
            _timerController.SetupNewGameCycle();
            _gameState.ChangeGameState(_gameState.PreviousGameStates);
            _panel.SetActive(false);
            _pauseMenuController.ContinueGame();
            

            if (_onCloseEvent != null)
            {
                _onCloseEvent.Invoke();
            }
        }

    }
}