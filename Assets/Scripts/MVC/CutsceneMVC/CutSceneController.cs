﻿using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.MVC.TimerMVC;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.CutsceneMVC
{
    public class CutSceneController : MonoBehaviour
    {
        [Inject] private CutSceneModel _cutSceneModel;
        [Inject] private CutSceneView _cutSceneView;
        [Inject] private MoneyModel _moneyModel;
        [Inject] private WillModel _willModel;

        public void OpenCutScene(int completedQuests, int questTotalForCycle)
        {
            string firstPhase = _cutSceneModel.GetFirstPhrase(completedQuests, questTotalForCycle);
            string secondPhase = _cutSceneModel.GetSecondPhase(completedQuests, questTotalForCycle);

            _cutSceneView.Init($"{firstPhase} {secondPhase}", (int)_moneyModel.MaxCurrencyAmount, (int)_willModel.MaxCurrencyAmount);
            _cutSceneView.Open();
        }
    }
}