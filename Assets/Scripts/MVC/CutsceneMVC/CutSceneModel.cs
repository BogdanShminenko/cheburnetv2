﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.CutsceneMVC
{
    public class CutSceneModel : MonoBehaviour
    {
        
        public string GetFirstPhrase(int completedQuests , int questTotalForCycle)
        {
            if(completedQuests >= 3)
            {
                return "Отличная работа!";
            }else if(completedQuests == 0 && questTotalForCycle > 0)
            {
                return "Работаешь слабо.";
            }
            else
            {
                return "Неплохая работа.";
            }
        }

        public string GetSecondPhase(int completedQuests, int questTotalForCycle)
        {
            if (completedQuests > 2)
            {
                return $"Квестов закончено {completedQuests}. Я распорядился серьезно увеличить тебе бюджет.";
            }
            else if (completedQuests == 1)
            {
                return "Один квест закончен. Я распорядился увеличить тебе бюджет.";
            }
            else if(completedQuests == 0 && questTotalForCycle > 0)
            {
                return "Ни одного квеста не закончено. Я распорядился немного увеличить тебе бюджет, впредь работай эффективнее.";
            }
            else
            {
                return "Я распорядился немного увеличить тебе бюджет.";
            }
        }

    }
}