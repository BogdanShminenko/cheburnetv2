﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.Utilities;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.TimerMVC
{
    public sealed class TimerModel : MonoBehaviour
    {
        public Action OnTimerStarted;
        public Action OnYearPassedCallback;
        public Action OnMonthPassedCallback;
        public Action OnGameTimerOverCallback;
        public Action OnGameNewCycle;

        private const int MONTHS_COUNT = 12;
        private const int MAX_TOTAL_MONTH_COUNT = 300;

        [Inject] private GameSettingsData _gameSettingsData;
        [Inject] private DateTriggerConventer _dateTriggerConventer;

        [SerializeField] private List<GameCycle> _gameCycles = new List<GameCycle>();
        [SerializeField] private uint _currentYear;
        [SerializeField] private uint _currentMonth = 1;
        public float TimeSinceYearPassed { get; private set; }
        public uint CurrentYear => _currentYear;
        public uint CurrentMonth => _currentMonth;
        public int TotalMonthCount { get; private set; }
        public bool IsPaused { get; private set; } = true;
        public float GetYearProgress() => TimeSinceYearPassed / (float)_gameSettingsData.GameYearInSeconds;
        
        public void InitGameCycles(List<GameCyclesData> gameCyclesDatas)
        {
            foreach(var item in gameCyclesDatas)
            {
                GameCycle gameCycle = new GameCycle(_dateTriggerConventer);
                gameCycle.Init(item);
                _gameCycles.Add(gameCycle);
            }
        }

        public bool TryGetGameCycle(out GameCycle gameCycle)
        {
            if(_gameCycles.Count == 0)
            {
                gameCycle = null;
                return false;
            }
            gameCycle = _gameCycles[0];
            _gameCycles.RemoveAt(0);
            return true;
        }

        private void Update()
        {
            if (IsPaused)
                return;
            CalculateGameCycle();
        }

        public void PauseTimer()
        {
            IsPaused = true;
        }

        public void Continue()
        {
            IsPaused = false;
        }

        public void StartTimerCycle()
        {
            _currentYear = _gameSettingsData.StartYear;
            OnGameNewCycle?.Invoke();
            _currentMonth = 1;
            IsPaused = false;
            TimeSinceYearPassed = 0.0f;
            OnTimerStarted?.Invoke();
        }

        public void SetTimeAndStartTimerCycle(uint currentYear , uint currentMonth, float timeSinceYearPassed , int monthCount)
        {
            TotalMonthCount = monthCount;
            _currentYear = currentYear;
            _currentMonth = currentMonth;
            TimeSinceYearPassed = timeSinceYearPassed;
            IsPaused = false;
            OnGameNewCycle?.Invoke();
            OnTimerStarted?.Invoke();
        }

        private void CalculateGameCycle()
        {
            uint currentMounth = CurrentMonth;
            _currentMonth = (uint)((TimeSinceYearPassed / (float)_gameSettingsData.GameYearInSeconds) * MONTHS_COUNT) + 1;
            if (CurrentMonth > currentMounth)
            {
                if(TotalMonthCount + 1 <= MAX_TOTAL_MONTH_COUNT)
                    TotalMonthCount++;
                OnMonthPassedCallback?.Invoke();
            }
            if (TimeSinceYearPassed < _gameSettingsData.GameYearInSeconds) TimeSinceYearPassed += Time.deltaTime;
            else HandlePassedYear();
        }
        private void HandlePassedYear()
        {
            _currentYear++;
            _currentMonth = 1;
            TimeSinceYearPassed = 0.0f;
            OnYearPassed();
            if (CurrentYear >= _gameSettingsData.EndYear)
            {
                GameTimerIsOver();
            }
        }


        private void OnYearPassed()
        {
            OnYearPassedCallback?.Invoke();
            if((CurrentYear - _gameSettingsData.StartYear) % _gameSettingsData.GameCycleIntervalInYears == 0) 
            {
                OnGameNewCycle?.Invoke();
            }
        }
        private void GameTimerIsOver()
        {
            OnGameTimerOverCallback?.Invoke();
            IsPaused = true;
        }
    }
}