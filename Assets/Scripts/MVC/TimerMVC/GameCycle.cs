﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.TriggersAndOperationsTypes;
using Assets.Scripts.Utilities;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.TimerMVC
{
    [Serializable]
    public class GameCycle
    {
        public string CycleID {get;set;}
        [field: SerializeField] public DateTrigger DateTrigger { get; set; }
        private DateTriggerConventer _dateTriggerConventer;

        public GameCycle(DateTriggerConventer dateTriggerConventer)
        {
            _dateTriggerConventer = dateTriggerConventer;
        }

        public void Init(GameCyclesData gameCyclesData)
        {
            CycleID = gameCyclesData.CycleID;   
            if(_dateTriggerConventer.TryConvertStringToDateTrigger(gameCyclesData.TriggerDate, out DateTrigger dateTrigger))
                DateTrigger = dateTrigger;
        }
    }
}