﻿using UnityEngine;
using TMPro;
using Zenject;

namespace Assets.Scripts.MVC.TimerMVC
{
    public sealed class TimerView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _yearCounter;
        [SerializeField] private TMP_Text _monthText;

        [Inject] private TimerModel _timerModel;
        [Inject] private TimerProgresssBar _timerProgresssBar;

        private string[] _months = new string[12]
        {
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь",
        };

        private void OnEnable()
        {
            _timerModel.OnTimerStarted += InitTimerRenderer;
            _timerModel.OnYearPassedCallback += UpdateYearConter;
            _timerModel.OnMonthPassedCallback += UpdateMonth;
        }

        private void OnDisable()
        {
            _timerModel.OnTimerStarted -= InitTimerRenderer;
            _timerModel.OnYearPassedCallback -= UpdateYearConter;
            _timerModel.OnMonthPassedCallback -= UpdateMonth;
        }

        private void Update()
        {
            HandleProgressBar();
        }
        private void HandleProgressBar()
        {
            _timerProgresssBar.RenderProgressBar(_timerModel.GetYearProgress());
        }
        private void InitTimerRenderer()
        {
            _timerProgresssBar.RenderProgressBar(0);
            UpdateYearConter();
            UpdateMonth();
        }
        private void UpdateYearConter()
        {
            _yearCounter.text = _timerModel.CurrentYear.ToString();
        }

        private void UpdateMonth()
        {
            if(_timerModel.CurrentMonth - 1  < _months.Length)
            {
                _monthText.text = _months[_timerModel.CurrentMonth - 1];
            }
            else
            {
                _monthText.text = _months[0];
            }
        }
    }
}