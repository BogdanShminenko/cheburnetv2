﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MVC.TimerMVC
{
    public sealed class TimerProgresssBar : MonoBehaviour
    {
        [SerializeField] private Image _progressFillImage;

        public void RenderProgressBar(float progress)
        {
            _progressFillImage.fillAmount = progress;
        }

    }
}