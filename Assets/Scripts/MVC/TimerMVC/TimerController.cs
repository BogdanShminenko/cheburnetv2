﻿using Assets.Scripts.Data.GameData.Configs;
using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.MVC.CutsceneMVC;
using Assets.Scripts.MVC.QuestMVC;
using System;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.TimerMVC
{
    public sealed class TimerController : MonoBehaviour
    {
        [Inject] private TimerModel _timerModel;
        [Inject] private GameCyclesConfig _gameCyclesConfig;
        [Inject] private QuestModel _questModel;
        [Inject] private MoneyModel _moneyModel;
        [Inject] private CutSceneController _cutSceneController;

        public int QuestComplitedForCycle { get; private set; }
        public int QuestsTriggeredForCycle { get; private set; }
        [SerializeField] private GameCycle _currentGameCycle;

        private void Awake()
        {
            _questModel.OnQuestCompleted += CompleteQuestHandler;
            _questModel.OnQuestTriggered += OnTriggeredQuest;
        }

        public void StartTimer()
        {
            _timerModel.StartTimerCycle();
            _timerModel.InitGameCycles(_gameCyclesConfig.GameCyclesDatas);
            if (_timerModel.TryGetGameCycle(out GameCycle gameCycle))
            {
                _currentGameCycle = gameCycle;
            }
        }

        private void OnTriggeredQuest(Quest quest)
        {
            QuestsTriggeredForCycle++;
        }

        private void CompleteQuestHandler()
        {
            QuestComplitedForCycle++;
        }


        public void TryTriggerGameCycle()
        {
            if (_currentGameCycle == null)
                return;

            try
            {
                if (new DateTime(_currentGameCycle.DateTrigger.Year, _currentGameCycle.DateTrigger.Month, 1) <= new DateTime((int)_timerModel.CurrentYear, (int)_timerModel.CurrentMonth, 1))
                {
                    _moneyModel.IncreaseMaxMoney();
                    _moneyModel.IncreaseMoneyRestoreTime();
                    if (_currentGameCycle.CycleID != "cycle_1")
                        _cutSceneController.OpenCutScene(QuestComplitedForCycle, QuestsTriggeredForCycle);
                    _currentGameCycle = null;
                }
            }
            catch
            {

            }
        }

        public void SetupNewGameCycle()
        {
            if (_timerModel.TryGetGameCycle(out GameCycle gameCycle))
            {
                SetupGameCycle(gameCycle);
            }
        }

        private void SetupGameCycle(GameCycle gameCycle)
        {
            if(gameCycle != null)
                _currentGameCycle = gameCycle;
            QuestComplitedForCycle = 0;
            QuestsTriggeredForCycle = 0;
        }

    }
}