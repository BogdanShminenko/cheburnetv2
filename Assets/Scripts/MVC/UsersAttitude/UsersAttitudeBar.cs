﻿using Assets.Scripts.Accrual;
using Assets.Scripts.MVC.InternetMVC;
using Assets.Scripts.TriggersAndOperationsTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.UsersAttitude
{
    public sealed class UsersAttitudeBar : MonoBehaviour
    {
        [SerializeField] private List<UserUIItem> _userUIItems;

        [SerializeField] private TMP_Text _debugText;


        [Inject] private AccuralInLoop _accuralInLoop;
        [Inject] private UsersAttitudeModel _usersAttitudeModel;
        [Inject] private InternetModel _internetModel;

        private string _loyalDebug;
        private string _rebelDebug;
        private string _neutralDebug;

        private void Start()
        {
            _usersAttitudeModel.SetSegmentCount(_userUIItems.Count);
        }

        private void OnEnable()
        {
            _accuralInLoop.OnCalculateAttitudeResult += HandleAttitudeResultInLoop;
        }

        private void OnDisable()
        {
            _accuralInLoop.OnCalculateAttitudeResult -= HandleAttitudeResultInLoop;
        }

        private void HandleAttitudeResultInLoop()
        {
            _loyalDebug = "";
            _neutralDebug = "";
            _rebelDebug = "";
            foreach (var item in _accuralInLoop.AttitudeResults)
            {
                if(item.CurrencyType == CurrencyType.Rebel)
                {
                    _rebelDebug += GetLiteral(item);
                }
                else if (item.CurrencyType == CurrencyType.Loyal)
                {
                    _loyalDebug += GetLiteral(item);
                }
                else if (item.CurrencyType == CurrencyType.Neutral)
                {
                    _neutralDebug += GetLiteral(item);
                }
            }
        }

        private string GetLiteral(AttitudeResult attitudeResult)
        {
            string literal = "";
            if (attitudeResult.OperationType == OperationType.Increase)
                literal += "+";
            else if (attitudeResult.OperationType == OperationType.Decrease)
                literal += "-";
            literal += attitudeResult.StartCount;
            if(attitudeResult.ValueCangeType == ValueCangeType.Moment)
                literal += "M";
            else if(attitudeResult.ValueCangeType == ValueCangeType.Slow)
                literal += "S";
            else if (attitudeResult.ValueCangeType == ValueCangeType.Fast)
                literal += "F";
            else if (attitudeResult.ValueCangeType == ValueCangeType.Normal)
                literal += "N";
            return literal;
        }

        private void Update()
        {
            _debugText.text = $"loyal: {Math.Round((float)_usersAttitudeModel.LoyalUsersPercent)}%  {_loyalDebug} \n"
                            + $"rebel: {Math.Round((float)_usersAttitudeModel.RebelUsersPercent)}% {_rebelDebug}\n"
                            + $"neutral: {Math.Round((float)_usersAttitudeModel.NeutralUsersPercent)}% {_neutralDebug}\n"
                            + $"Full percents: {_usersAttitudeModel.LoyalUsersPercent + _usersAttitudeModel.RebelUsersPercent + _usersAttitudeModel.NeutralUsersPercent}%"
                            + $"Users count on Map: {_internetModel.CountUsersInInnerInternet}";
        }

        //Обновляем UI bar с юзерами
        public void ChangeAttitude(int loyalCountPercent , int rebelCountPercent , int neutralCountPercent)
        {
            int loyal = (int)System.MathF.Round(_usersAttitudeModel.LoyalUsersPercent * (_userUIItems.Count/100f));
            int rebel = (int)System.MathF.Round(_usersAttitudeModel.RebelUsersPercent * (_userUIItems.Count / 100f));
            int neutral = _userUIItems.Count - loyal - rebel;
            //int neutral = (int)System.MathF.Round(_usersAttitudeModel.NeutralUsersPercent * (_userUIItems.Count / 100f));
            //neutral = _userUIItems.Count - loyal - rebel;

            int count = 0;
            int nextUsers = loyal;
            for (int i = 0; i < nextUsers; i++)
            {
                if (i < _userUIItems.Count)
                {
                    _userUIItems[i].ChangeAttitude(AttitudeTypes.Loyal);
                    count++;
                }
            }
            nextUsers += neutral;
            for (int i = count; i < nextUsers; i++)
            {
                if (i < _userUIItems.Count)
                {
                    _userUIItems[i].ChangeAttitude(AttitudeTypes.Neutral);
                    count++;
                }
            }
            nextUsers += rebel;
            for (int i = count; i <= nextUsers; i++)
            {
                if (i < _userUIItems.Count)
                    _userUIItems[i].ChangeAttitude(AttitudeTypes.Rebel);
            }

        }
    }
}