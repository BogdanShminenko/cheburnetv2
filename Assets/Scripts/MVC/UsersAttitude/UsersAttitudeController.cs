﻿using System.Collections;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using Zenject;

namespace Assets.Scripts.MVC.UsersAttitude
{
    public class UsersAttitudeController : MonoBehaviour
    {
        [Inject] private UsersAttitudeModel _usersAttitudeModel;
        [Inject] private GameSettingsData _gameSettingsData;
       
        public void InitUserAttitude()
        {
            _usersAttitudeModel.InitAttitudesPercent((int)_gameSettingsData.LoyalUsersPercent, (int)_gameSettingsData.RebelUsersPercent);
        }

        public void IncreaseLoyalPercent(int loyalUsers)
        {
            _usersAttitudeModel.IncreaseLoyalPercent(loyalUsers);
        }

        public void DecreaseLoyalPercent(int loyalUsers)
        {
            _usersAttitudeModel.DecreaseLoyalPercent(loyalUsers);
        }

        public void IncreaseRebelPercent(int rebelUsers)
        {
            _usersAttitudeModel.IncreaseRebelPercent(rebelUsers);
        }

        public void DecreaseRebelPercent(int rebelUsers)
        {
            _usersAttitudeModel.DecreaseRebelPercent(rebelUsers);
        }

        public void IncreaseNeutralPercent(int neutralUsers)
        {
            _usersAttitudeModel.IncreaseNeutralPercent(neutralUsers);
        }

        public void DecreaseNeutralPercent(int neutralUsers)
        {
            _usersAttitudeModel.DecreaseNeutralPercent(neutralUsers);
        }
    }
}