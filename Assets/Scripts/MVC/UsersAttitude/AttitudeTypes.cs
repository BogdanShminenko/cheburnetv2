﻿using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.UsersAttitude
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AttitudeTypes 
    {
        Loyal,
        Rebel,
        Neutral
    }
}