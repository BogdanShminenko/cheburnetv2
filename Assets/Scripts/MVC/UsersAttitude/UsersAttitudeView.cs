﻿using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.UsersAttitude
{
    public class UsersAttitudeView : MonoBehaviour
    {
        [Inject] private UsersAttitudeModel _model;
        [Inject] private UsersAttitudeBar _usersAttitudeBar;

        public void SwitchActive()
        {
            _usersAttitudeBar.gameObject.SetActive(!_usersAttitudeBar.gameObject.activeSelf);
        }

        private void OnEnable()
        {
            _model.OnChangeAttitude += ChangeUserAttitude;
        }

        private void OnDisable()
        {
            _model.OnChangeAttitude -= ChangeUserAttitude;
        }

        public void ChangeUserAttitude()
        {
            _usersAttitudeBar.ChangeAttitude((int)_model.LoyalUsersPercent, (int)_model.RebelUsersPercent, (int)_model.NeutralUsersPercent);
        }

    }
}