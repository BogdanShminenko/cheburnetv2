﻿using System.Collections;
using UnityEngine;
using System;
using Assets.Scripts.Accrual;

namespace Assets.Scripts.MVC.UsersAttitude
{
    public class UsersAttitudeModel : MonoBehaviour
    {

        public event System.Action OnChangeAttitude;

        [SerializeField] private float _loyalUsersPercent;
        [SerializeField] private float _rebelUsersPercent;
        [SerializeField] private float _neutralUsersPercent;

        public float LoyalUsersPercent => _loyalUsersPercent;
        public float RebelUsersPercent => _rebelUsersPercent;
        public float NeutralUsersPercent => _neutralUsersPercent;


        public int CountOfUsersByType(AttitudeTypes attitudeTypes ,int createdUsers)
        {
            if (attitudeTypes == AttitudeTypes.Loyal)
                return (int)Math.Ceiling(createdUsers * LoyalUsersPercent * 0.01f);
            else if (attitudeTypes == AttitudeTypes.Rebel)
                return (int)Math.Ceiling(createdUsers * RebelUsersPercent * 0.01f);
            else
                return createdUsers - (int)Math.Ceiling(createdUsers * LoyalUsersPercent * 0.01f) - (int)Math.Ceiling(createdUsers * RebelUsersPercent * 0.01f);
        }

        public void SetSegmentCount(int segmentCount)
        {
            if(segmentCount <= 0)
                segmentCount = 0;
        }

        public void InitAttitudesPercent(int loyalUsersPercent, int rebelUsersPercent)
        {
            _loyalUsersPercent = loyalUsersPercent;
            _rebelUsersPercent = rebelUsersPercent;
            _neutralUsersPercent = 100 - loyalUsersPercent - rebelUsersPercent;
            OnChangeAttitude?.Invoke();
        }

        private void RecalculateRebelsAndLoyaks(int count, OperationType operationType)
        {            
            int rebel;
            int loyal;

            if (count % 2 == 0)
            {
                rebel = count / 2;
                loyal = count / 2;
            }
            else
            {
                count++;
                rebel = count / 2;
                loyal = count / 2;

                loyal--;
            }
            int currentRebel = 0;
            for (int i = 0; i < rebel; i++)
            {
                if (operationType == OperationType.Increase)
                {
                    if (_rebelUsersPercent + 1 > 100)
                    {
                        loyal += rebel - currentRebel;
                        break;
                    }
                    currentRebel++;
                    _rebelUsersPercent++;
                }
                else
                {
                    if (_rebelUsersPercent - 1 < 0)
                    {
                        loyal += rebel - currentRebel;
                        break;
                    }
                    currentRebel++;
                    _rebelUsersPercent--;
                }
            }

            for (int i = 0; i < loyal; i++)
            {
                if (operationType == OperationType.Increase)
                {
                    if (_loyalUsersPercent + 1 > 100)
                    {
                        break;
                    }
                    currentRebel++;
                    _loyalUsersPercent++;
                }
                else
                {
                    if (_loyalUsersPercent - 1 < 0)
                    {
                        break;
                    }
                    currentRebel++;
                    _loyalUsersPercent--;
                }
            }
        }

        private void ConvertPositiveValuePercents()
        {
            if (_loyalUsersPercent < 0)
                _loyalUsersPercent = 0;
            if (_rebelUsersPercent < 0)
                _rebelUsersPercent = 0;
            if (_neutralUsersPercent < 0)
                _neutralUsersPercent = 0;
        }

        private void RecalculatePercents(AttitudeTypes attitudeTypes)
        {
            ConvertPositiveValuePercents();
            int currentFullPercents = (int)(_loyalUsersPercent + _rebelUsersPercent + _neutralUsersPercent);
            if(attitudeTypes == AttitudeTypes.Neutral)
            {
                if (currentFullPercents > 100)
                {
                    RecalculateRebelsAndLoyaks(currentFullPercents - 100, OperationType.Decrease);
                }
                else if (currentFullPercents < 100)
                {
                    RecalculateRebelsAndLoyaks(100 - currentFullPercents, OperationType.Increase);
                }
            }
            else
            {
                if (currentFullPercents > 100)
                {
                    if(_neutralUsersPercent <= 0)
                    {
                        if(attitudeTypes == AttitudeTypes.Rebel)
                        {
                            _loyalUsersPercent -= currentFullPercents - 100;
                        }
                        else
                        {
                            _rebelUsersPercent -= currentFullPercents - 100;
                        }
                    }
                    else
                    {
                        _neutralUsersPercent -= currentFullPercents - 100;
                    }

                }
                else if (currentFullPercents < 100)
                {
                    _neutralUsersPercent += 100 - currentFullPercents;

                }
            }
            ClampPercents();
        }


        private void Update()
        {
            ClampPercents();
        }

        private void ClampPercents()
        {
            _loyalUsersPercent = Math.Clamp(_loyalUsersPercent, 0, 100);
            _rebelUsersPercent = Math.Clamp(_rebelUsersPercent, 0, 100);
            _neutralUsersPercent = Math.Clamp(_neutralUsersPercent, 0, 100);
        }

        public void IncreaseLoyalPercent(int loyalUsersPercent)
        {
            _loyalUsersPercent += loyalUsersPercent;
            RecalculatePercents(AttitudeTypes.Loyal);
            OnChangeAttitude?.Invoke();
        }

        public void DecreaseLoyalPercent(int loyalUsersPercent)
        {
            _loyalUsersPercent -= loyalUsersPercent;
            RecalculatePercents(AttitudeTypes.Loyal);
            OnChangeAttitude?.Invoke();
        }

        public void IncreaseRebelPercent(int rebelUsersPercent)
        {
            _rebelUsersPercent += rebelUsersPercent;
            RecalculatePercents(AttitudeTypes.Rebel);
            OnChangeAttitude?.Invoke();
        }

        public void DecreaseRebelPercent(int rebelUsersPercent)
        {
            _rebelUsersPercent -= rebelUsersPercent;
            RecalculatePercents(AttitudeTypes.Rebel);
            OnChangeAttitude?.Invoke();
        }

        public void IncreaseNeutralPercent(int neutralUsers)
        {
            _neutralUsersPercent += neutralUsers;
            RecalculatePercents(AttitudeTypes.Neutral);
            OnChangeAttitude?.Invoke();
        }

        public void DecreaseNeutralPercent(int neutralUsers)
        {
            _neutralUsersPercent -= neutralUsers;
            RecalculatePercents(AttitudeTypes.Neutral);
            OnChangeAttitude?.Invoke();
        }
    }
}