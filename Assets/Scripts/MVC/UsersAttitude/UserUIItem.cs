﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MVC.UsersAttitude
{
    public class UserUIItem : MonoBehaviour
    {
        [SerializeField] private Image _color;
        [SerializeField] private Color _loyalColor;
        [SerializeField] private Color _rebelColor;
        [SerializeField] private Color _neutralColor;
        private AttitudeTypes _attitudeTypes;

        //Меняем цвет юзера
        public void ChangeAttitude(AttitudeTypes attitudeTypes)
        {
            _attitudeTypes = attitudeTypes;
            if(_attitudeTypes == AttitudeTypes.Loyal)
            {
                _color.color = _loyalColor;
            }else if(_attitudeTypes == AttitudeTypes.Rebel)
            {
                _color.color = _rebelColor;
            }else if(_attitudeTypes == AttitudeTypes.Neutral)
            {
                _color.color = _neutralColor;
            }
        }

    }
}