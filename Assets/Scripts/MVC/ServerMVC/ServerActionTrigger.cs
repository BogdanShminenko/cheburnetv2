﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Game;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.TriggersAndOperationsTypes;
using System;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.ServerMVC
{
    public class ServerActionTrigger : MonoBehaviour
    {
        [Inject] private GameInfo _gameInfo;
        [Inject] private TimerModel _timerModel;

        public bool CheckTrigered(ServerActionCase server)
        {
            if (TryTriggerEventByDate(server.DateTrigger)
                && CheckByAttitude(server.TriggerLoyalOver, CurrencyType.Loyal)
                && CheckByAttitude(server.TriggerLoyalUnder, CurrencyType.Loyal)
                && CheckByAttitude(server.TriggerRebelOver, CurrencyType.Rebel)
                && CheckByAttitude(server.TriggerRebelUnder, CurrencyType.Rebel)
                && (_gameInfo.CheckTrigger(server.TriggerSignal) || server.TriggerSignal == "null"))
            {
                return true;
            }
            return false;
        }

        private bool TryTriggerEventByDate(DateTrigger dateTrigger)
        {
            if (dateTrigger == null)
                return true;
            if (new DateTime(dateTrigger.Year, dateTrigger.Month, 1) <= new DateTime((int)_timerModel.CurrentYear, (int)_timerModel.CurrentMonth, 1))
            {
                return true;
            }
            return false;
        }

        private bool CheckByAttitude(AttitudeTrigger attitudeTrigger, CurrencyType currencyType)
        {
            if (attitudeTrigger == null)
            {
                return true;
            }
            if (attitudeTrigger.AccuralDirection == OperationType.Increase)
            {
                if (attitudeTrigger.CurrentAdditude <= _gameInfo.GetAttitudeByType(currencyType))
                {
                    return true;
                }
            }
            else if (attitudeTrigger.AccuralDirection == OperationType.Decrease)
            {
                if (attitudeTrigger.CurrentAdditude >= _gameInfo.GetAttitudeByType(currencyType))
                {
                    return true;
                }
            }

            return false;
        }
    }
}