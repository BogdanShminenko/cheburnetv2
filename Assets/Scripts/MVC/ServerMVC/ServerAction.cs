﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Data.GameData;
using Assets.Scripts.Game;
using Assets.Scripts.TriggersAndOperationsTypes;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.ServerMVC
{

    [Serializable]
    public class ServerAction
    {
        public string ServerID { get; set; }
        public string ActionCaseID { get; set; }
        public int OrderNumber { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int PriceMoney { get; set; }
        public int PriceWill { get; set; }
        public Ownage ResultOwnage { get; set; }
        public AttitudeResult ResultRebel { get; set; }
        public AttitudeResult ResultLoyal { get; set; }
        public AttitudeResult ResultControl { get; set; }
        public string ResultTrigger { get; set; }
        private GameInfo _gameInfo;

        public bool IsEmpty
        {
            get
            {
                if(PriceMoney == 0 && PriceWill == 0 && Title == "")
                    return true;
                return false;
            }
        }

        public ServerAction(GameInfo gameInfo)
        {
            _gameInfo = gameInfo;
        }

        public void Init(ServerActionData data)
        {
            ServerID = data.ServerID;
            ActionCaseID = data.ActionCaseID;                
            OrderNumber = data.OrderNumber;
            Title = data.Title;
            Description = data.Description;
            PriceMoney = data.PriceMoney;
            PriceWill = data.PriceWill;
            ResultTrigger = data.ResultTrigger;
            //ResultControl = new AccuralData(CurrencyType.)

            if (data.ResultLoyal != 0)
            {
                if (data.ResultLoyal > 0)
                    ResultLoyal = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultLoyalSpeed), data.ResultLoyal, Accrual.CurrencyType.Loyal, Accrual.OperationType.Increase);
                else
                    ResultLoyal = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultLoyalSpeed), data.ResultLoyal, Accrual.CurrencyType.Loyal, Accrual.OperationType.Decrease);
            }
            if (data.ResultRebel != 0)
            {
                if (data.ResultRebel >= 0)
                    ResultRebel = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultRebelSpeed), data.ResultRebel, Accrual.CurrencyType.Rebel, Accrual.OperationType.Increase);
                else
                    ResultRebel = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultRebelSpeed), data.ResultRebel, Accrual.CurrencyType.Rebel, Accrual.OperationType.Decrease);
            }
            if (data.ResultControl != 0)
            {
                if (data.ResultControl >= 0)
                    ResultControl = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultControlSpeed), data.ResultControl, Accrual.CurrencyType.InternetControl, Accrual.OperationType.Increase);
                else
                    ResultControl = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(data.ResultControlSpeed), data.ResultControl, Accrual.CurrencyType.InternetControl, Accrual.OperationType.Decrease);
            }



            InitServerOwnage(data.ResultOwnage);
        }
        private ValueCangeType GetValueChangeBySymbol(string symbol)
        {
            if (symbol == "M")
                return ValueCangeType.Moment;
            else if (symbol == "S")
                return ValueCangeType.Slow;
            else if (symbol == "N")
                return ValueCangeType.Normal;
            return ValueCangeType.Fast;
        }

        private void InitServerOwnage(string ownage)
        {
            if (ownage == "")
                ResultOwnage = Ownage.Free;
            if (ownage == "f")
                ResultOwnage = Ownage.Free;
            if (ownage == "u")
                ResultOwnage = Ownage.User;
        }

    }
}