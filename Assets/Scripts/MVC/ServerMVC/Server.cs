﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.Game;
using Assets.Scripts.TriggersAndOperationsTypes;
using Assets.Scripts.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

namespace Assets.Scripts.MVC.ServerMVC
{
    [Serializable]
    public class Server
    {
        [field: SerializeField] public string ServerID { get; set; }
        [field: SerializeField] public int ServerGroup { get; set; }
        [field: SerializeField] public ServerPerInternetType Type { get; set; }
        public DateTrigger TriggerDate { get; set; }
        public AttitudeTrigger TriggerRebelOver { get; set; }
        public AttitudeTrigger TriggerRebelUnder { get; set; }
        public AttitudeTrigger TriggerLoyalOver { get; set; }
        public AttitudeTrigger TriggerLoyalUnder { get;  set; }
        public AttitudeResult ResultControl { get; set; }

        public List<string> TriggersSignal = new List<string>();
        [field: SerializeField] public float Range { get; set; }
        [field: SerializeField] public string Title { get; set; }
        [field: SerializeField] public string Description { get; set; }
        [field: SerializeField] public Ownage Ownage { get; set; }
        public bool IsActioned { get; set; }
        public string Icon { get; set; }
        [JsonIgnore] public Sprite IconSprite { get; set; }
        private GameInfo _gameInfo;
        public event System.Action<Ownage> OnChangeOwnage;
        public event Action OnAddedNewServerAction;
        private DateTriggerConventer _dateTriggerConventer;
        public List<ServerActionCase> ServerActionCases = new List<ServerActionCase>();
        public ServerActionCase CurrentServerActionCase { get; set; }

        public Server(GameInfo gameInfo,DateTriggerConventer dateTriggerConventer)
        {
            _gameInfo = gameInfo;
            _dateTriggerConventer = dateTriggerConventer;
        }

        public void InitDependecies(DateTriggerConventer dateTriggerConventer)
        {
            _dateTriggerConventer = dateTriggerConventer;
        }

        public void ChangeOwnage(Ownage ownage)
        {
            Ownage = ownage;
            OnChangeOwnage?.Invoke(Ownage);
        }

        public void LoadIcon()
        {
            if (Icon != null && Icon.Length > 0)
            {
                IconSprite = Resources.Load<Sprite>($"Sprites/Servers/{Icon.Replace(".png", "")}");
            }
        }

        public void AddServerActionCase(ServerActionCase serverAction)
        {
            ServerActionCases.Add(serverAction);
            TryGetServerActionCase(out ServerActionCase serverActionCase);
            if (serverAction.ServerActions.Count > 0)
            {
                OnAddedNewServerAction?.Invoke();
            }
            ServerActionCases = ServerActionCases.OrderByDescending(item => item.ActionCaseOrder).ToList();
        }

        public void TirggeredServerAction(ServerAction serverAction)
        {
            OnAddedNewServerAction?.Invoke();
        }

        public void RemoveActionFromCase(ServerAction serverAction)
        {
            if(CurrentServerActionCase != null)
            {
                CurrentServerActionCase.RemoveActionFromCase(serverAction);
            }
        }


        public bool TryGetServerActionCase(out ServerActionCase serverActionCase)
        {
            ServerActionCase[] serverCase = ServerActionCases.Where(item => item.ServerActions.Count> 0).OrderByDescending(x => x.ActionCaseOrder).ToArray();
            if (serverCase == null || serverCase.Length == 0)
            {
                serverActionCase = null;
                return false;
            }
            serverActionCase = serverCase[0];
            if(CurrentServerActionCase != null)
                CurrentServerActionCase.OnTriggeredServerAction -= TirggeredServerAction;

            CurrentServerActionCase = serverActionCase;
            CurrentServerActionCase.OnTriggeredServerAction += TirggeredServerAction;
            return true;
        }

        public bool TryGetServerActionCaseFromCurrent(out ServerActionCase serverActionCase)
        {
            if(CurrentServerActionCase == null)
            {
                serverActionCase = null;
                return false;
            }
            serverActionCase = CurrentServerActionCase;
            return true;
        }

        public void InitServer(ServerData serverData)
        {
            if(_dateTriggerConventer.TryConvertStringToDateTrigger(serverData.TriggerDate,out DateTrigger dateTrigger))
            {
                TriggerDate = dateTrigger;
            }
            ServerID = serverData.ServerID;
            ServerGroup = serverData.ServerGroup;
            string[] triggers = serverData.TriggerSignal.Split(',');
            TriggersSignal = triggers.ToList();
            Range = serverData.Range;
            Title = serverData.Title;
            Description = serverData.Description;
            Icon = serverData.Icon;
            InitTriggers(serverData);
            InitServerType(serverData.Type);
            InitServerOwnage(serverData.Ownage);
            if(Icon != null && Icon.Length > 0)
                IconSprite = Resources.Load<Sprite>($"Sprites/Servers/{Icon.Replace(".png","")}");


            if (serverData.ResultControl != 0)
            {
                if (serverData.ResultControl >= 0)
                    ResultControl = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(serverData.ResultControlSpeed), serverData.ResultControl, Accrual.CurrencyType.InternetControl, Accrual.OperationType.Increase);
                else
                    ResultControl = new AttitudeResult(_gameInfo.Slow, _gameInfo.Normal, _gameInfo.Fast, GetValueChangeBySymbol(serverData.ResultControlSpeed), serverData.ResultControl, Accrual.CurrencyType.InternetControl, Accrual.OperationType.Decrease);
            }

        }

        private ValueCangeType GetValueChangeBySymbol(string symbol)
        {
            if (symbol == "M")
                return ValueCangeType.Moment;
            else if (symbol == "S")
                return ValueCangeType.Slow;
            else if (symbol == "N")
                return ValueCangeType.Normal;
            return ValueCangeType.Fast;
        }

        private void InitServerOwnage(string ownage)
        {
            if (ownage == "")
                Ownage = Ownage.Free;
            if (ownage == "f")
                Ownage = Ownage.Free;
            if (ownage == "u")
                Ownage = Ownage.User;
        }

        private void InitServerType(string type)
        {
            if (type == "")
                Type = ServerPerInternetType.Inner;
            if(type == "i")
                Type = ServerPerInternetType.Inner;
            if(type == "o")
                Type = ServerPerInternetType.Outer;
        }

        private void InitTriggers(ServerData serverData)
        {
            if (serverData.TriggerLoyalOver != 0)
            {
                TriggerLoyalOver = new AttitudeTrigger(Accrual.OperationType.Increase, (int)serverData.TriggerLoyalOver);
            }
            if (serverData.TriggerLoyalUnder != 0)
            {
                TriggerLoyalUnder = new AttitudeTrigger(Accrual.OperationType.Decrease, (int)serverData.TriggerLoyalUnder);
            }
            if (serverData.TriggerRebelOver != 0)
            {
                TriggerRebelOver = new AttitudeTrigger(Accrual.OperationType.Increase, (int)serverData.TriggerRebelOver);
            }
            if (serverData.TriggerRebelUnder != 0)
            {
                TriggerRebelUnder = new AttitudeTrigger(Accrual.OperationType.Decrease, (int)serverData.TriggerRebelUnder);
            }
        }
    }
}