﻿using Assets.Scripts.GameStates;
using Assets.Scripts.InternetObjects.Servers;
using Assets.Scripts.MVC.ServerMVC.ServerViews;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MVC.ServerMVC
{
    public class ServerView : MonoBehaviour
    {
        [SerializeField] private GameObject _actionPanel;
        [SerializeField] private GameObject _panel;
        [SerializeField] private Image _icon;
        [SerializeField] private TMP_Text _titleText;
        [SerializeField] private TMP_Text _descriptionText;
        [Inject] private ServerActionBaseView _serverActionBaseView;
        [Inject] private ServerActionBuyView _serverActionBuyView;
        [Inject] private ServerActionCaptureView _serverActionCaptureView;
        [Inject] private GameState _gameState;

        private Server _currentServer;
        private ServerActionCase _serverActionCase;
        private IServerActionViewState _serverActionViewState;
        private ServerObject _currentServerObject;

        public void InitServer(Server server , ServerObject serverObject)
        {
            _currentServer = server;
            _currentServerObject = serverObject;
            _titleText.text = _currentServer.Title;
            _descriptionText.text = _currentServer.Description;
            if (server.IconSprite != null)
            {
                _icon.gameObject.SetActive(true);
                _icon.sprite = server.IconSprite;
            }
            else
            {
                _icon.gameObject.SetActive(false);
            }
            InitState();
        }

        public void InitState()
        {
            if (_currentServer.TryGetServerActionCase(out ServerActionCase serverAction))
            {
                _actionPanel.SetActive(true);
                _serverActionCase = serverAction;
                if (_serverActionCase.ServerActions.Count == 0 || _serverActionCase.ServerActions[0].IsEmpty)
                {
                    SetInServerActionState(_serverActionBaseView);
                }
                else
                {
                    SetInServerActionState(_serverActionBuyView);
                }
            }
            else
            {
                SetInServerActionState(_serverActionBaseView);
            }
        }


        public void SetInServerActionState(IServerActionViewState state)
        {
            if (_serverActionViewState != null)
                _serverActionViewState.ExitFromState();
            _serverActionViewState = state;
            _serverActionViewState.EnterInState(_currentServer, _serverActionCase);
        }

        public void OpenPanel()
        {
            InitState();
            _gameState.ChangeGameState(GameStates.GameStates.Server);
            _panel.SetActive(true);
        }

        public void ClosePanel()
        {
            _gameState.ChangeGameState(GameStates.GameStates.InternetMap);
            _currentServerObject.OffNotification();
            _panel.SetActive(false);
        }
    }
}