﻿using Assets.Scripts.InternetObjects.Servers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.MVC.ServerMVC
{
    public class OuterServerLineService : MonoBehaviour
    {
        [SerializeField] private GameObject _point;
        [SerializeField] private float _circleRadius;
        [SerializeField] private ServerOuterLine _serverOuterLinePrefab;
        [SerializeField] private Transform _circleCenterPoint;
        [SerializeField] private float _radCofficient;
        [SerializeField] private List<ServerOuterLine> _serverOuterLines;
        [SerializeField] private List<Vector3> _pointsOnCircle;

        private void Awake()
        {
            _pointsOnCircle = GetPointsOnCircle();
        }

        public void CreateServerObjectLine(ServerObject serverObject)
        {
            if (serverObject == null)
            {
                Debug.LogError("Error server null");
            }
            else
            {
                Vector3 closestPointOnCicrle = GetPointByX(serverObject.transform.position);

                Vector3 middlePoint = new Vector3(serverObject.transform.position.x,
                    (serverObject.transform.position.y + closestPointOnCicrle.y) / 2, 0);

                ServerOuterLine serverOuterLine = Instantiate(_serverOuterLinePrefab,
                    serverObject.transform.position, Quaternion.identity);
                _serverOuterLines.Add(serverOuterLine);
                serverOuterLine.SetPoints(0, serverObject.transform.position);
                serverOuterLine.SetPoints(1, middlePoint);
                Vector3 pointConnectionWithCicrle = GetClosestPoint(middlePoint);
                serverOuterLine.SetPoints(2, pointConnectionWithCicrle);
            }
        }

        private Vector3 GetPointByX(Vector3 point)
        {
            float maxYPointPosition;
            bool isUpPoint;

            if(point.y < _circleCenterPoint.position.y)
            {
                isUpPoint = false;
                maxYPointPosition = _circleCenterPoint.position.y - 1;
            }
            else
            {
                isUpPoint = true;
                maxYPointPosition = _circleCenterPoint.position.y + 1;
            }


            Vector3 currentClosest = _pointsOnCircle[0];
            foreach (var item in _pointsOnCircle)
            {
                if (Mathf.Abs(item.x - point.x) < Mathf.Abs(currentClosest.x - point.x) && CheckPointHeight(isUpPoint, maxYPointPosition, item.y))
                {
                    currentClosest = item;
                }

            }
            return currentClosest;
        }

        private bool CheckPointHeight(bool isUpPoint , float MaxYPoint , float height)
        {
            if (isUpPoint)
            {
                if (MaxYPoint < height)
                    return true;
                return false;
            }else
            {
                if (MaxYPoint > height)
                    return true;
                return false;
            }
        }

        private List<Vector3> GetPointsOnCircle()
        {
            List<Vector3> points = new List<Vector3>();
            float angle = 0;
            float radCofficient = _radCofficient;

            float pointsCount = 360 / radCofficient;

            for (int i = 0; i < pointsCount; i++)
            {
                var x = Mathf.Cos(angle * radCofficient) * _circleRadius;
                var y = Mathf.Sin(angle * radCofficient) * _circleRadius;
                angle += _radCofficient;
                points.Add(new Vector3(x, y, 0) + _circleCenterPoint.transform.position);
            }
            return points;
        }

        private Vector3 GetClosestPoint(Vector3 point)
        {
           
            Vector3 currentClosest = _pointsOnCircle[0];
            foreach(var item in _pointsOnCircle)
            {
                if (Vector3.Distance(point, item) < Vector3.Distance(point, currentClosest))
                {
                    currentClosest = item;
                }
            }
            return currentClosest;
        }
        
    }
}