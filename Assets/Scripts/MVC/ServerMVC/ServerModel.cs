﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.Game;
using Assets.Scripts.InternetObjects.Servers;
using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.ServerMVC
{
    public class ServerModel : MonoBehaviour
    {
        [Inject] private DateTriggerConventer _dateTriggerConventer;
        [Inject] private GameInfo _gameInfo;
        [SerializeField] private List<Server> _servers = new List<Server>();
        [SerializeField] private List<Server> _serversTriggerQueue = new List<Server>();
        private List<ServerActionCase> _serversActionCasesQueue = new List<ServerActionCase>();
        private List<ServerActionCase> _serverActionCases = new List<ServerActionCase>();
        private List<ServerAction> _serverActions = new List<ServerAction>();

        public IEnumerable<Server> Servers { get { return _servers; } }
        public IReadOnlyList<Server> ServersTriggerQueue { get { return _serversTriggerQueue; } }
        public IReadOnlyList<ServerActionCase> ServerActionCasesQueue => _serversActionCasesQueue;
        public IReadOnlyList<ServerAction> ServerActions => _serverActions;

        public event System.Action OnInitedServers;

        public void SetServerActionCasesQueue(List<ServerActionCase> actionCases)
        {
            _serversActionCasesQueue = actionCases;
        }

        public void SetServerTriggerQueue(List<Server> servers)
        {
            _serversTriggerQueue = servers;
        }

        public void SetServers(List<Server> servers)
        {
            _servers = servers;
        }

        public void SetServerAction(List<ServerAction> serverActions)
        {
            _serverActions = serverActions;
        }

        public void InitServers()
        {
            OnInitedServers?.Invoke();
        }

        public void InitServers(List<ServerData> serverDatas)
        {
            List<Server> serverTemplateQueue = new List<Server>();
            foreach (var item in serverDatas)
            {
                Server server = new Server(_gameInfo,_dateTriggerConventer);
                server.InitServer(item);
                _servers.Add(server);
                serverTemplateQueue.Add(server);
            }
            List<ServerPairs> serverPairs = new List<ServerPairs>();
            List<int> serverIDGrouped = new List<int>();
            for(int i = 0; i < serverTemplateQueue.Count; i++)
            {
                if(serverTemplateQueue.Where(x => x.ServerGroup == serverTemplateQueue[i].ServerGroup &&
                !serverIDGrouped.Contains(serverTemplateQueue[i].ServerGroup)).ToArray().Length >= 2)
                {
                    serverIDGrouped.Add(serverTemplateQueue[i].ServerGroup);
                    ServerPairs serverPair = new ServerPairs();
                    serverPair.Servers = serverTemplateQueue.Where(x => x.ServerGroup == serverTemplateQueue[i].ServerGroup).ToList();
                    serverPairs.Add(serverPair);
                    serverPair.Servers.Shuffle();
                    for(int j = 0; j < serverPair.Servers.Count - 1; j++)
                        serverTemplateQueue.Remove(serverPair.Servers[j]);
                }
            }
            _serversTriggerQueue = serverTemplateQueue;
            OnInitedServers?.Invoke();
        }

        public void InitServerActionCases(List<ServerActionCasesData> serverActionCasesDatas)
        {
            foreach (var item in serverActionCasesDatas)
            {
                ServerActionCase server = new ServerActionCase();
                server.InitDependecies(_dateTriggerConventer);
                server.Init(item);
                _serverActionCases.Add(server);
                _serversActionCasesQueue.Add(server);
            }
        }

        public void DistrebuteServerActionCases(ServerActionCase serverActionCase)
        {
            foreach(var item in _servers)
            {
                if(item.ServerID == serverActionCase.ServerID)
                    item.AddServerActionCase(serverActionCase);
            }
        }

        private void DistrebuteServerAction(ServerAction serverAction)
        {
            foreach(var item in _serverActionCases)
            {
                if(item.ActionCaseID == serverAction.ActionCaseID)
                {
                    item.AddServerAction(serverAction);
                }
            }
        }

        public void InitServerAction(List<ServerActionData> serverActionDatas)
        {
            foreach(var item in serverActionDatas)
            {
                ServerAction serverAction = new ServerAction(_gameInfo);
                serverAction.Init(item);
                _serverActions.Add(serverAction);
                DistrebuteServerAction(serverAction);
            }
        }

        public bool TryGetServerByID(string id , out Server server)
        {
            server = _servers.SingleOrDefault(item => item.ServerID == id);
            return server != null;
        }

        public void RemoveServerActionCaseFromQueue(ServerActionCase serverActionCase)
        {
            _serversActionCasesQueue.Remove(serverActionCase);
        }

        public void RemoveServerFromQueue(Server server)
        {
            _serversTriggerQueue.Remove(server);
        }

        private class ServerPairs
        {
            public List<Server> Servers = new List<Server>();
        }
    }
}