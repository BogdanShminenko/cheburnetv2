﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.ServerMVC
{
    public class ServerOuterLine : MonoBehaviour
    {
        [SerializeField] private LineRenderer _lineRenderer;


        public void SetPoints(int index , Vector3 position)
        {
            _lineRenderer.SetPosition(index, position);
        }

    }
}