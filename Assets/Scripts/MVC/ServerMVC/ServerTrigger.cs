﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Game;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.MVC.UsersAttitude;
using Assets.Scripts.TriggersAndOperationsTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.ServerMVC
{
    public class ServerTrigger : MonoBehaviour
    {
        [Inject] private TimerModel _timerModel;
        [Inject] private GameInfo _gameInfo;

        public bool CheckTrigered(Server server)
        {
            if (TryTriggerEventByDate(server.TriggerDate) 
                && CheckByAttitude(server.TriggerLoyalOver, CurrencyType.Loyal)
                && CheckByAttitude(server.TriggerLoyalUnder, CurrencyType.Loyal)
                && CheckByAttitude(server.TriggerRebelOver, CurrencyType.Rebel)
                && CheckByAttitude(server.TriggerRebelUnder, CurrencyType.Rebel)
                && CheckByTriggers(server.TriggersSignal))
            {
                return true;
            }
            return false;
        }

        private bool CheckByTriggers(IReadOnlyCollection<string> signals)
        {
            int count = 0;
            if (signals.Count == 0)
                return true;
            foreach (string signal in signals)
            {
                if(_gameInfo.CheckTrigger(signal) || signal == "null" || signal.Length == 0)
                {
                    count++;
                }
            }
            if(count == signals.Count)
            {
                return true;
            }
            return false;
        }

        private bool CheckByAttitude(AttitudeTrigger attitudeTrigger , CurrencyType currencyType)
        {
            if(attitudeTrigger == null)
            {
                return true;
            }
            if(attitudeTrigger.AccuralDirection == OperationType.Increase)
            {
                if (attitudeTrigger.CurrentAdditude <= _gameInfo.GetAttitudeByType(currencyType))
                {
                    return true;
                }
            }
            else if (attitudeTrigger.AccuralDirection == OperationType.Decrease)
            {
                if (attitudeTrigger.CurrentAdditude >= _gameInfo.GetAttitudeByType(currencyType))
                {
                    return true;
                }
            }

            return false;
        }

        private bool TryTriggerEventByDate(DateTrigger dateTrigger)
        {
            if (dateTrigger == null)
                return true;
            if(dateTrigger.Year <= _timerModel.CurrentYear && dateTrigger.Month <= _timerModel.CurrentMonth)
            {
                return true;
            }
            //if (new DateTime(dateTrigger.Year, dateTrigger.Month, 2) <= new DateTime((int)_timerModel.CurrentYear, (int)_timerModel.CurrentMonth, 1))
            //{
            //    return true;
            //}
            return false;
        }


    }
}