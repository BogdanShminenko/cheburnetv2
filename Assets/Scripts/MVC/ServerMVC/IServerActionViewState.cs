﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.MVC.ServerMVC
{
    public interface IServerActionViewState
    {
        void EnterInState(Server server, ServerActionCase serverAction);
        void ExitFromState();
    }
}