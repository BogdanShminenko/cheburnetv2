﻿using Assets.Scripts.MVC.CurrencyMVC;
using System.Collections;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.ServerMVC.ServerViews
{
    public class ServerActionBlockView : MonoBehaviour, IServerActionViewState
    {
        [SerializeField] private TMP_Text _titleText;
        [SerializeField] private TMP_Text _descriptionText;
        [SerializeField] private TMP_Text _buyMoneyPriceText;
        [SerializeField] private TMP_Text _buyWillPriceText;
        [SerializeField] private GameObject _panel;


        [Inject] private MoneyModel _moneyModel;
        [Inject] private WillModel _willModel;
        [Inject] private CurrencyController _currencyController;
        [Inject] private ServerController _serverController;

        private ServerAction _serverAction;
        private Server _server;

        public void EnterInState(Server server, ServerAction serverAction)
        {
            _server = server;
            _serverAction = serverAction;
            _titleText.text = server.Title;
            _descriptionText.text = _serverAction.Description;
            _buyMoneyPriceText.text = $"Заблокировать за \n {_serverAction.PriceMoney} \n Денег";
            _buyWillPriceText.text = $"Заблокировать за \n {_serverAction.PriceWill} \n Воли";
            _panel.SetActive(true);
        }

        public void ExitFromState()
        {
            _panel.SetActive(false);
        }

        public void BlockForMoney()
        {
            if (_serverAction != null)
            {
                if (_moneyModel.CurrentAmount >= _serverAction.PriceMoney)
                {
                    _currencyController.SpendMoney(_serverAction.PriceMoney);
                    _serverController.HandleServerAction(_server, _serverAction);
                }
            }
        }

        public void BlockForWill()
        {
            if (_serverAction != null)
            {
                if (_willModel.CurrentAmount >= _serverAction.PriceWill)
                {
                    _currencyController.SpendWill(_serverAction.PriceWill);
                    _serverController.HandleServerAction(_server, _serverAction);
                }
            }
        }

        public void EnterInState(Server server, ServerActionCase serverAction)
        {
        }
    }
}