﻿using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.MVC.ProjectMVC;
using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.ServerMVC.ServerViews
{
    public class ServerActionBuyView : MonoBehaviour, IServerActionViewState
    {
        [SerializeField] private TMP_Text _titleText;
        [SerializeField] private TMP_Text _descriptionText;
        [SerializeField] private GameObject _panel;
        [SerializeField] private Transform _parent;

        [Inject] private ServerActionButton _serverActionButtonPrefab;
        [Inject] private MoneyModel _moneyModel;
        [Inject] private WillModel _willModel;
        [Inject] private CurrencyController _currencyController;
        [Inject] private ServerController _serverController;
        private ServerActionCase _serverActionCase;
        private Server _server;

        private List<ServerActionButton> _serverActionButtons = new List<ServerActionButton>();

        public void EnterInState(Server server, ServerActionCase serverActionCase)
        {
            _titleText.text = server.Title;
            _descriptionText.text = server.Description;
            _server = server;
            _serverActionCase = serverActionCase;
            _serverActionCase.OnTriggeredServerAction += CreateServerAction;
            foreach (var item in _serverActionCase.ServerActions)
                CreateServerAction(item);
            _panel.SetActive(true);
        }

        public void ExitFromState()
        {
            _serverActionCase.OnTriggeredServerAction -= CreateServerAction;
            foreach (var item in _serverActionButtons)
                Destroy(item.gameObject);
            _serverActionButtons.Clear();
            _panel.SetActive(false);
        }
        
        private void CreateServerAction(ServerAction serverAction)
        {
            ServerActionButton serverActionButton = Instantiate(_serverActionButtonPrefab, _parent);
            serverActionButton.InitDependecies(_moneyModel, _willModel, _currencyController, _serverController);
            serverActionButton.Init(_server, serverAction);
            _serverActionButtons.Add(serverActionButton);
        }

    }
}