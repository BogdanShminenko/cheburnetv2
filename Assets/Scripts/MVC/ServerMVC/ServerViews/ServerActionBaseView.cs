﻿using System.Collections;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

namespace Assets.Scripts.MVC.ServerMVC.ServerViews
{
    public class ServerActionBaseView : MonoBehaviour, IServerActionViewState
    {
        [SerializeField] private TMP_Text _text;
        [SerializeField] private GameObject _panel;

        public void EnterInState(Server server, ServerActionCase serverAction)
        {
            _panel.SetActive(true);
        }

        public void ExitFromState()
        {
            _panel.SetActive(false);
        }
    }
}