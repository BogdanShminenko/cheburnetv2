﻿using Assets.Scripts.MVC.CurrencyMVC;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Event = AK.Wwise.Event;

namespace Assets.Scripts.MVC.ServerMVC.ServerViews
{
    public class ServerActionButton : MonoBehaviour
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public Event _buyEvent;
            public Event _disabledEvent;
        }
        
        [SerializeField] private Sprite _activeImage;
        [SerializeField] private Sprite _inactiveImage;
        [SerializeField] private Image _buyImage;
        [SerializeField] private TMP_Text _titleText;
        [SerializeField] private TMP_Text _moneyPriceText;
        [SerializeField] private TMP_Text _willPriceText;
        [SerializeField] private GameObject _moneyObject;
        [SerializeField] private GameObject _willObject;
        
        [SerializeField] private WwiseSound _wwiseSound;

        private MoneyModel _moneyModel;
        private WillModel _willModel;
        private CurrencyController _currencyController;
        private ServerController _serverController;

        private ServerAction _serverAction;
        private Server _server;

        private bool _isHaveWill;
        private bool _isHaveMoney;

        public void InitDependecies(MoneyModel moneyModel, WillModel willModel , CurrencyController currencyController, ServerController serverController)
        {
            _moneyModel = moneyModel;
            _willModel = willModel;
            _currencyController = currencyController;
            _serverController = serverController;
        }

        public void Init(Server server, ServerAction serverAction)
        {
            _server = server;
            _serverAction = serverAction;
            _titleText.text = _serverAction.Title;
            if (_serverAction.PriceMoney != 0)
            {
                _isHaveMoney = true;
                _moneyPriceText.text = $"{_serverAction.PriceMoney}";
                _moneyObject.SetActive(true);
            }
            if (_serverAction.PriceWill != 0)
            {
                _isHaveWill = true; 
                _willPriceText.text = $"{_serverAction.PriceWill}";
                _willObject.SetActive(true);
            }
            if(_isHaveWill && !_isHaveMoney)
            {
                if (_willModel.CurrentAmount >= _serverAction.PriceWill)
                {
                    _buyImage.sprite = _activeImage;
                }
                else
                {
                    _buyImage.sprite = _inactiveImage;
                }
            }else if (!_isHaveWill && _isHaveMoney)
            {
                if (_moneyModel.CurrentAmount >= _serverAction.PriceMoney)
                {
                    _buyImage.sprite = _activeImage;
                }
                else
                {
                    _buyImage.sprite = _inactiveImage;
                }
            }
            else if (_isHaveWill && _isHaveMoney)
            {
                if (_moneyModel.CurrentAmount >= _serverAction.PriceMoney && _willModel.CurrentAmount >= _serverAction.PriceWill)
                {
                    _buyImage.sprite = _activeImage;
                }
                else
                {
                    _buyImage.sprite = _inactiveImage;
                }
            }

            _moneyModel.OnCurrencyUpdate += UpdateButtonsStatus;
            _willModel.OnCurrencyUpdate += UpdateButtonsStatus;
        }

        private void UpdateButtonsStatus()
        {
   
            if (_isHaveWill && !_isHaveMoney && _buyImage != null)
            {
                if (_willModel.CurrentAmount >= _serverAction.PriceWill)
                {
                    _buyImage.sprite = _activeImage;
                }
                else
                {
                    _buyImage.sprite = _inactiveImage;
                }
            }
            else if (!_isHaveWill && _isHaveMoney && _buyImage != null)
            {
                if (_moneyModel.CurrentAmount >= _serverAction.PriceMoney)
                {
                    _buyImage.sprite = _activeImage;
                }
                else
                {
                    _buyImage.sprite = _inactiveImage;
                }
            }
            else if (_isHaveWill && _isHaveMoney && _buyImage != null)
            {
                if (_moneyModel.CurrentAmount >= _serverAction.PriceMoney && _willModel.CurrentAmount >= _serverAction.PriceWill)
                {
                    _buyImage.sprite = _activeImage;
                }
                else
                {
                    _buyImage.sprite = _inactiveImage;
                }
            }
        }

        public void Buy()
        {
            var isBuyed = false;
            
            if (_serverAction != null)
            {
                if (_moneyModel.CurrentAmount >= _serverAction.PriceMoney &&
                    _willModel.CurrentAmount >= _serverAction.PriceWill)
                {
                    _currencyController.SpendWill(_serverAction.PriceWill);
                    _currencyController.SpendMoney(_serverAction.PriceMoney);
                    _server.IsActioned = true;
                    _serverController.HandleServerAction(_server, _serverAction);

                    _wwiseSound._buyEvent.Post(gameObject);
                    isBuyed = true;
                }
            }

            if (!isBuyed)
            {
                _wwiseSound._disabledEvent.Post(gameObject);
            }
        }
    }
}