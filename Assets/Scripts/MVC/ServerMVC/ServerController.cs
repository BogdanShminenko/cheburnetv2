﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Data.GameData;
using Assets.Scripts.Data.GameData.Configs;
using Assets.Scripts.Game;
using Assets.Scripts.GameStates;
using Assets.Scripts.InternetObjects.Servers;
using Assets.Scripts.InternetObjects.User;
using Assets.Scripts.MVC.InternetControllMVC;
using Assets.Scripts.MVC.InternetMVC;
using Assets.Scripts.Utilities;
using ModestTree.Util;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.ServerMVC
{
    public class ServerController : MonoBehaviour
    {
        [Inject] private ServerObject _serverObjectPreab;
        [Inject] private InternetController _internetController;
        [Inject] private ServerTrigger _serverTrigger;
        [Inject] private ServerModel _serverModel;
        [Inject] private ServerView _serverView;
        [Inject] private ServerConfig _serverConfig;
        [Inject] private ServerActionCaseDatasConfig _serverActionCaseDatasConfig;
        [Inject] private ServerActionDataConfig _serverActionDataConfig;
        [Inject] private ServerController _serverController;
        [Inject] private GameInfo _gameInfo;
        [Inject] private ServerActionTrigger _serverActionTrigger;
        [Inject] private AccuralInLoop _accuralInLoop;
        [Inject] private CheckUI _checkUI;
        [Inject] private GameState _gameState;

        public event System.Action OnCreatedFirstServer;

        public void InitServers()
        {
            _serverModel.InitServers(_serverConfig.ServerDatas);
            _serverModel.InitServerActionCases(_serverActionCaseDatasConfig.ServerActionCasesDatas);
            _serverModel.InitServerAction(_serverActionDataConfig.ServerActionDatas);
        }

        public void HandleServerAction(Server server, ServerAction serverAction)
        {
            if (serverAction.ResultRebel != null)
                _accuralInLoop.HandleAttitudeResult(serverAction.ResultRebel);
            if (serverAction.ResultLoyal != null)
                _accuralInLoop.HandleAttitudeResult(serverAction.ResultLoyal);
            if (serverAction.ResultControl != null)
                _accuralInLoop.HandleAttitudeResult(serverAction.ResultControl);

            server.ChangeOwnage(serverAction.ResultOwnage);
            server.RemoveActionFromCase(serverAction);
            _gameInfo.AddTrigger(serverAction.ResultTrigger);
            CallTriggerServerActionCase();
            _serverView.InitState();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                for(int i = 0; i < 10; i++)
                {
                    _internetController.CreateInternetMapObject(_serverObjectPreab, ServerPerInternetType.Outer, out ServerObject server);
                }
            }
        }

        public void CallTriggerServer()
        {
            for(int i = 0; i < _serverModel.ServersTriggerQueue.Count; i++)
            {
                if (_serverTrigger.CheckTrigered(_serverModel.ServersTriggerQueue[i]))
                {
                    _internetController.CreateInternetMapObject(_serverObjectPreab, _serverModel.ServersTriggerQueue[i].Type, out ServerObject server);
                    if (server != null)
                    {
                        server.InitServer(_gameState, _checkUI, _serverModel.ServersTriggerQueue[i], _serverController);
                        if (_serverModel.ServersTriggerQueue[i].ResultControl != null)
                            _accuralInLoop.HandleAttitudeResult(_serverModel.ServersTriggerQueue[i].ResultControl);
                        _serverModel.RemoveServerFromQueue(_serverModel.ServersTriggerQueue[i]);
                        OnCreatedFirstServer?.Invoke();
                    }
                }
            }
        }

        public void CallTriggerServerActionCase()
        {
            for (int i = 0; i < _serverModel.ServerActionCasesQueue.Count; i++)
            {
                if (_serverActionTrigger.CheckTrigered(_serverModel.ServerActionCasesQueue[i]))
                {
                    _serverModel.DistrebuteServerActionCases(_serverModel.ServerActionCasesQueue[i]);
                    _serverModel.RemoveServerActionCaseFromQueue(_serverModel.ServerActionCasesQueue[i]);
                }
            }
        }

        public void SelectServer(Server server , ServerObject serverObject)
        {
            _serverView.InitServer(server , serverObject);
            _serverView.OpenPanel();
        }

    }
}