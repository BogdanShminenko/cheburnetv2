﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.TriggersAndOperationsTypes;
using Assets.Scripts.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.MVC.ServerMVC
{
    [Serializable]
    public class ServerActionCase
    {
        public string ServerID { get; set; }
        public string ActionCaseID { get; set; }
        [field: SerializeField] public int ActionCaseOrder { get; set; }
        public string TriggerDate { get; set; }
        public AttitudeTrigger TriggerRebelOver { get; set; }
        public AttitudeTrigger TriggerRebelUnder { get; set; }
        public AttitudeTrigger TriggerLoyalOver { get; set; }
        public AttitudeTrigger TriggerLoyalUnder { get; set; }
        public string TriggerSignal { get; set; }
        public DateTrigger DateTrigger { get; set; }
        public List<ServerAction> ServerActions = new List<ServerAction>();

        public event Action<ServerAction> OnTriggeredServerAction;

        private DateTriggerConventer _dateTriggerConventer;

        public void InitDependecies(DateTriggerConventer dateTriggerConventer)
        {
            _dateTriggerConventer = dateTriggerConventer;
        }
        public void Init(ServerActionCasesData serverActionCase)
        {
            ServerID = serverActionCase.ServerID;
            ActionCaseID = serverActionCase.ActionCaseID;
            ActionCaseOrder = serverActionCase.ActionCaseOrder;
            TriggerDate = serverActionCase.TriggerDate; 
            TriggerSignal = serverActionCase.TriggerSignal;
            if(_dateTriggerConventer.TryConvertStringToDateTrigger(TriggerDate, out DateTrigger dateTrigger))
            {
                DateTrigger = dateTrigger;
            }
            InitTriggers(serverActionCase);
        }

        public void AddServerAction(ServerAction serverAction)
        {
            ServerActions.Add(serverAction);
            ServerActions.OrderBy(item => item.OrderNumber);
            OnTriggeredServerAction?.Invoke(serverAction);
        }

        public void RemoveActionFromCase(ServerAction serverAction)
        {
            ServerActions.Remove(serverAction);
        }

        public bool TryGetServerAction(out ServerAction serverAction)
        {
            if (ServerActions.Count == 0)
            {
                serverAction = null;
                return false;
            }

            serverAction = ServerActions[0];
            return true;
        }

        private void InitTriggers(ServerActionCasesData serverActionCase)
        {
            if (serverActionCase.TriggerLoyalOver != 0)
            {
                TriggerLoyalOver = new AttitudeTrigger(Accrual.OperationType.Increase, (int)serverActionCase.TriggerLoyalOver);
            }
            if (serverActionCase.TriggerLoyalUnder != 0)
            {
                TriggerLoyalUnder = new AttitudeTrigger(Accrual.OperationType.Decrease, (int)serverActionCase.TriggerLoyalUnder);
            }
            if (serverActionCase.TriggerRebelOver != 0)
            {
                TriggerRebelOver = new AttitudeTrigger(Accrual.OperationType.Increase, (int)serverActionCase.TriggerRebelOver);
            }
            if (serverActionCase.TriggerRebelUnder != 0)
            {
                TriggerRebelUnder = new AttitudeTrigger(Accrual.OperationType.Decrease, (int)serverActionCase.TriggerRebelUnder);
            }
        }
    }
}