﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.QuestMVC
{
    public class QuestModel : MonoBehaviour
    {
        [Inject] private DateTriggerConventer _dateTriggerConventer;

        [SerializeField] private List<Quest> _questsQueue = new List<Quest>();
        [SerializeField] private List<Quest> _triggeredQuests = new List<Quest>();
        
        public IList<Quest> QuestsQueue => _questsQueue;
        public IList<Quest> TriggeredQuests => _triggeredQuests;

        public event Action<Quest> OnQuestTriggered;
        public event Action OnQuestCompleted;

        public void SetQuestsFromSave(List<Quest> questsQueue, List<Quest> triggerQuests)
        {
            _questsQueue = questsQueue;
            _triggeredQuests = triggerQuests;

            for (int i = 0; i < _triggeredQuests.Count; i++)
            {
                OnQuestTriggered?.Invoke(_triggeredQuests[i]);
            }
        }

        public void InitQuests(List<QuestData> quests)
        {
            foreach(var item in quests)
            {
                Quest quest = new Quest();
                quest.InitDepedencies(_dateTriggerConventer);
                quest.Init(item);
                _questsQueue.Add(quest);
            }
        }

        public void CompleteQuest(Quest quest)
        {
            Debug.Log("Quest Completed");
            OnQuestCompleted?.Invoke();
            quest.ChangeQuestStatus(QuestStatus.Complete);
        }

        public void TriggerQuest(Quest quest, QuestStatus questCreateStatus = QuestStatus.New)
        {
            _triggeredQuests.Add(quest);
            _questsQueue.Remove(quest);
            quest.ChangeQuestStatus(QuestStatus.New);
            OnQuestTriggered?.Invoke(quest);
        }
    }
}