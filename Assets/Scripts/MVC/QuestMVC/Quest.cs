﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.TriggersAndOperationsTypes;
using Assets.Scripts.Utilities;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.MVC.QuestMVC
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum QuestStatus
    {
        Incomplete = 0,
        New = 1,
        Complete = 2
    }

    [Serializable]
    public class Quest
    {
        [field: SerializeField] public QuestStatus Status { get; set; } = QuestStatus.New;
        [field: SerializeField] public string QuestID { get; set; }
        [field: SerializeField] public DateTrigger TriggerDate { get; set; }
        [field: SerializeField] public AttitudeTrigger TriggerRebelOver { get; set; }
        [field: SerializeField] public AttitudeTrigger TriggerRebelUnder { get; set; }
        [field: SerializeField] public AttitudeTrigger TriggerLoyalOver { get; set; }
        [field: SerializeField] public AttitudeTrigger TriggerLoyalUnder { get; set; }
        [field: SerializeField] public List<string> TriggerSignals = new List<string>();
        [field: SerializeField] public string Title { get; set; }
        [field: SerializeField] public string Description { get; set; }
        [field: SerializeField] public DateTrigger CheckBeforeDate { get; set; }
        [field: SerializeField] public AttitudeTrigger CheckRebelOver { get; set; }
        [field: SerializeField] public AttitudeTrigger CheckRebelUnder { get; set; }
        [field: SerializeField] public AttitudeTrigger CheckLoyalOver { get; set; }
        [field: SerializeField] public AttitudeTrigger CheckLoyalUnder { get; set; }
        [field: SerializeField] public AttitudeTrigger CheckControlOver { get; set; }
        [field: SerializeField] public List<string> CheckSignals = new List<string>();


        private DateTriggerConventer _dateTriggerConventer;

        public event Action OnChangeStatus;

        public void InitDepedencies(DateTriggerConventer dateTriggerConventer)
        {
            _dateTriggerConventer = dateTriggerConventer;
        }

        public void Init(QuestData data)
        {
            QuestID = data.QuestID;
            Title = data.Title;
            Description = data.Description;
            InitTriggers(data);
            InitCheckCondition(data);
            InitSignal(data.CheckSignal, ref CheckSignals);
            InitSignal(data.TriggerSignal, ref TriggerSignals);

            if(_dateTriggerConventer.TryConvertStringToDateTrigger(data.TriggerDate, out DateTrigger triggerDate))
                TriggerDate = triggerDate;

            if (_dateTriggerConventer.TryConvertStringToDateTrigger(data.CheckBeforeDate, out DateTrigger checkBeforeDate))
                CheckBeforeDate = checkBeforeDate;
        }

        public void ChangeQuestStatus(QuestStatus status)
        {
            Status = status;
            OnChangeStatus?.Invoke();
        }

        private void InitSignal(string signalsString,ref List<string> signals)
        {
            if (signalsString == "" || signalsString.Length == 0)
            {
                signals.Add("null");
            }
            else
            {
                string[] triggers = signalsString.Split(',');
                signals = triggers.ToList();
            }
        }

        private void InitTriggers(QuestData data)
        {
            if (data.TriggerLoyalOver != 0)
            {
                TriggerLoyalOver = new AttitudeTrigger(Accrual.OperationType.Increase, data.TriggerLoyalOver);
            }
            if (data.TriggerLoyalUnder != 0)
            {
                TriggerLoyalUnder = new AttitudeTrigger(Accrual.OperationType.Decrease, data.TriggerLoyalUnder);
            }
            if (data.TriggerRebelOver != 0)
            {
                TriggerRebelOver = new AttitudeTrigger(Accrual.OperationType.Increase, data.TriggerRebelOver);
            }
            if (data.TriggerRebelUnder != 0)
            {
                TriggerRebelUnder = new AttitudeTrigger(Accrual.OperationType.Decrease, data.TriggerRebelUnder);
            }
        }

        private void InitCheckCondition(QuestData data)
        {
            if (data.CheckLoyalOver != 0)
            {
                CheckLoyalOver = new AttitudeTrigger(Accrual.OperationType.Increase, data.CheckLoyalOver);
            }
            if (data.CheckLoyalUnder != 0)
            {
                CheckLoyalUnder = new AttitudeTrigger(Accrual.OperationType.Decrease, data.CheckLoyalUnder);
            }
            if (data.CheckRebelOver != 0)
            {
                CheckRebelOver = new AttitudeTrigger(Accrual.OperationType.Increase, data.CheckRebelOver);
            }
            if (data.CheckRebelUnder != 0)
            {
                CheckRebelUnder = new AttitudeTrigger(Accrual.OperationType.Decrease, data.CheckRebelUnder);
            }
            if(data.CheckControlOver != 0)
            {
                CheckControlOver = new AttitudeTrigger(Accrual.OperationType.Increase, data.CheckControlOver);
            }
        }
    }
}