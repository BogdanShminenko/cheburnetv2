﻿using System.Collections;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.QuestMVC
{
    public class QuestInfoPanel : MonoBehaviour
    {
        [SerializeField] private GameObject _questPanel;
        [SerializeField] private TMP_Text _questTitle;
        [SerializeField] private TMP_Text _questDescription;

        private Quest _currentQuest;

        public void InitQuest(Quest quest)
        {
            _currentQuest = quest;
            _questTitle.text = _currentQuest.Title;
            _questDescription.text = _currentQuest.Description;
        }

        public void OpenPanel()
        {
            _questPanel.SetActive(true);
        }

        public void Close()
        {
            _questPanel.SetActive(false);
        }
    }
}