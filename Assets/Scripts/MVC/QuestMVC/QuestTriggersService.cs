﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Game;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.TriggersAndOperationsTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.QuestMVC
{
    public class QuestTriggersService
    {
        [Inject] private QuestModel _questModel;
        [Inject] private TimerModel _timerModel;
        [Inject] private GameInfo _gameInfo;

        public bool CheckTriggerByDate(Quest quest)
        {
            if (quest.TriggerDate == null)
                return true;

            if (new DateTime(quest.TriggerDate.Year, quest.TriggerDate.Month, 1) <= new DateTime((int)_timerModel.CurrentYear, (int)_timerModel.CurrentMonth, 1))
            {
                return true;
            }
            return false;
        }

        private bool CheckByAttitude(AttitudeTrigger attitudeTrigger, CurrencyType currencyType)
        {
            if (attitudeTrigger == null)
            {
                return true;
            }
            if (attitudeTrigger.AccuralDirection == OperationType.Increase)
            {
                if (attitudeTrigger.CurrentAdditude <= _gameInfo.GetAttitudeByType(currencyType))
                {
                    return true;
                }
            }
            else if (attitudeTrigger.AccuralDirection == OperationType.Decrease)
            {
                if (attitudeTrigger.CurrentAdditude >= _gameInfo.GetAttitudeByType(currencyType))
                {
                    return true;
                }
            }

            return false;
        }

        private bool CheckTriggerByTriggerSignal(IReadOnlyCollection<string> triggersSignals)
        {
            int count = 0;
            foreach (string signal in triggersSignals)
            {
                if (_gameInfo.CheckTrigger(signal) || signal == "null")
                {
                    count++;
                }
            }
            if (count == triggersSignals.Count)
            {
                return true;
            }
            return false;
        }

        // Пытаемся затригерить события по отношению юзеров
        public bool TryTriggerByRelationship(Quest triggeredEvent)
        {
            if (CheckByAttitude(triggeredEvent.TriggerLoyalOver, CurrencyType.Loyal)
                && CheckByAttitude(triggeredEvent.TriggerLoyalUnder, CurrencyType.Loyal)
                && CheckByAttitude(triggeredEvent.TriggerRebelOver, CurrencyType.Rebel)
                && CheckByAttitude(triggeredEvent.TriggerRebelUnder, CurrencyType.Rebel))
            {
                return true;
            }
            return false;
        }

        public void CallTriggerQuests()
        {
            for (int i = 0; i < _questModel.QuestsQueue.Count; i++)
            {
                if (TryTriggerByRelationship(_questModel.QuestsQueue[i]) && CheckTriggerByDate(_questModel.QuestsQueue[i])
                    && CheckTriggerByTriggerSignal(_questModel.QuestsQueue[i].TriggerSignals))
                {
                    _questModel.TriggerQuest(_questModel.QuestsQueue[i]);
                }
            }
        }
    }
}