﻿using Assets.Scripts.GameStates;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.QuestMVC
{
    public class QuestView : MonoBehaviour
    {
        [SerializeField] private Transform _parent;
        [SerializeField] private GameObject _panel;
        [SerializeField] private GameObject _newQuestNotify;

        [Inject] private QuestInfoPanel _questInfoPanel;
        [Inject] private QuestItem _questItemPrefab;
        [Inject] private QuestModel _questModel;
        [Inject] private QuestController _questController;
        [Inject] private GameState _gameState;
        [Inject] private ButtonsMenu _buttonsMenu;

        private List<QuestItem> _questItems = new List<QuestItem>();

        public bool NotificationStatus => _newQuestNotify.activeSelf;


        private void OnEnable()
        {
            _questModel.OnQuestTriggered += AddQuest;
        }

        private void OnDisable()
        {
            _questModel.OnQuestTriggered -= AddQuest;
        }

        public void SetNotificationStatus(bool notificationStatus)
        {
            _newQuestNotify.SetActive(notificationStatus);
        }

        public void Open()
        {
            OffNotification();
            _gameState.ChangeGameState(GameStates.GameStates.Quests);
            _panel.SetActive(true);
        }

        public void Close()
        {
            _gameState.ChangeGameState(GameStates.GameStates.InternetMap);
            _questController.ViewedNewQuests();
            _panel.SetActive(false);
            _buttonsMenu.ResetButtonsWithOutOffPanels();
        }

        public void CloseWithOutNewQuests()
        {
            _gameState.ChangeGameState(GameStates.GameStates.InternetMap);
            _panel.SetActive(false);
            _buttonsMenu.ResetButtonsWithOutOffPanels();
        }

        private void QuestItemClickHandler(Quest quest)
        {
            _questInfoPanel.InitQuest(quest);
            _questInfoPanel.OpenPanel();
        }

        public void OnNotification()
        {
            if(!_panel.activeSelf)
                _newQuestNotify.SetActive(true);
        }

        public void OffNotification()
        {
            _newQuestNotify.SetActive(false);
        }

        public void AddQuest(Quest quest)
        {
            QuestItem questItem = Instantiate(_questItemPrefab, _parent);
            questItem.OnClick += QuestItemClickHandler;
            questItem.transform.SetSiblingIndex(0);
            questItem.InitQuest(quest);
            _questItems.Add(questItem);
            OnNotification();
        }
    }
}