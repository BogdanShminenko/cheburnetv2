﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Data.GameData.Configs;
using Assets.Scripts.Game;
using Assets.Scripts.MVC.EventMVC;
using Assets.Scripts.MVC.ServerMVC;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.TriggersAndOperationsTypes;
using Assets.Scripts.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.QuestMVC
{
    public class QuestController : MonoBehaviour
    {
        [Inject] private QuestTriggersService _questTriggers;
        [Inject] private QuestModel _questModel;
        [Inject] private QuestConfig _questConfig;
        [Inject] private QuestCompliteService _questCompliteService;

        public void InitQuests()
        {
            _questModel.InitQuests(_questConfig.Quests);
        }

        public void ViewedNewQuests()
        {
            foreach(var item in _questModel.TriggeredQuests)
            {
                if(item.Status == QuestStatus.New)
                {
                    item.ChangeQuestStatus(QuestStatus.Incomplete);
                }
            }
        }

        public void CallTriggerAndCompliteQuest()
        {
            _questTriggers.CallTriggerQuests();
            _questCompliteService.CallCompliteQuests();
        }
    }
}