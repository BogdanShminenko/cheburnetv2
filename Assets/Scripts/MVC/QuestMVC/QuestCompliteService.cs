﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Game;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.TriggersAndOperationsTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MVC.QuestMVC
{
    public class QuestCompliteService
    {
        [Inject] private QuestModel _questModel;
        [Inject] private TimerModel _timerModel;
        [Inject] private GameInfo _gameInfo;
        [Inject] private QuestView _questView;

        public bool CheckCompliteByDate(Quest quest)
        {
            if (quest.CheckBeforeDate == null)
                return true;

            if (new DateTime(quest.CheckBeforeDate.Year, quest.CheckBeforeDate.Month, 1) >= new DateTime((int)_timerModel.CurrentYear, (int)_timerModel.CurrentMonth, 1))
            {
                return true;
            }
            return false;
        }

        private bool CheckByAttitude(AttitudeTrigger attitudeTrigger, CurrencyType currencyType)
        {
            if (attitudeTrigger == null)
            {
                return true;
            }
            if (attitudeTrigger.AccuralDirection == OperationType.Increase)
            {
                if (attitudeTrigger.CurrentAdditude <= _gameInfo.GetAttitudeByType(currencyType))
                {
                    return true;
                }
            }
            else if (attitudeTrigger.AccuralDirection == OperationType.Decrease)
            {
                if (attitudeTrigger.CurrentAdditude >= _gameInfo.GetAttitudeByType(currencyType))
                {
                    return true;
                }
            }

            return false;
        }

        private bool CheckTCompliteByCheckSignal(IReadOnlyCollection<string> complite)
        {
            int count = 0;
            foreach (string signal in complite)
            {
                if (_gameInfo.CheckTrigger(signal) || signal == "null")
                {
                    count++;
                }
            }
            if (count == complite.Count)
            {
                return true;
            }
            return false;
        }

        public bool TryTriggerByRelationship(Quest quest)
        {
            if (CheckByAttitude(quest.CheckLoyalOver, CurrencyType.Loyal)
                && CheckByAttitude(quest.CheckLoyalUnder, CurrencyType.Loyal)
                && CheckByAttitude(quest.CheckRebelOver, CurrencyType.Rebel)
                && CheckByAttitude(quest.CheckRebelUnder, CurrencyType.Rebel)
                && CheckByAttitude(quest.CheckControlOver, CurrencyType.InternetControl))
            {
                return true;
            }
            return false;
        }

        public void CallCompliteQuests()
        {
            for (int i = 0; i < _questModel.TriggeredQuests.Count; i++)
            {
                if (_questModel.TriggeredQuests[i].Status != QuestStatus.Complete && TryTriggerByRelationship(_questModel.TriggeredQuests[i]) && CheckCompliteByDate(_questModel.TriggeredQuests[i])
                    && CheckTCompliteByCheckSignal(_questModel.TriggeredQuests[i].CheckSignals))
                {
                    _questModel.CompleteQuest(_questModel.TriggeredQuests[i]);
                    _questView.OnNotification();
                }
            }
        }
    }
}