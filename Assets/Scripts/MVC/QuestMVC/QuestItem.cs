﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.MVC.QuestMVC
{
    public class QuestItem : MonoBehaviour, IPointerClickHandler
    {
        [System.Serializable]
        private struct WwiseSound
        {
            public AK.Wwise.Event _clickEvent;
        }
        
        [SerializeField] private GameObject _questNewIcon;
        [SerializeField] private GameObject _questCompleteIcon;

        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _description;

        [SerializeField] private WwiseSound _wwiseSound;
        
        public Quest CurrentQuest { get; private set; }
        public event Action<Quest> OnClick;
        
        public void InitQuest(Quest quest)
        {
            CurrentQuest = quest;
            _title.text = quest.Title;
            _description.text = quest.Description;
            ChangeQuestStatusHandler();
            CurrentQuest.OnChangeStatus += ChangeQuestStatusHandler;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick?.Invoke(CurrentQuest);
            
            if (_wwiseSound._clickEvent.IsValid())
            {
                _wwiseSound._clickEvent.Post(gameObject);
            }
        }

        private void ChangeQuestStatusHandler()
        {
            if(CurrentQuest.Status == QuestStatus.Incomplete)
            {
                _questCompleteIcon.SetActive(false);
                _questNewIcon.SetActive(false);
            }else if(CurrentQuest.Status == QuestStatus.Complete)
            {
                _questCompleteIcon.SetActive(true);
                _questNewIcon.SetActive(false);
            }
            else if (CurrentQuest.Status == QuestStatus.New)
            {
                _questCompleteIcon.SetActive(false);
                _questNewIcon.SetActive(true);
            }
        }
    }
}