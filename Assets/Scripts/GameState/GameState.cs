﻿using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System;
using UnityEngine;

namespace Assets.Scripts.GameStates
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum GameStates
    {
        InternetMap,
        Events,
        Projects,
        Quests,
        Laws,
        Server,
        Pause,
        CutScene,
        EndGame,
        EventPanel
    }

    public class GameState : MonoBehaviour
    {
        [field: SerializeField] public GameStates CurrentGameState { get; private set; } = GameStates.InternetMap;
        public event Action OnGameStateChanged;
        public GameStates PreviousGameStates { get; private set; }

        public void ChangeGameState(GameStates newState)
        {
            PreviousGameStates = CurrentGameState;
            CurrentGameState = newState;
            OnGameStateChanged?.Invoke();
        }
    }
}