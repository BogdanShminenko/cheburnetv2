﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.InternetMapGeneration
{
    public class Cell
    {
        public Vector2 Position { get; private set; }

        public Cell(Vector2 position)
        {
            Position = position;
        }

        public void ChengePosition(Vector2 position)
        {
            Position = position;
        }
    }
}