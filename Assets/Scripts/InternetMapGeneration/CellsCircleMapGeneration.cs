﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.InternetObjects;
using Assets.Scripts.InternetObjects.Servers;
using Assets.Scripts.InternetObjects.User;
using Assets.Scripts.MVC.InternetMVC;
using Assets.Scripts.Utilities;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.InternetMapGeneration
{
    public sealed class CellsCircleMapGeneration : MonoBehaviour, IGeneration
    {
        [Inject] private InternetMapPositions _mapCenter;
        [Inject] private InternetMapSettingsData _internetMapSettingsData;
        [Inject] private InternetModel _internetModel;

        private const float RADIUS_DISTANCE_CELLS = 10f;

        private const int SERVER_X_SIZE = 3;
        private const int SERVER_Y_SIZE = 3;
        private const float X_STEP = 0.5f;
        private const float Y_STEP = 0.5f;
        private const float SERVER_DISTANCE = 5f;
        private const float USER_DISTANCE = 2f;

        private List<Cell> _cells;
        private Queue<Cell> _cellsQueue;

        public IEnumerable<Cell> Cells => _cells;
        public IEnumerable<Cell> CellsQueue => _cellsQueue;

        private void Awake()
        {
            GenerateCenterCellsMap();
        }

        public void SetCellsFromSave(IEnumerable<Cell> cells , IEnumerable<Cell> cellsQueue)
        {
            _cells = cells.ToList();
            _cellsQueue = new Queue<Cell>(cellsQueue);
        }

        public void DeleteFromQueueByVector(Vector2 vector2)
        {
            Cell cell = _cellsQueue.FirstOrDefault(item => item.Position == vector2);
            if(cell != null)
            {
                _cellsQueue = new Queue<Cell>(_cellsQueue.Where(item => item != cell).ToList());
            }
        }

        public void DeleteFromQueueByCell(Cell deletedCell)
        {
            _cellsQueue = new Queue<Cell>(_cellsQueue.Where(item => item != deletedCell).ToList());
        }

        public void GenerateCenterCellsMap()
        {
            _cells = new List<Cell>();


            float horizontalStartPositionCofficient = 10;
            float verticalStartPositionCofficient = 10;

            for (float x = _mapCenter.CenterPosition.x - (_internetMapSettingsData.MapInnerXSize * X_STEP) - horizontalStartPositionCofficient; x < _internetMapSettingsData.MapInnerXSize + _mapCenter.CenterPosition.x; x += X_STEP)
            {
                for(float y = _mapCenter.CenterPosition.y - (_internetMapSettingsData.MapInnerYSize * Y_STEP) - verticalStartPositionCofficient; y < _internetMapSettingsData.MapInnerYSize + _mapCenter.CenterPosition.y; y += Y_STEP)
                {
                    _cells.Add(new Cell(new Vector2(x, y)));
                }
            }
            _cells = ExcludeCellsExitFromRadius(_cells);
            List<Cell> shuffleCells = new List<Cell>(_cells);
            _cellsQueue = new Queue<Cell>(shuffleCells.Shuffle());
        }

        private bool CheckIsCanPlaced(Cell cell , out List<Cell> cells)
        {
            cells = new List<Cell>();
            List<Vector2> vector2s = new List<Vector2>();

            for(float x = 0; x < SERVER_X_SIZE * X_STEP; x += X_STEP)
            {
                for (float y = 0; y < SERVER_Y_SIZE * Y_STEP; y += Y_STEP)
                {
                    vector2s.Add(cell.Position + new Vector2(x, y));
                }
            }
            int countAdded = 0;
            foreach(var vector in vector2s)
            {
                Cell checkCell = _cellsQueue.FirstOrDefault(item => item.Position == vector);
                if (checkCell != null)
                {
                    countAdded++;
                    cells.Add(checkCell);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private List<Cell> ExcludeCellsExitFromRadius(List<Cell> cells)
        {
            var cellsSorted = new List<Cell>();

            foreach(var item in cells)
            {
                float distance = Vector2.Distance(item.Position, _mapCenter.CenterPosition);
                if(RADIUS_DISTANCE_CELLS > distance)
                {
                    cellsSorted.Add(item);
                }
            }

            return cellsSorted;

        }

        private bool CheckToCreateServerOnPosition(Vector2 position)
        {
            foreach (var item in _internetModel.Servers)
            {
                if (Vector3.Distance(new Vector3(position.x + X_STEP, position.y + Y_STEP, 0),
                    item.transform.position) <= SERVER_DISTANCE)
                    return false;
            }
            return true;
        }

        private bool CheckToCreateUserObjectOnPosition(Vector2 position)
        {
            foreach (var item in _internetModel.Servers)
            {
                if (Vector3.Distance(position, item.transform.position) <= USER_DISTANCE)
                    return false;
            }
            return true;
        }

        public void RepositionUserObject(UserObject userObject)
        {
            int count = 0;
            while (true)
            {
                Cell cell = _cellsQueue.Dequeue();

                if (CheckToCreateUserObjectOnPosition(cell.Position))
                {
                    userObject.transform.position = cell.Position;
                    userObject.Position = cell.Position;
                    break;
                }
                else
                {
                    _cellsQueue.Enqueue(cell);
                }

                count++;
                if (count >= 100)
                {
                    userObject.gameObject.SetActive(false);
                }
            }
        }

        public T Generate<T>(T internetObjectPrefab) where T : InternetObject
        {
            if (internetObjectPrefab is ServerObject)
            {
                if (GetCellsForServer(out List<Cell> cells))
                {
                    foreach (var item in cells)
                        DeleteFromQueueByCell(item);
                    Vector2 position = new Vector2(cells[0].Position.x + X_STEP, cells[0].Position.y + Y_STEP);
                    T internetObject = MonoBehaviour.Instantiate(internetObjectPrefab, position, Quaternion.identity);
                    internetObject.ObjectLocationType = ObjectLocationType.Inner;
                    internetObject.Position = position;
                    return internetObject;
                }
                else
                {
                    if(GetCellsForServerReducedRequirements(out List<Cell> cells1))
                    {
                        Cell cell1 = cells1[0];
                        foreach (var item in cells1)
                            DeleteFromQueueByCell(item);
                        foreach (var item in cells1)
                        {
                            if(_internetModel.TryGetUserByPosition(item.Position, out UserObject userObject))
                            {
                                RepositionUserObject(userObject);
                            }
                        }
                        T internetObject1 = MonoBehaviour.Instantiate(internetObjectPrefab, cell1.Position, Quaternion.identity);
                        internetObject1.ObjectLocationType = ObjectLocationType.Inner;
                        internetObject1.Position = cell1.Position;
                        return internetObject1;
                    }
                    Cell cell2 = _cellsQueue.Dequeue();
                    T internetObject2 = MonoBehaviour.Instantiate(internetObjectPrefab, cell2.Position, Quaternion.identity);
                    internetObject2.ObjectLocationType = ObjectLocationType.Inner;
                    internetObject2.Position = cell2.Position;
                    return internetObject2;
                }
            }
            else
            {
                int count = 0;
                while (true)
                {
                    Cell cell1 = _cellsQueue.Dequeue();

                    if (CheckToCreateUserObjectOnPosition(cell1.Position))
                    {
                        T internetObject1 = MonoBehaviour.Instantiate(internetObjectPrefab, cell1.Position, Quaternion.identity);
                        internetObject1.ObjectLocationType = ObjectLocationType.Inner;
                        internetObject1.Position = cell1.Position;
                        return internetObject1;
                    }
                    else
                    {
                        _cellsQueue.Enqueue(cell1);
                    }

                    count++;
                    if (count >= 100)
                    {
                        break;
                    }
                }
                Cell cell = _cellsQueue.Dequeue();
                T internetObject = MonoBehaviour.Instantiate(internetObjectPrefab, cell.Position, Quaternion.identity);
                internetObject.ObjectLocationType = ObjectLocationType.Inner;
                internetObject.Position = cell.Position;
                return internetObject;
            }
        }

        public T GenerateInPosition<T>(T internetObjectPrefab, Vector2 position) where T : InternetObject
        {
            if (internetObjectPrefab is ServerObject)
            {
                T internetObject = Instantiate(internetObjectPrefab, position, Quaternion.identity);
                Cell cell = _cellsQueue.FirstOrDefault(item => item.Position == position);
                if (cell != null)
                {
                    if (CheckIsCanPlaced(cell, out List<Cell> cells))
                    {
                        foreach (var item in cells)
                            DeleteFromQueueByCell(item);
                    }
                }
                internetObject.Position = position;
                return internetObject;
            }
            else
            {
                T internetObject = Instantiate(internetObjectPrefab, position, Quaternion.identity);
                internetObject.Position = position;
                DeleteFromQueueByVector(position);
                return internetObject;
            }
        }

        public List<InternetObject> Generate(InternetObject internetObjectPrefab , int count)
        {
            List<InternetObject> internetObjects = new List<InternetObject>();
            
            for(int i = 0; i < count; i++)
            {
                if (_cellsQueue.Count <= 0)
                    return internetObjects;

                if(internetObjectPrefab is ServerObject)
                {
                    Debug.Log("is Server");
                    if (GetCellsForServer(out List<Cell> cells))
                    {
                        foreach(var item in cells)
                            DeleteFromQueueByCell(item);
                        Vector2 position = new Vector2(cells[0].Position.x + X_STEP, cells[0].Position.y + Y_STEP);
                        InternetObject internetObject = MonoBehaviour.Instantiate(internetObjectPrefab, position, Quaternion.identity);
                        internetObject.ObjectLocationType = ObjectLocationType.Inner;
                        internetObject.Position = cells[0].Position;
                        internetObjects.Add(internetObject);
                    }
                    else
                    {
                        Debug.LogError("Over cells for server object");
                    }
                }
                else
                {
                    Cell cell = _cellsQueue.Dequeue();
                    InternetObject internetObject = MonoBehaviour.Instantiate(internetObjectPrefab, cell.Position, Quaternion.identity);
                    internetObject.ObjectLocationType = ObjectLocationType.Inner;
                    internetObject.Position = cell.Position;
                    Debug.Log("Generate in position " + cell.Position);
                    internetObjects.Add(internetObject);
                }
            }

            return internetObjects; 
        }

        private List<Cell> GetCellsFromUsers(Vector2 position)
        {
            List<Cell> cells = new List<Cell>();
            List<Vector2> vector2s = new List<Vector2>();

            for(float x = 0; x < SERVER_X_SIZE * X_STEP; x += X_STEP)
            {
                for (float y = 0; y < SERVER_Y_SIZE * Y_STEP; y += Y_STEP)
                {
                    vector2s.Add(position + new Vector2(x, y));
                }
            }
            int countAdded = 0;
            foreach (var vector in vector2s)
            {
                Cell checkCell = _cellsQueue.FirstOrDefault(item => item.Position == vector);
                if (checkCell != null)
                {
                    countAdded++;
                    cells.Add(checkCell);
                }
            }
            return cells;
        }

        private bool GetCellsForServerReducedRequirements(out List<Cell> cells)
        {
            cells = new List<Cell>();
            for (int i = 0; i < 1000; i++)
            {
                UserObject[] userObjects = _internetModel.UserObjects.ToArray();
                UserObject userObject = userObjects[Random.Range(0, userObjects.Length)];
                if (CheckToCreateServerOnPosition(userObject.Position))
                {
                    userObject.gameObject.SetActive(false);
                    cells.Add(new Cell(userObject.Position));
                    cells.AddRange(GetCellsFromUsers(userObject.Position));
                    return true;
                }
            }
            cells = new List<Cell>();
            return false;
        }

        private bool GetCellsForServer(out List<Cell> cells)
        {         
            for(int i = 0; i < 200; i++)
            {
                Cell cell = _cellsQueue.Dequeue();
                _cellsQueue.Enqueue(cell);
                if (CheckIsCanPlaced(cell, out cells) && CheckToCreateServerOnPosition(cell.Position))
                {
                    return true;
                }
                else
                {
                    _cellsQueue.Enqueue(cell);
                }
            }
            cells = new List<Cell>();
            return false;
        }
    }
}