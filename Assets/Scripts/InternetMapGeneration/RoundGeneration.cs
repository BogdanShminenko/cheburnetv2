﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.InternetObjects;
using Assets.Scripts.InternetObjects.Node;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Assets.Scripts.InternetMapGeneration
{
    public class RoundGeneration : MonoBehaviour, IGeneration
    {
        [SerializeField] private float _angleMin;
        [SerializeField] private float _angleMax;
        private const float RADIUS_SCALE = 2.6f;
        [Inject] private InternetMapSettingsData _internetMapSettingsData;
        [Inject] private InternetMapPositions _internetMapCenter;

        public List<InternetObject> Generate(InternetObject internetObjectPrefab, int count)
        {
            List<InternetObject> nodes = new List<InternetObject>();
            float radius = _internetMapSettingsData.Radius / RADIUS_SCALE;
            for (int i = 0; i < count; i ++)
            {
                float angle = Random.Range(_angleMin, _angleMax);
                float xPosition = _internetMapCenter.CenterPosition.x + radius * Mathf.Cos(angle);
                float yPosition = _internetMapCenter.CenterPosition.y + radius * Mathf.Sin(angle);

                InternetObject node = Instantiate(internetObjectPrefab, new Vector3(xPosition, yPosition,0) , Quaternion.identity);
                node.Position = new Vector2(xPosition, yPosition);

                nodes.Add(node);
            }
            return nodes;
        }


        public T GenerateInPosition<T>(T internetObjectPrefab, Vector2 position) where T : InternetObject
        {
            T internetObject = Instantiate(internetObjectPrefab, position, Quaternion.identity);
            internetObject.Position = position;
            return internetObject;
        }

    }
}