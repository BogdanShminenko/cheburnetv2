﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.InternetObjects;
using Assets.Scripts.InternetObjects.Servers;
using Assets.Scripts.InternetObjects.User;
using Assets.Scripts.MVC.InternetMVC;
using Assets.Scripts.Utilities;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.InternetMapGeneration
{
    public sealed class CellSquareMapGeneration : MonoBehaviour, IGenerationOffset
    {
        [SerializeField] private LayerMask _blokedPositionLayer;
        [Inject] private InternetMapSettingsData _internetMapSettingsData;
        [Inject] private InternetModel _internetModel;
        [Inject] private InternetMapPositions _internetMapPositions;

        private List<Cell> _cells;
        private Queue<Cell> _cellsQueue;

        private int _serverXSize = 3;
        private int _serverYSize = 3;
        private float _xStep = 0.5f;
        private float _yStep = 0.5f;
        private float _serverDistance = 5f;
        private float _lowServerDistance = 3f;
        private float _scale = 1f;

        public IEnumerable<Cell> Cells => _cells;
        public IEnumerable<Cell> CellsQueue => _cellsQueue;


        public bool TryGetCellFromQueue(out Cell cell)
        {
            cell = _cellsQueue.Dequeue();
            if (cell != null)
                return true;
            return false;
        }

        public void SetCellsFromSave(IEnumerable<Cell> cells, IEnumerable<Cell> cellsQueue)
        {
            _cells = cells.ToList();
            _cellsQueue = new Queue<Cell>(cellsQueue);
        }


        private void Awake()
        {
            RecalculateSizes();
            GenerateCenterCellsMap();
        }

        private void RecalculateSizes()
        {
            _internetMapPositions.Up.position -= new Vector3(-2, 2.5f, 0);
            _internetMapPositions.Down.position += new Vector3(2, 2.5f, 0);
            _xStep = 0.2f;
            _yStep = 0.2f;
            _scale = 1f;
            _internetMapSettingsData.MapOuterXSize = 20;
            _internetMapSettingsData.MapOuterYSize = 4;
        }

        public void DeleteFromQueueByVector(Vector2 vector2)
        {
            Cell cell = _cellsQueue.FirstOrDefault(item => item.Position == vector2);
            if (cell != null)
            {
                _cellsQueue = new Queue<Cell>(_cellsQueue.Where(item => item != cell).ToList());
            }
        }

        public void DeleteFromQueueByCell(Cell deletedCell)
        {
            _cellsQueue = new Queue<Cell>(_cellsQueue.Where(item => item != deletedCell).ToList());
        }

        private bool CheckToCollisionInBlockedPoint(Vector3 point)
        {
            Collider2D collider2D = Physics2D.OverlapBox(point, new Vector2(3, 3), 0 , _blokedPositionLayer);
            if(collider2D == null)
                return true;
            return false;

        }

        public T GenerateInPosition<T>(T internetObjectPrefab, Vector2 position, Vector2 offset) where T : InternetObject
        {
            if (internetObjectPrefab is ServerObject)
            {
                T internetObject = Instantiate(internetObjectPrefab, position, Quaternion.identity);
                internetObject.transform.localScale *= _scale;
                Cell cell = _cellsQueue.FirstOrDefault(item => item.Position == position - offset);
                if (cell != null)
                {
                    if(CheckIsCanPlaced(cell ,out List<Cell> cells))
                    {
                        foreach (var item in cells)
                            DeleteFromQueueByCell(item);
                    }
                }
                internetObject.Position = position;
                internetObject.Offset = offset;
                return internetObject;
            }
            else
            {
                T internetObject = Instantiate(internetObjectPrefab, position + offset, Quaternion.identity);
                internetObject.transform.localScale *= _scale;
                DeleteFromQueueByVector(position);
                internetObject.Position = position;
                internetObject.Offset = offset;
                return internetObject;
            }

        }

        public void GenerateCenterCellsMap()
        {
            _cells = new List<Cell>();

            for (float x = 0; x < _internetMapSettingsData.MapOuterXSize; x += _xStep)
            {
                for (float y = 0; y < _internetMapSettingsData.MapOuterYSize; y += _yStep)
                {
                    _cells.Add(new Cell(new Vector2(x, y)));
                }
            }
            List<Cell> shuffleCells = new List<Cell>(_cells);
            _cellsQueue = new Queue<Cell>(shuffleCells.Shuffle());
        }

        private bool CheckToCreateServerOnPosition(Vector2 position, Vector3 positionOffset)
        {
            foreach (var item in _internetModel.Servers)
            {
                if (Vector3.Distance(new Vector3(positionOffset.x + position.x + _xStep, positionOffset.y + position.y + _yStep, 0),
                    item.transform.position) <= _serverDistance)
                    return false;
            }
            return true;
        }

        private bool CheckToCreateServerOnPositionWithLowDistance(Vector2 position, Vector3 positionOffset)
        {
            foreach (var item in _internetModel.Servers)
            {
                if (Vector3.Distance(new Vector3(positionOffset.x + position.x + _xStep, positionOffset.y + position.y + _yStep, 0),
                    item.transform.position) <= _lowServerDistance)
                    return false;
            }
            return true;
        }


        public T Generate<T>(T internetObjectPrefab, Vector3 positionOffset) where T : InternetObject
        {
            T internetObject = null;
            if (internetObjectPrefab is ServerObject)
            {
                int count = 0;
                while (true)
                {
                    if (GetCellsForServer(out List<Cell> cells, positionOffset))
                    {
                        Vector2 position = new Vector2(positionOffset.x + cells[0].Position.x + _xStep, positionOffset.y + cells[0].Position.y + _yStep);

                        if (CheckToCollisionInBlockedPoint(position))
                        {
                            foreach (var item in cells)
                                DeleteFromQueueByCell(item);
                            internetObject = MonoBehaviour.Instantiate(internetObjectPrefab, position, Quaternion.identity);
                            internetObject.transform.localScale *= _scale;
                            internetObject.Position = cells[0].Position;
                            internetObject.Offset = positionOffset;
                            break;
                        }
                        else
                        {
                            _cellsQueue.Enqueue(cells[0]);
                        }
                    }
                    else
                    {
                        if(GetCellsForServerReducedRequirements(out List<Cell> cells1, positionOffset))
                        {
                            Vector2 position = new Vector2(positionOffset.x + cells1[0].Position.x + _xStep, positionOffset.y + cells1[0].Position.y + _yStep);

                            if (CheckToCollisionInBlockedPoint(position))
                            {
                                foreach (var item in cells1)
                                    DeleteFromQueueByCell(item);
                                internetObject = MonoBehaviour.Instantiate(internetObjectPrefab, position, Quaternion.identity);
                                internetObject.transform.localScale *= _scale;
                                internetObject.Position = cells1[0].Position;
                                internetObject.Offset = positionOffset;
                                break;
                            }
                        }
                    }
                    count++;
                    if (count >= 100)
                        break;
                }
            }
            else
            {
                Cell cell = _cellsQueue.Dequeue();
                internetObject = Instantiate(internetObjectPrefab, new Vector3(cell.Position.x + positionOffset.x,
                                                                                                cell.Position.y + positionOffset.y, 0), Quaternion.identity);
                internetObject.transform.localScale *= _scale;
                internetObject.Position = cell.Position;
                internetObject.Offset = positionOffset;
            }
            return internetObject;
        }

        public List<T> Generate<T>(T internetObjectPrefab, int count , Vector3 positionOffset, ObjectLocationType objectLocationType) where T : InternetObject
        {
            List<T> internetObjects = new List<T>();

            for (int i = 0; i < count; i++)
            {
                if (_cellsQueue.Count <= 0)
                {
                    Debug.Log("Cells queue exit");
                    return internetObjects;
                } 
                if (internetObjectPrefab is ServerObject)
                {
                    Debug.Log("is Server");
                    if (GetCellsForServer(out List<Cell> cells, positionOffset))
                    {
                        foreach (var item in cells)
                            DeleteFromQueueByCell(item);
                        Vector2 position = new Vector2(positionOffset.x + cells[0].Position.x + _xStep, positionOffset.y + cells[0].Position.y + _yStep);
                        T internetObject = MonoBehaviour.Instantiate(internetObjectPrefab, position, Quaternion.identity);
                        internetObject.transform.localScale *= _scale;
                        internetObject.Position = cells[0].Position;
                        internetObject.Offset = positionOffset;
                        internetObject.ObjectLocationType = objectLocationType;
                        internetObjects.Add(internetObject);
                    }
                    else
                    {
                        Debug.LogError("Over cells for server object");
                    }
                }
                else
                {
                    Cell cell = _cellsQueue.Dequeue();
                    T internetObject = MonoBehaviour.Instantiate(internetObjectPrefab, new Vector3(cell.Position.x + positionOffset.x,
                                                                                                    cell.Position.y + positionOffset.y, 0), Quaternion.identity);
                    internetObject.transform.localScale *= _scale;
                    internetObject.Position = cell.Position;
                    internetObject.Offset = positionOffset;
                    internetObject.ObjectLocationType = objectLocationType;
                    internetObjects.Add(internetObject);
                }
            }

            return internetObjects;
        }

        private List<Cell> GetCellsFromUsers(Vector2 position)
        {
            List<Cell> cells = new List<Cell>();
            List<Vector2> vector2s = new List<Vector2>();

            for (float x = 0; x < _serverXSize * _xStep; x += _xStep)
            {
                for (float y = 0; y < _serverYSize * _yStep; y += _yStep)
                {
                    vector2s.Add(position + new Vector2(x, y));
                }
            }
            int countAdded = 0;
            foreach (var vector in vector2s)
            {
                Cell checkCell = _cellsQueue.FirstOrDefault(item => item.Position == vector);
                if (checkCell != null)
                {
                    countAdded++;
                    cells.Add(checkCell);
                }
            }
            return cells;
        }

        private bool CheckIsCanPlaced(Cell cell, out List<Cell> cells)
        {
            cells = new List<Cell>();
            List<Vector2> vector2s = new List<Vector2>();

            for (float x = 0; x < _serverXSize * _xStep; x += _xStep)
            {
                for (float y = 0; y < _serverYSize * _yStep; y += _yStep)
                {
                    vector2s.Add(cell.Position + new Vector2(x, y));
                }
            }
            int countAdded = 0;
            foreach (var vector in vector2s)
            {
                Cell checkCell = _cellsQueue.FirstOrDefault(item => item.Position == vector);
                if (checkCell != null)
                {
                    countAdded++;
                    cells.Add(checkCell);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private bool GetCellsForServerReducedRequirements(out List<Cell> cells, Vector3 positionOffset)
        {
            cells = new List<Cell>();
            for (int i = 0; i < 1000; i++)
            {
                UserObject[] userObjects = _internetModel.UserObjects.ToArray();
                UserObject userObject = userObjects[Random.Range(0, userObjects.Length)];
                if (CheckToCreateServerOnPositionWithLowDistance(userObject.Position, positionOffset))
                {
                    userObject.gameObject.SetActive(false);
                    cells.Add(new Cell(userObject.Position));
                    cells.AddRange(GetCellsFromUsers(userObject.Position));
                    return true;
                }
            }
            cells = new List<Cell>();
            return false;
        }

        private bool GetCellsForServer(out List<Cell> cells , Vector3 positionOffset)
        {
            for (int i = 0; i < 1000; i++)
            {
                Cell cell = _cellsQueue.Dequeue();
                _cellsQueue.Enqueue(cell);
                if (CheckIsCanPlaced(cell, out cells) && CheckToCreateServerOnPosition(cell.Position, positionOffset))
                {
                    return true;
                }
                else
                {
                    _cellsQueue.Enqueue(cell);
                }
            }
            cells = new List<Cell>();
            return false;
        }
    }
}