﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.InternetMapGeneration
{
    public sealed class InternetMapPositions : MonoBehaviour
    {
        [SerializeField] private Transform _center;
        [SerializeField] private Transform _up;
        [SerializeField] private Transform _down;


        public Transform Up => _up;
        public Transform Down => _down;

        public Vector3 CenterPosition => _center.position;
        public Vector3 UpPosition => _up.position;
        public Vector3 DownPosition => _down.position;

    }
}