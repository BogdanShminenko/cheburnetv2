﻿using Assets.Scripts.InternetObjects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.InternetMapGeneration
{
    public interface IGeneration
    {
        public List<InternetObject> Generate(InternetObject internetObjectPrefab, int count);
        
    }

    public interface IGenerationOffset
    {
        public List<T> Generate<T>(T internetObjectPrefab, int count, Vector3 positionOffset, ObjectLocationType objectLocationType) where T : InternetObject;
    }
}