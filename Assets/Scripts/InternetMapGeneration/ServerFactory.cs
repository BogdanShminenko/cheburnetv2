﻿using Assets.Scripts.InternetObjects.Servers;
using Assets.Scripts.MVC.ServerMVC;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.InternetMapGeneration
{
    public class ServerFactory : IFactory<Vector3, ServerObject>
    {
        [Inject] private CellSquareMapGeneration _cellSquareMapGeneration;
        private DiContainer _container;

        public ServerFactory(DiContainer container)
        {
            _container = container;
        }
       
        public ServerObject Create(Vector3 positionOffset)
        {
            ServerObject serverObject = null;
            if (_cellSquareMapGeneration.TryGetCellFromQueue(out Cell cell))
            {
                serverObject = _container.Instantiate<ServerObject>();
                serverObject.transform.position = new Vector3(cell.Position.x + positionOffset.x, cell.Position.y + positionOffset.y, 0);
            }
            return serverObject;
        }
    }
}