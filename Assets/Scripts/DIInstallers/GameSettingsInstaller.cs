﻿using Assets.Scripts.Game;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers
{
    public class GameSettingsInstaller : MonoInstaller
    {
        [SerializeField] private GameSettings _gameSettings;

        public override void InstallBindings()
        {
            Container.Bind<GameSettings>().FromInstance(_gameSettings).AsSingle().NonLazy();
        }
    }
}