﻿using Assets.Scripts.Data.GameData.Configs;
using Assets.Scripts.MVC.LawsMVC;
using Assets.Scripts.MVC.LawsMVC.LawViews;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCLawInstaller : MonoInstaller
    {
        [SerializeField] private LawActionButtonsState _lawActionButtonsState;
        [SerializeField] private LawModel _lawModel;
        [SerializeField] private LawView _lawView;
        [SerializeField] private LawController _lawController;
        [SerializeField] private LawPanel _lawPanel;
        [SerializeField] private AcceptAndBribeState _acceptAndBribeState;
        [SerializeField] private AcceptAndPushState _acceptAndPushState;
        [SerializeField] private AcceptedState _acceptedState;
        [SerializeField] private UnsuccessfulState _unsuccessfulState;
        [SerializeField] private LawUIItem _lawUIItem;

        public override void InstallBindings()
        {
            Container.Bind<LawActionButtonsState>().FromInstance(_lawActionButtonsState).AsSingle();
            Container.Bind<LawModel>().FromInstance(_lawModel).AsSingle();
            Container.Bind<LawView>().FromInstance(_lawView).AsSingle();
            Container.Bind<LawController>().FromInstance(_lawController).AsSingle();
            Container.Bind<LawPanel>().FromInstance(_lawPanel).AsSingle();
            Container.Bind<AcceptAndBribeState>().FromInstance(_acceptAndBribeState).AsSingle();
            Container.Bind<AcceptAndPushState>().FromInstance(_acceptAndPushState).AsSingle();
            Container.Bind<AcceptedState>().FromInstance(_acceptedState).AsSingle();    
            Container.Bind<UnsuccessfulState>().FromInstance(_unsuccessfulState).AsSingle();
            Container.Bind<LawUIItem>().FromInstance(_lawUIItem).AsSingle();
        }

    }
}