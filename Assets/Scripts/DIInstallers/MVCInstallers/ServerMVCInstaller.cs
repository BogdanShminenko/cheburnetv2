﻿using Assets.Scripts.MVC.ServerMVC;
using Assets.Scripts.MVC.ServerMVC.ServerViews;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class ServerMVCInstaller : MonoInstaller
    {
        [SerializeField] private ServerActionButton _serverActionButton;
        [SerializeField] private ServerView _serverView;
        [SerializeField] private ServerModel _serverModel;
        [SerializeField] private ServerController _serverController;
        [SerializeField] private ServerTrigger _serverTrigger;
        [SerializeField] private ServerActionTrigger _serverActionTrigger;
        [SerializeField] private ServerActionBaseView _serverActionBaseView;
        [SerializeField] private ServerActionBlockView _serverActionBlockView;
        [SerializeField] private ServerActionBuyView _serverActionBuyView;
        [SerializeField] private ServerActionCaptureView _serverActionCaptureView;

        public override void InstallBindings()
        {
            Container.Bind<ServerActionButton>().FromInstance(_serverActionButton).AsSingle();
            Container.Bind<ServerView>().FromInstance(_serverView).AsSingle();
            Container.Bind<ServerModel>().FromInstance(_serverModel).AsSingle();
            Container.Bind<ServerController>().FromInstance(_serverController).AsSingle();
            Container.Bind<ServerTrigger>().FromInstance(_serverTrigger).AsSingle();
            Container.Bind<ServerActionTrigger>().FromInstance(_serverActionTrigger).AsSingle();
            Container.Bind<ServerActionBaseView>().FromInstance(_serverActionBaseView).AsSingle();
            Container.Bind<ServerActionBlockView>().FromInstance(_serverActionBlockView).AsSingle();
            Container.Bind<ServerActionBuyView>().FromInstance(_serverActionBuyView).AsSingle();
            Container.Bind<ServerActionCaptureView>().FromInstance(_serverActionCaptureView).AsSingle();
        }

    }
}