﻿using Assets.Scripts.MVC.ProjectMVC.Views;
using Assets.Scripts.MVC.ProjectMVC;
using System.Collections;
using UnityEngine;
using Zenject;
using Assets.Scripts.MVC.ProjectMVC.Views.InfoStatePanels;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCProjectInstaller : MonoInstaller
    {
        [SerializeField] private ProjectLine _projectLinePrefab;
        [SerializeField] private ProjectsItemsLine _projectsItemsLinePrefab;
        [SerializeField] private ProjectModel _projectModel;
        [SerializeField] private ProjectTreeItem _projectTreeItemPrefab;
        [SerializeField] private ProjectController _projectController;
        [SerializeField] private ProjectView _projectViewPrefab;
        [SerializeField] private ProjectInfoPanel _projectInfoPanel;
        [SerializeField] private DevelopProcess _developProcessPrefab;
        [SerializeField] private ResearchedState _researchedState;
        [SerializeField] private AvailableDevelopAndBuyState _availableDevelopAndBuyState;
        [SerializeField] private AvailableDevelopState _availableDevelopState;
        [SerializeField] private DevelopmentProcessState _developmentProcessState;
        [SerializeField] private NotAvailableResearchState _notAvailableResearchState;
        [SerializeField] private ProjectImplementedState _projectImplementedState;

        public override void InstallBindings()
        {
            Container.Bind<ProjectLine>().FromInstance(_projectLinePrefab).AsSingle();
            Container.Bind<ProjectsItemsLine>().FromInstance(_projectsItemsLinePrefab).AsSingle();
            Container.Bind<ProjectModel>().FromInstance(_projectModel).AsSingle();
            Container.Bind<ProjectTreeItem>().FromInstance(_projectTreeItemPrefab).AsSingle();
            Container.Bind<ProjectController>().FromInstance(_projectController).AsSingle();
            Container.Bind<ProjectView>().FromInstance(_projectViewPrefab).AsSingle();
            Container.Bind<ProjectInfoPanel>().FromInstance(_projectInfoPanel).AsSingle();
            Container.Bind<DevelopProcess>().FromInstance(_developProcessPrefab).AsSingle();
            Container.Bind<ResearchedState>().FromInstance(_researchedState).AsSingle();
            Container.Bind<AvailableDevelopAndBuyState>().FromInstance(_availableDevelopAndBuyState).AsSingle();
            Container.Bind<AvailableDevelopState>().FromInstance(_availableDevelopState).AsSingle();
            Container.Bind<DevelopmentProcessState>().FromInstance(_developmentProcessState).AsSingle();
            Container.Bind<NotAvailableResearchState>().FromInstance(_notAvailableResearchState).AsSingle();
            Container.Bind<ProjectImplementedState>().FromInstance(_projectImplementedState).AsSingle();
        }
    }
}