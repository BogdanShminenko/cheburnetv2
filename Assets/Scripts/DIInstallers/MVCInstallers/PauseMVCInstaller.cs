﻿using Assets.Scripts.MVC.PauseMenuMVC;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class PauseMVCInstaller : MonoInstaller
    {
        [SerializeField] private PauseMenuModel _pauseMenuModel;
        [SerializeField] private PauseMenuController _pauseMenuController;
        [SerializeField] private PauseMenuView _pauseMenuView;

        public override void InstallBindings()
        {
            Container.Bind<PauseMenuModel>().FromInstance(_pauseMenuModel).AsSingle();
            Container.Bind<PauseMenuController>().FromInstance(_pauseMenuController).AsSingle();
            Container.Bind<PauseMenuView>().FromInstance(_pauseMenuView).AsSingle();
        }
    }
}