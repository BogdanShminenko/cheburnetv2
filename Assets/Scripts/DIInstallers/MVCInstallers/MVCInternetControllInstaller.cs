﻿using Assets.Scripts.MVC.InternetControllMVC;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCInternetControllInstaller : MonoInstaller
    {
        [SerializeField] private InternetControllController _internetControllController;
        [SerializeField] private InternetControllView _internetControllView;

        public override void InstallBindings()
        {
            Container.Bind<InternetControllController>().FromInstance(_internetControllController).AsSingle();
            Container.Bind<InternetControllView>().FromInstance(_internetControllView).AsSingle();  
            Container.Bind<InternetControllModel>().FromNew().AsSingle();
        }
    }
}