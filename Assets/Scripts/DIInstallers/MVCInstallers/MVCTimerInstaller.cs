﻿using Assets.Scripts.MVC.TimerMVC;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public sealed class MVCTimerInstaller : MonoInstaller
    {
        [SerializeField] private TimerController _timerController;
        [SerializeField] private TimerModel _timerModel;
        [SerializeField] private TimerView _timerView;
        [SerializeField] private TimerProgresssBar _progressBar;

        public override void InstallBindings()
        {
            Container.Bind<TimerController>().FromInstance(_timerController).AsSingle();
            Container.Bind<TimerModel>().FromInstance(_timerModel).AsSingle();
            Container.Bind<TimerView>().FromInstance(_timerView).AsSingle();
            Container.Bind<TimerProgresssBar>().FromInstance(_progressBar).AsSingle();
        }
    }
}