﻿using Assets.Scripts.MVC.UsersAttitude;
using Zenject;
using UnityEngine;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCAttitudeInstaller : MonoInstaller
    {
        [SerializeField] private UsersAttitudeModel _attitudeModel;
        [SerializeField] private UsersAttitudeController _attitudeController;
        [SerializeField] private UsersAttitudeView _attitudeView;
        [SerializeField] private UsersAttitudeBar _attitudeBar;

        public override void InstallBindings()
        {
            Container.Bind<UsersAttitudeController>().FromInstance(_attitudeController).AsSingle();
            Container.Bind<UsersAttitudeModel>().FromInstance(_attitudeModel).AsSingle();
            Container.Bind<UsersAttitudeView>().FromInstance(_attitudeView).AsSingle();
            Container.Bind<UsersAttitudeBar>().FromInstance(_attitudeBar).AsSingle();
        }

    }
}