﻿using Assets.Scripts.MVC.QuestMVC;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCQuestInstaller : MonoInstaller
    {
        [SerializeField] private QuestInfoPanel _questInfoPanel;
        [SerializeField] private QuestItem _questItemPrefab;
        [SerializeField] private QuestModel _questModel;
        [SerializeField] private QuestController _questController;
        [SerializeField] private QuestView _questView;

        public override void InstallBindings()
        {
            Container.Bind<QuestInfoPanel>().FromInstance(_questInfoPanel).AsSingle();
            Container.Bind<QuestController>().FromInstance(_questController).AsSingle();
            Container.Bind<QuestModel>().FromInstance(_questModel).AsSingle();
            Container.Bind<QuestItem>().FromInstance(_questItemPrefab).AsSingle();
            Container.Bind<QuestView>().FromInstance(_questView).AsSingle();
            Container.Bind<QuestCompliteService>().FromNew().AsSingle();
            Container.Bind<QuestTriggersService>().FromNew().AsSingle();
        }
    }
}