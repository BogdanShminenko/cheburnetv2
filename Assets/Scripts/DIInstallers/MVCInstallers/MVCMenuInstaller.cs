﻿using Assets.Scripts.MVC.MenuMVC;
using Assets.Scripts.MVC.MenuMVC.View;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCMenuInstaller : MonoInstaller
    {
        [SerializeField] private PrivacyPolicyView _privacyPolicyView;
        [SerializeField] private MenuView _menuView;

        public override void InstallBindings()
        {
            Container.Bind<MenuView>().FromInstance(_menuView).AsSingle();
            Container.Bind<PrivacyPolicyView>().FromInstance(_privacyPolicyView).AsSingle();
        }
    }
}