﻿using Assets.Scripts.MVC.CutsceneMVC;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCCutSceneInstaller : MonoInstaller
    {
        [SerializeField] private CutSceneController _cutSceneController;
        [SerializeField] private CutSceneModel _cutSceneModel;
        [SerializeField] private CutSceneView _cutSceneView;

        public override void InstallBindings()
        {
            Container.Bind<CutSceneController>().FromInstance(_cutSceneController).AsSingle();
            Container.Bind<CutSceneModel>().FromInstance(_cutSceneModel).AsSingle();
            Container.Bind<CutSceneView>().FromInstance(_cutSceneView).AsSingle();
        }
    }
}