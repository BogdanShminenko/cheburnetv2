﻿using Assets.Scripts.MVC.EventMVC;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCEventInstaller : MonoInstaller
    {
        [SerializeField] private EventController _eventController;
        [SerializeField] private EventModel _eventModel;
        [SerializeField] private EventView _eventView;
        [SerializeField] private EventPanel _eventPanel;
        [SerializeField] private EventsPanel _eventsPanel;

        public override void InstallBindings()
        {
            Container.Bind<EventController>().FromInstance(_eventController).AsSingle();
            Container.Bind<EventModel>().FromInstance(_eventModel).AsSingle();
            Container.Bind<EventView>().FromInstance(_eventView).AsSingle();
            Container.Bind<EventPanel>().FromInstance(_eventPanel).AsSingle();
            Container.Bind<EventsPanel>().FromInstance(_eventsPanel).AsSingle();
        }
    }
}