using Zenject;
using UnityEngine;
using Assets.Scripts.MVC.CurrencyMVC;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCCurrencyInstaller : MonoInstaller
    {
        [SerializeField] private WillModel _willmodel;
        [SerializeField] private MoneyModel _moneyModel;
        [SerializeField] private CurrencyController _currencyController;
        [SerializeField] private WillView _willView;
        [SerializeField] private MoneyView _moneyView;

        public override void InstallBindings()
        {
            Container.Bind<WillModel>().FromInstance(_willmodel).AsSingle();
            Container.Bind<CurrencyController>().FromInstance(_currencyController).AsSingle();
            Container.Bind<MoneyModel>().FromInstance(_moneyModel).AsSingle();
            Container.Bind<WillView>().FromInstance(_willView).AsSingle();
            Container.Bind<MoneyView>().FromInstance(_moneyView).AsSingle();
        }
    }
}
