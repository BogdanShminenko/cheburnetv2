﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.InternetMapGeneration;
using Assets.Scripts.InternetObjects.Node;
using Assets.Scripts.InternetObjects.Servers;
using Assets.Scripts.InternetObjects.User;
using Assets.Scripts.MVC.InternetMVC;
using Assets.Scripts.MVC.ServerMVC;
using Assets.Scripts.Utilities;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCInternetInstaller : MonoInstaller
    {
        [SerializeField] private CellSquareMapGeneration _cellSquareMapGeneration;
        [SerializeField] private InternetMapPositions _mapCenter;
        [SerializeField] private InternetController _internetController;
        [SerializeField] private InternetModel _internetModel;
        [SerializeField] private InternetView _internetView;
        [SerializeField] private CellsCircleMapGeneration _cellsGeneration;
        [SerializeField] private RoundGeneration _roundGeneration;
        [SerializeField] private ServerObject _serverPrefab;
        [SerializeField] private UserObject _userPrefab;
        [SerializeField] private NodeObject _nodePrefab;
        [SerializeField] private ServerFactory _serverFactory;
        [SerializeField] private OuterServerLineService _outerServerLineService;

        public override void InstallBindings()
        {
            Container.Bind<OuterServerLineService>().FromInstance(_outerServerLineService).AsSingle();
            Container.Bind<CellSquareMapGeneration>().FromInstance(_cellSquareMapGeneration).AsSingle();
            Container.Bind<InternetController>().FromInstance(_internetController).AsSingle();
            Container.Bind<InternetModel>().FromInstance(_internetModel).AsSingle();
            Container.Bind<InternetView>().FromInstance(_internetView).AsSingle();
            Container.Bind<CellsCircleMapGeneration>().FromInstance(_cellsGeneration).AsSingle();
            Container.Bind<RoundGeneration>().FromInstance(_roundGeneration).AsSingle();
            Container.Bind<ServerObject>().FromInstance(_serverPrefab).AsSingle();
            Container.Bind<InternetMapPositions>().FromInstance(_mapCenter).AsSingle();
            Container.Bind<UserObject>().FromInstance(_userPrefab).AsSingle();
            Container.Bind<NodeObject>().FromInstance(_nodePrefab).AsSingle();
            Container.Bind<CalculateUsersGrow>().FromNew().AsSingle();
            //Container.BindFactory<ServerObject, ServerFactoryPlaceHolder>().FromFactory<ServerFactory>();
        }

    }

}