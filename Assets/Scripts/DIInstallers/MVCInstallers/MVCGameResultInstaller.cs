﻿using Assets.Scripts.MVC.GameResultMVC;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCGameResultInstaller : MonoInstaller
    {
        [SerializeField] private GameResultController _gameResultController;
        [SerializeField] private GameResultModel _gameResultModel;
        [SerializeField] private GameResultView _gameResultView;

        public override void InstallBindings()
        {
            Container.Bind<GameResultController>().FromInstance(_gameResultController).AsSingle();
            Container.Bind<GameResultModel>().FromInstance(_gameResultModel).AsSingle();
            Container.Bind<GameResultView>().FromInstance(_gameResultView).AsSingle();
        }
    }
}