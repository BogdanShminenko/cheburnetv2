﻿using Assets.Scripts.MVC.SaveSystemMVC;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.MVCInstallers
{
    public class MVCSaveSystemInstaller : MonoInstaller
    {
        [SerializeField] private SaveSystemController _saveSystemController;
        [SerializeField] private SaveSystemModel _saveSystemModel;
        [SerializeField] private SaveSystemView _saveSystemView;

        public override void InstallBindings()
        {
            Container.Bind<SaveSystemController>().FromInstance(_saveSystemController).AsSingle();
            Container.Bind<SaveSystemModel>().FromNew().AsSingle();
            Container.Bind<SaveSystemView>().FromInstance(_saveSystemView).AsSingle();
        }

    }
}