﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.Data.GameData.Configs;
using Assets.Scripts.Data.GameData.FileLoaders;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.DataInstallers
{
    public sealed class GameDataInstaller : MonoInstaller
    {
        [SerializeField] private ConfigFileReader _configFileReader;
        [SerializeField] private GameSettingsData _gameSettingsData;
        [SerializeField] private EventConfig _eventConfig;
        [SerializeField] private LawConfig _lawConfig;
        [SerializeField] private ProjectConfig _projectConfig;
        [SerializeField] private QuestConfig _questConfig;
        [SerializeField] private ServerConfig _serverConfig;
        [SerializeField] private ServerActionCaseDatasConfig _serverActionCaseDatasConfig;
        [SerializeField] private ServerActionDataConfig _serverActionDataConfig;
        [SerializeField] private GameCyclesConfig _gameCyclesConfig;

        public override void InstallBindings()
        {
            Container.Bind<ConfigFileReader>().FromInstance(_configFileReader).AsSingle();
            Container.Bind<GameSettingsData>().FromInstance(_gameSettingsData).AsSingle(); 
            Container.Bind<EventConfig>().FromInstance(_eventConfig).AsSingle();
            Container.Bind<LawConfig>().FromInstance(_lawConfig).AsSingle();
            Container.Bind<ProjectConfig>().FromInstance(_projectConfig).AsSingle();
            Container.Bind<QuestConfig>().FromInstance(_questConfig).AsSingle();
            Container.Bind<ServerConfig>().FromInstance(_serverConfig).AsSingle();
            Container.Bind<InternetMapSettingsData>().FromNew().AsSingle();
            Container.Bind<ConfigFileWriter> ().FromNew().AsSingle();
            Container.Bind<ServerActionDataConfig>().FromInstance(_serverActionDataConfig).AsSingle();
            Container.Bind<ServerActionCaseDatasConfig>().FromInstance(_serverActionCaseDatasConfig).AsSingle();
            Container.Bind<GameCyclesConfig>().FromInstance(_gameCyclesConfig).AsSingle();
        }
    }
}