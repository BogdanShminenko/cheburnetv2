﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Utilities;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.DataInstallers
{
    public class AccuralInstaller : MonoInstaller
    {
        [SerializeField] private AccuralHandler _accuralHandler;

        public override void InstallBindings()
        {
            Container.Bind<AccuralHandler>().FromInstance(_accuralHandler).AsSingle();
            Container.Bind<AccuralInLoop>().FromNew().AsSingle();
            Container.Bind<AttitudeCheck>().FromNew().AsSingle();
        }
    }
}