﻿using Assets.Scripts.Utilities;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers.ConverterInstallers
{
    public class ConverterInstaller : MonoInstaller
    {
        [SerializeField] private DateTriggerConventer _dateTriggerConventer;

        public override void InstallBindings()
        {
            Container.Bind<AttitudeTrrigerConverter>().FromNew().AsSingle();
            Container.Bind<ResultAttitudeOperationConverter>().FromNew().AsSingle();
            Container.Bind<DateTriggerConventer>().FromInstance(_dateTriggerConventer).AsSingle();
            Container.Bind<ServerOwnerTriggerConverter>().FromNew().AsSingle();
            Container.Bind<DateToStringConverter>().FromNew().AsSingle();
            Container.Bind<AttitudeOperationToStringConverter>().FromNew().AsSingle();
        }
    }
}