﻿using Assets.Scripts.GameStates;
using Assets.Scripts.Utilities;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers
{
    public class GameStateInstaller : MonoInstaller
    {
        [SerializeField] private GameState _gameState;
        [SerializeField] private CheckUI _checkUI;
        [SerializeField] private ButtonsMenu _buttonsMenu;

        public override void InstallBindings()
        {
            Container.Bind<ButtonsMenu>().FromInstance(_buttonsMenu).AsSingle();
            Container.Bind<GameState>().FromInstance(_gameState).AsSingle();
            Container.Bind<CheckUI>().FromInstance(_checkUI).AsSingle();
        }
    }
}