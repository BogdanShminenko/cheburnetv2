﻿using Assets.Scripts.Game;
using Assets.Scripts.Utilities;
using System.Collections;
using System.ComponentModel;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private GameInfo _gameInfo;

        public override void InstallBindings()
        {
            Container.Bind<GameInfo>().FromInstance(_gameInfo).AsSingle();
        }
    }
}