﻿using Assets.Scripts.Game;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.DIInstallers
{
    public class GameLoopInstaller : MonoInstaller
    {
        [SerializeField] private GameLoop _gameLoop;

        public override void InstallBindings()
        {
            Container.Bind<GameLoop>().FromInstance(_gameLoop).AsSingle();
        }
    }
}