﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Data.GameData.FileLoaders;
using Assets.Scripts.MVC.EventMVC;
using Assets.Scripts.MVC.GameResultMVC;
using Assets.Scripts.MVC.InternetMVC;
using Assets.Scripts.MVC.LawsMVC;
using Assets.Scripts.MVC.ProjectMVC;
using Assets.Scripts.MVC.QuestMVC;
using Assets.Scripts.MVC.ServerMVC;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.Utilities;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game
{
    public class GameLoop : MonoBehaviour
    {
        [Inject] private EventController _eventController;
        [Inject] private ProjectController _projectController;
        [Inject] private LawController _lawController;
        [Inject] private ServerController _serverController;
        [Inject] private AccuralInLoop _accuralInLoop;
        [Inject] private QuestController _questController;
        [Inject] private TimerController _timerController;
        [Inject] private InternetController _internetController;
        [Inject] private GameSettingsData _gameSettingsData;
        [Inject] private GameResultController _gameResultController;
        private Coroutine _callTriggerPerSecondsCoroutine;
        private Coroutine _slowLoopCoroutine;
        private Coroutine _normalLoopCoroutine;
        private Coroutine _fastLoopCoroutine;

        private bool _createdFirstServer;

        private void Awake()
        {
            _serverController.OnCreatedFirstServer += CreatedFirstServerHandler;
        }

        public void StartLoop()
        {
            _callTriggerPerSecondsCoroutine = StartCoroutine(CallTriggerPerSeconds());
            _slowLoopCoroutine = StartCoroutine(SlowLoop());
            _normalLoopCoroutine = StartCoroutine(NormalLoop());
            _fastLoopCoroutine = StartCoroutine(FastLoop());

        }

        public void Pause()
        {
            StopCoroutine(_callTriggerPerSecondsCoroutine);
            StopCoroutine(_slowLoopCoroutine);
            StopCoroutine(_normalLoopCoroutine);
            StopCoroutine(_fastLoopCoroutine);
        }

        public void Continue()
        {
            _callTriggerPerSecondsCoroutine = StartCoroutine(CallTriggerPerSeconds());
            _slowLoopCoroutine = StartCoroutine(SlowLoop());
            _normalLoopCoroutine = StartCoroutine(NormalLoop());
            _fastLoopCoroutine = StartCoroutine(FastLoop());
        }

        private void CreatedFirstServerHandler()
        {
            if (_createdFirstServer)
                return;
            _internetController.CalculateUsers();
            _createdFirstServer = true;
        }


        private IEnumerator CallTriggerPerSeconds()
        {
            while (true)
            {
                _eventController.CallTriggerEvent();
                _projectController.CallTriggerProjects();
                _lawController.CallTriggerLaws();
                _serverController.CallTriggerServer();
                _serverController.CallTriggerServerActionCase();
                _questController.CallTriggerAndCompliteQuest();
                _timerController.TryTriggerGameCycle();
                _gameResultController.CheckToResult();
                yield return new WaitForSeconds(0.001f);
            }
        }

        private IEnumerator SlowLoop()
        {
            float tick = _gameSettingsData.Tick * _gameSettingsData.Slow;
            while (true)
            {
                _accuralInLoop.CalculateAttitudeResultsSlow();
                yield return new WaitForSeconds(tick);
            }
        }

        private IEnumerator NormalLoop()
        {
            float tick = _gameSettingsData.Tick * _gameSettingsData.Normal;
            while (true)
            {
                _accuralInLoop.CalculateAttitudeResultsNormal();
                yield return new WaitForSeconds(tick);
            }
        }

        private IEnumerator FastLoop()
        {
            float tick = _gameSettingsData.Tick * _gameSettingsData.Fast;
            while (true)
            {
                _accuralInLoop.CalculateAttitudeResultsFast();
                yield return new WaitForSeconds(tick);
            }
        }
    }
}