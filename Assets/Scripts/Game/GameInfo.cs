﻿using Assets.Scripts.Accrual;
using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.MVC.InternetControllMVC;
using Assets.Scripts.MVC.UsersAttitude;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game
{
    public class GameInfo : MonoBehaviour
    {
        [SerializeField] private List<string> _resultTrigger = new List<string>();
        [Inject] private MoneyModel _moneyModel;
        [Inject] private WillModel _willModel;
        [Inject] private UsersAttitudeModel _usersAttitudeModel;
        [Inject] private InternetControllModel _internetControllModel;
        [SerializeField] private GameSettingsData _gameSettingsData;

        public float Slow => _gameSettingsData.Slow;
        public float Normal => _gameSettingsData.Normal;
        public float Fast => _gameSettingsData.Fast;

        public IEnumerable<string> ResultTrigger => _resultTrigger;

        public void SetResultTriggers(IEnumerable<string> resultTrigger)
        {
            _resultTrigger = resultTrigger.ToList();
        }

        public int GetAttitudeByType(CurrencyType currencyType)
        {
            if(currencyType == CurrencyType.Will)
            {
                return (int)_willModel.CurrentAmount;
            }
            else if (currencyType == CurrencyType.Money)
            {
                return (int)_moneyModel.CurrentAmount;
            }
            else if (currencyType == CurrencyType.Rebel)
            {
                return (int)_usersAttitudeModel.RebelUsersPercent;
            }
            else if (currencyType == CurrencyType.Loyal)
            {
                return (int)_usersAttitudeModel.LoyalUsersPercent;
            }
            else if (currencyType == CurrencyType.Neutral)
            {
                return (int)_usersAttitudeModel.NeutralUsersPercent;
            }
            else if (currencyType == CurrencyType.InternetControl)
            {
                return _internetControllModel.InternetControll;
            }
            return 0;
        }

        public void AddTrigger(string trigger)
        {
            _resultTrigger.Add(trigger);
        }

        public bool CheckTrigger(string trigger)
        {
            if(_resultTrigger.Contains(trigger))
                return true;
            return false;
        }

    }
}