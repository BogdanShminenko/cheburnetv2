﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Game
{
    [CreateAssetMenu(fileName = "GameSettings" , menuName = "ScriptableObjects/GameSettings")]
    public class GameSettings : ScriptableObject
    {
        public bool IsStartWithSave { get; set; }
    }
}