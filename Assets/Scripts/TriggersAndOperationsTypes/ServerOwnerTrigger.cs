﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.TriggersAndOperationsTypes
{
    [Serializable]
    public class ServerOwnerTrigger 
    {
        public int ServerInOwn { get; private set; }

        public ServerOwnerTrigger(int serverInOwn)
        {
            ServerInOwn = serverInOwn;
        }

    }
}