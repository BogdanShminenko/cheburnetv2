﻿using Assets.Scripts.Accrual;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.TriggersAndOperationsTypes
{
    [Serializable]

    public class AttitudeTrigger
    {
        [field: SerializeField] public OperationType AccuralDirection { get; set; }
        [field: SerializeField] public int CurrentAdditude { get; set; }


        public AttitudeTrigger(OperationType accuralDirection, int currentAdditude)
        {
            CurrentAdditude = currentAdditude;
            AccuralDirection = accuralDirection;
        }
    }
}