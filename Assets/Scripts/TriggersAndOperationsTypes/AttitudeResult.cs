﻿using Assets.Scripts.Accrual;
using Assets.Scripts.MVC.UsersAttitude;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Collections;
using UnityEngine;
using Assets.Scripts.Game;
using System;

namespace Assets.Scripts.TriggersAndOperationsTypes
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ValueCangeType
    {
        Moment = 0,
        Slow = 1,
        Normal = 2,
        Fast = 3
    }

    [Serializable]
    public class AttitudeResult
    {
        public ValueCangeType ValueCangeType { get; set; }
        public int Count { get; set; }
        public int StartCount { get; set; }
        public CurrencyType CurrencyType { get; set; }
        public OperationType OperationType { get; set; }
        public float StartSeconds { get; set; }
        
        public AttitudeResult(float slow , float normal , float fast,ValueCangeType valueCangeType, int count, CurrencyType currencyType, OperationType operationType)
        {
            ValueCangeType = valueCangeType;
            Count = Mathf.Abs(count);
            StartCount = Count;
            CurrencyType = currencyType;
            OperationType = operationType;
            if(ValueCangeType == ValueCangeType.Slow)
            {
                StartSeconds = slow;
            }
            else if (ValueCangeType == ValueCangeType.Normal)
            {
                StartSeconds = normal;
            }
            else if (ValueCangeType == ValueCangeType.Fast)
            {
                StartSeconds = fast;
            }
        }

        public void DecreaseCountByOne()
        {
            Count--;
        }

    }
}