﻿using Assets.Scripts.Accrual;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.TriggersAndOperationsTypes
{
    [Serializable]
    public class AttitudeOperation
    {
        [field: SerializeField] public CurrencyType CurrencyType { get; set; }
        [field: SerializeField] public OperationType AccuralDirection { get; set; }
        [field: SerializeField] public int CurrentAdditude { get; set; }

        public AttitudeOperation(OperationType accuralDirection, CurrencyType currencyType, int currentAdditude)
        {
            CurrentAdditude = currentAdditude;
            AccuralDirection = accuralDirection;
            CurrencyType = currencyType;
        }
    }
}