﻿using Newtonsoft.Json;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.TriggersAndOperationsTypes
{
    [Serializable]
    public class DateTrigger 
    {
        [field: SerializeField] public int Year { get; set; }
        [field: SerializeField] public int Month { get; set; }
        public DateTrigger(int year, int month)
        {
            Year = year;
            Month = month;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj is DateTrigger dateTrigger)
            {
                if(dateTrigger.Year == Year && dateTrigger.Month == Month) 
                    return true;
                else 
                    return false;
                
            }
            return base.Equals(obj);
        }

        [JsonIgnore] public DateTime DateTime => new DateTime(Year, Month, 1);
    }
}