﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Data.GameData
{

    [Serializable]
    public class LawData
    {
        public string LawID { get; set; }
        public string TriggerDate { get; set; }
        public string TriggerSignal { get; set; }
        public int TriggerRebelOver { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TextExpected { get; set; }
        public int Price1Will { get; set; }
        public int Chance1 { get; set; }
        public int Price2Will { get; set; }
        public int Chance2 { get; set; }
        public int Price3Money { get; set; }
        public int Chance3 { get; set; }
        public string TextFail { get; set; }
        public int HoldTime { get; set; }
        public string TextSuccess { get; set; }
        public int ResultRebel { get; set; }
        public string ResultRebelSpeed { get; set; }
        public int ResultLoyal { get; set; }
        public string ResultLoyalSpeed { get; set; }
        public int ResultControl { get; set; }
        public string ResultControlSpeed { get; set; }
        public string ResultTrigger { get; set; }
        public string Icon { get; set; }

    }
}