﻿using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Data.GameData
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ServerPerInternetType
    {
        Inner = 0,
        Outer = 1
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Ownage
    {
        User = 0,
        Free = 1
    }

    [Serializable]
    public class ServerData 
    {
        public string ServerID { get; set; }
        public int ServerGroup { get; set; }
        public string Type { get; set; }
        public string TriggerDate { get; set; }
        public float TriggerRebelOver { get; set; }
        public float TriggerRebelUnder { get; set; }
        public float TriggerLoyalOver { get; set; }
        public float TriggerLoyalUnder { get; set; }
        public string TriggerSignal { get; set; }
        public float Range { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public int ResultControl { get; set; }
        public string ResultControlSpeed { get; set; }
        public string Description { get; set; }
        public string Ownage { get; set; }
    }
}