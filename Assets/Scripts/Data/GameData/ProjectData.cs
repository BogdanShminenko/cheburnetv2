﻿using Newtonsoft.Json;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Data.GameData
{
    [Serializable]
    public class ProjectData
    {
        public string ProjectID { get; set; }
        public string TriggerDate { get; set; }
        public string TriggerSignal { get; set; }
        public string Logo { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TextDisabled { get; set; }
        public string TextAvailable { get; set; }
        public int PriceDevelop { get; set; }
        public int PriceBuy { get; set; }
        public int Time { get; set; }
        public string TextProcess { get; set; }
        public string TextComplete { get; set; }
        public string ResultTriggerComplete { get; set; }
        public string TextImplemented { get; set; }
        public string ResultTriggerImplemented { get; set; }
        public int ResultRebel { get; set; }
        public string ResultRebelSpeed { get; set; }
        public int ResultLoyal { get; set; }
        public string ResultLoyalSpeed { get; set; }
        public int ResultControl { get; set; }
        public string Icon { get; set; }

        public string ResultControlSpeed { get; set; }
        public ProjectActionsData ProjectActionsData;
    }
}