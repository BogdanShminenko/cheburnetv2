﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Data.GameData
{
    [Serializable]
    public class ActionServer
    {
        public string Action;
    }

    [Serializable]
    public class ServerActionContainer 
    {
        public int SeerverActionID;
        public string TimeTrigger;
        public string LoyaltyTrigger;
        public string Action;
        public string Title;
        public string Text;
        public int MoneyPrice;
        public int WillPrice;
        public string Condition;
        public string ConditionText;
        public string Result;
    }
}