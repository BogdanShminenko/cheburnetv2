﻿
using System;

namespace Assets.Scripts.Data.GameData
{
    [Serializable]
    public class ServerActionCasesData 
    {
        public string ServerID { get; set; }
        public string ActionCaseID { get; set; }
        public int ActionCaseOrder { get; set; }
        public string TriggerDate { get; set; }
        public int TriggerRebelOver { get; set; }
        public int TriggerRebelUnder { get; set; }
        public int TriggerLoyalOver { get; set; }
        public int TriggerLoyalUnder { get; set; }
        public string TriggerSignal { get; set; }
    }
}