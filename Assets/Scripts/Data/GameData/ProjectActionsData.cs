﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Data.GameData
{
    [Serializable]
    public class ActionGrayCaseData
    {
        public string Text;
    }
    [Serializable]
    public class ActionYellowCaseData
    {
        public string Text;
        public string Button;
        public int Price;
        public int TimeYear;
        public int TimeMonth;
        public string[] Result;
    }
    [Serializable]
    public class ActionCaseProcessData
    {
        public string Text;
        public string PBar;
        public int TimeYear;
        public int TimeMonth;
        public string[] Result;
    }
    [Serializable]
    public class ActionBlueCaseData
    {
        public string Text;
        public string Button;
        public int DevelopPrice;
        public int BuyPrice;
        public int TimeYear;
        public int TimeMonth;
        public string[] Result;
    }
    [Serializable]
    public class ActionGreenCaseData
    {
        public string Text;
        public string Button;
        public int Result;
    }
    [Serializable]
    public class ActionCompleteCaseData
    {
        public string Text;
        public string[] Result;
    }

    [Serializable]
    public class ProjectActionsData
    {
        public int ID;
        public ActionGrayCaseData ActionGrayCase;
        public ActionYellowCaseData ActionYellowCase;
        public ActionCaseProcessData ProcessActionCase;
        public ActionBlueCaseData ActionBlueCase;
        public ActionGreenCaseData ActionGreenCase;
        public ActionCompleteCaseData ActionCompleteCase;
    }
}