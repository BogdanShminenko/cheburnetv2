﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Data.GameData
{
    [Serializable]
    public class GameCyclesData
    {
        public string CycleID { get; set; }
        public string TriggerDate { get; set; }
    }
}