﻿using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Data.GameData
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TypeOfEvent
    {
        Express = 0,
        Simple = 1
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TypeOfResusable
    {
        Resusability = 0,
        Disposability = 1
    }

    [Serializable]
    public class EventData
    {
        public string EventID { get; set; }
        public string EventType { get; set; }
        public string Reusable { get; set; }
        public string TriggerDate { get; set; }
        public int TriggerRebelOver { get; set; }
        public int TriggerRebelUnder { get; set; }
        public int TriggerLoyalOver { get; set; }
        public int TriggerLoyalUnder { get; set; }
        public string TriggerSignal { get; set; }
        public float Range { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ResultRebel { get; set; }
        public string ResultRebelSpeed { get; set; }
        public int ResultLoyal { get; set; }
        public string ResultLoyalSpeed { get; set; }
        public int ResultControl { get; set; }
        public string ResultControlSpeed { get; set; }
        public string ResultTrigger { get; set; }
        public string Icon { get; set; }

    }
}