using Assets.Scripts.Data.GameData;
using Assets.Scripts.Data.GameData.Configs;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettingsData" , menuName = "ScriptableObjects/GameSettingsData")]
public sealed class GameSettingsData : ScriptableObject , IConfig
{
    public uint StartYear = 2000;
    public uint EndYear = 2025;
    public float GameYearInSeconds = 15;
    public float MonthInYearCount = 12;
    public uint GameCycleIntervalInYears = 5;
    public uint MaxMoneyAmount = 1000;
    public float MoneyRestoreTimeCofficient = 5;
    public uint MoneyRestoreAmount = 20;
    public uint MaxWillAmount = 500;
    public float WillRestoreTimeCofficient = 2;
    public uint WillRestoreAmount = 10;
    public uint InternetControll = 10;

    public float WillMaxIncrease = 0.2f;
    public float WillRestoreIncrease = 0.2f;
    public float MoneyIncreaseForQuests = 0.1f ;
    public float MoneyIncreaseDefault = 0.5f ;
    public float Slow;
    public float Normal;
    public float Fast;


    public uint RebelUsersPercent = 30;
    public uint LoyalUsersPercent = 20;

    public float Tick => GameYearInSeconds / MonthInYearCount;

    public uint GameMounthInYear
    {
        get
        {
            return (uint)(60 / GameYearInSeconds);
        }
    }

    public uint CountOfYears
    {
        get
        {
            if(EndYear - StartYear <= 0)
            {
                return 10;
            }
            else
            {
                return EndYear - StartYear;
            }
        }
    } 


    public void SetValues<T>(T config)
    {
        if(config is GameSettingsData gameSettingsData)
        {
            StartYear = gameSettingsData.StartYear;
            EndYear = gameSettingsData.EndYear;
            GameCycleIntervalInYears = gameSettingsData.GameCycleIntervalInYears;
            GameYearInSeconds = gameSettingsData.GameYearInSeconds;
            MaxMoneyAmount = gameSettingsData.MaxMoneyAmount;
            MoneyRestoreAmount = gameSettingsData.MoneyRestoreAmount;
            MoneyRestoreTimeCofficient = gameSettingsData.MoneyRestoreTimeCofficient;
            MaxWillAmount = gameSettingsData.MaxWillAmount;
            WillRestoreTimeCofficient = gameSettingsData.WillRestoreTimeCofficient;
            WillRestoreAmount = gameSettingsData.WillRestoreAmount;
            InternetControll = gameSettingsData.InternetControll;
            WillMaxIncrease = gameSettingsData.WillMaxIncrease;
            WillRestoreIncrease = gameSettingsData.WillRestoreIncrease;
            MoneyIncreaseForQuests = gameSettingsData.MoneyIncreaseForQuests;
            MoneyIncreaseDefault = gameSettingsData.MoneyIncreaseDefault;
            RebelUsersPercent = gameSettingsData.RebelUsersPercent;
            LoyalUsersPercent = gameSettingsData.LoyalUsersPercent;
            Slow = gameSettingsData.Slow;
            Normal = gameSettingsData.Normal;
            Fast = gameSettingsData.Fast;
        }
        else
        {
            Debug.LogError("Wrong type config");
        }
    }
}
