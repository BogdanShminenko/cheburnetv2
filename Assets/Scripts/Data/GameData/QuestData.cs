﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Data.GameData
{
    [Serializable]
    public class QuestData
    {
        public string QuestID { get; set; }
        public string TriggerDate { get; set; }
        public int TriggerRebelOver { get; set; }
        public int TriggerRebelUnder { get; set; }
        public int TriggerLoyalOver { get; set; }
        public int TriggerLoyalUnder { get; set; }
        public string TriggerSignal { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string CheckBeforeDate { get; set; }
        public int CheckRebelOver { get; set; }
        public int CheckRebelUnder { get; set; }
        public int CheckLoyalOver { get; set; }
        public int CheckLoyalUnder { get; set; }
        public int CheckControlOver { get; set; }
        public string CheckSignal { get; set; }
    }
}