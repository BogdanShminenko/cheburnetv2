﻿
using System;

namespace Assets.Scripts.Data.GameData
{
    [Serializable]
    public class ServerActionData 
    {
        public string ServerID { get; set; }
        public string ActionCaseID { get; set; }
        public int OrderNumber { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int PriceMoney { get; set; }
        public int PriceWill { get; set; }
        public string ResultOwnage { get; set; }
        public int ResultRebel { get; set; }
        public string ResultRebelSpeed { get; set; }
        public int ResultLoyal { get; set; }
        public string ResultLoyalSpeed { get; set; }
        public int ResultControl { get; set; }
        public string ResultControlSpeed { get; set; }
        public string ResultTrigger { get; set; }
    }
}