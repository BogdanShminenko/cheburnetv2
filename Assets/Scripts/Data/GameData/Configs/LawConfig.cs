﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Data.GameData.Configs
{
    [CreateAssetMenu(fileName = "LawConfig", menuName = "ScriptableObjects/LawConfig")]
    public sealed class LawConfig : ScriptableObject , IConfig
    {
        public List<LawData> Laws;

        public void SetValues<T>(T config)
        {
            if(config is LawConfig lawConfig)
            {
                Laws = lawConfig.Laws;

#if UNITY_EDITOR
                EditorUtility.SetDirty(this);
#endif
            }
            else
            {
                Debug.LogError("Wrong config type");
            }
        }
    }
}