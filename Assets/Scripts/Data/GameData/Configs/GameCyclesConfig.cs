﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Data.GameData.Configs
{
    [CreateAssetMenu(fileName = "GameCyclesConfig", menuName = "ScriptableObjects/GameCyclesConfig")]
    public class GameCyclesConfig : ScriptableObject, IConfig
    {
        public List<GameCyclesData> GameCyclesDatas;

        public void SetValues<T>(T config)
        {
            if (config is GameCyclesConfig gameCycle)
            {
                GameCyclesDatas = gameCycle.GameCyclesDatas;
#if UNITY_EDITOR
                EditorUtility.SetDirty(this);
#endif
            }
            else
            {
                Debug.LogError("Wrong config type");
            }
        }
    }
}