﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Data.GameData.Configs
{
    public interface IConfig
    {
        void SetValues<T>(T config);
    }
}