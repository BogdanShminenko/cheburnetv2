﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Data.GameData.Configs
{
    [CreateAssetMenu(fileName = "ServerActionDataConfig", menuName = "ScriptableObjects/ServerActionDataConfig")]
    public class ServerActionDataConfig : ScriptableObject, IConfig
    {
        public List<ServerActionData> ServerActionDatas;

        public void SetValues<T>(T config)
        {
            if (config is ServerActionDataConfig serverConfig)
            {
                ServerActionDatas = serverConfig.ServerActionDatas;

#if UNITY_EDITOR
                EditorUtility.SetDirty(this);
#endif
            }
            else
            {
                Debug.LogError("Wrong config type");
            }
        }
    }
}