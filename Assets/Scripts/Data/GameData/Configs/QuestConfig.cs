﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Data.GameData.Configs
{
    [CreateAssetMenu(fileName = "QuestConfig", menuName = "ScriptableObjects/QuestConfig")]
    public sealed class QuestConfig : ScriptableObject , IConfig
    {
        public List<QuestData> Quests;

        public void SetValues<T>(T config)
        {
            if(config is QuestConfig questConfig)
            {
                Quests = questConfig.Quests;

#if UNITY_EDITOR
                EditorUtility.SetDirty(this);
#endif
            }
            else
            {
                Debug.LogError("Wrong config type");
            }
        }
    }
}