﻿using Assets.Scripts.MVC.LawsMVC;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Data.GameData.Configs
{
    [CreateAssetMenu(fileName = "ProjectConfig", menuName = "ScriptableObjects/ProjectConfig")]
    public sealed class ProjectConfig : ScriptableObject , IConfig
    {
        public List<ProjectData> Projects;

        public void SetValues<T>(T config)
        {
            if(config is ProjectConfig projectConfig)
            {
                Projects = projectConfig.Projects;

#if UNITY_EDITOR
                EditorUtility.SetDirty(this);
#endif
            }
            else
            {
                Debug.LogError("Wrong config type");
            }
        }
    }
}