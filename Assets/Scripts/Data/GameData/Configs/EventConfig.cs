﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Data.GameData.Configs
{
    [CreateAssetMenu(fileName = "EventConfig", menuName = "ScriptableObjects/EventConfig")]

    public sealed class EventConfig : ScriptableObject , IConfig
    {
        public List<EventData> Events;


        public void SetValues<T>(T config)
        {
            if(config is EventConfig eventConfig)
            {
                Events = eventConfig.Events;
#if UNITY_EDITOR
                EditorUtility.SetDirty(this);
#endif
            }
            else
            {
                Debug.LogError("Wrong config type");
            }
        }
    }
}