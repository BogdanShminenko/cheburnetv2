﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Data.GameData.Configs
{
    [CreateAssetMenu(fileName = "ServerConfig", menuName = "ScriptableObjects/ServerConfig")]
    public sealed class ServerConfig : ScriptableObject , IConfig
    {
        public List<ServerData> ServerDatas;


        public void SetValues<T>(T config)
        {
            if(config is ServerConfig serverConfig)
            {
                ServerDatas = serverConfig.ServerDatas;
#if UNITY_EDITOR
                EditorUtility.SetDirty(this);
#endif
            }
            else
            {
                Debug.LogError("Wrong config type");
            }
        }
    }
}