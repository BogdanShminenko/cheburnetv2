﻿using Assets.Scripts.MVC.QuestMVC;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Data.GameData.Configs
{
    [CreateAssetMenu(fileName = "ServerActionCaseDatasConfig", menuName = "ScriptableObjects/ServerActionCaseDatasConfig")]
    public class ServerActionCaseDatasConfig : ScriptableObject, IConfig
    {
        public List<ServerActionCasesData> ServerActionCasesDatas;

        public void SetValues<T>(T config)
        {
            if (config is ServerActionCaseDatasConfig serverConfig)
            {
                ServerActionCasesDatas = serverConfig.ServerActionCasesDatas;

#if UNITY_EDITOR
                EditorUtility.SetDirty(this);
#endif
            }
            else
            {
                Debug.LogError("Wrong config type");
            }
        }
    }
}