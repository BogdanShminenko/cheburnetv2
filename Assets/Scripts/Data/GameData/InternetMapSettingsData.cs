﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Data.GameData
{
    [Serializable]
    public class InternetMapSettingsData 
    {
        public byte ServerInnerCount { get; set; } = 0;
        public byte NodeCount { get; set; } = 0;
        public short UsersInnerCount { get; set; } = 0;

        public byte ServerOuterCount { get; set; } = 0;
        public short UsersOuterCount { get; set; } = 0;

        public int MapInnerXSize { get; set; } = 18;
        public int MapInnerYSize { get; set; }= 18;
        public int MapOuterXSize { get; set; } = 26;
        public int MapOuterYSize { get; set; } = 7;
        public float Radius { get; set; } = 32;
    }
}