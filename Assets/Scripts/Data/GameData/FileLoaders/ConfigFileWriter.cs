﻿using Assets.Scripts.Data.GameData.Configs;
using Newtonsoft.Json;
using System.IO;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Data.GameData.FileLoaders
{
    public class ConfigFileWriter
    {
        [Inject] private GameSettingsData _gameSettingsData;
        [Inject] private EventConfig _eventConfig;
        [Inject] private LawConfig _lawConfig;
        [Inject] private ProjectConfig _projectConfig;
        [Inject] private QuestConfig _questConfig;
        [Inject] private ServerConfig _serverConfig;

        public void WriteFiles()
        {
            WriteFile<GameSettingsData>(_gameSettingsData, "ConfigData.json");
            WriteFile<EventConfig>(_eventConfig, "EventConfig.json");
            WriteFile<LawConfig>(_lawConfig, "LawConfig.json");
            WriteFile<ProjectConfig>(_projectConfig, "ProjectConfig.json");
            WriteFile<QuestConfig>(_questConfig, "QuestConfig.json");
            WriteFile<ServerConfig>(_serverConfig, "ServerConfig.json");
        }

        public void WriteFile<T>(object config,string configName)
        {
            string path;

#if UNITY_EDITOR
            path = Path.Combine(Application.streamingAssetsPath, configName);
#else
            path = Path.Combine(Application.dataPath, configName);
#endif
            
            string jsonString = JsonConvert.SerializeObject((T)config, Formatting.Indented);
            File.WriteAllText(path, jsonString);
        }

    }
}