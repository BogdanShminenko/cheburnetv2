﻿using Assets.Scripts.Data.GameData.Configs;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.Networking;
using Zenject;

namespace Assets.Scripts.Data.GameData.FileLoaders
{
    public class ConfigFileReader : MonoBehaviour
    {
        [SerializeField] private TMP_Text _logg;

        [Inject] private GameSettingsData _gameSettingsData;
        [Inject] private EventConfig _eventConfig;
        [Inject] private LawConfig _lawConfig;
        [SerializeField] private ProjectConfig _projectConfig;
        [Inject] private QuestConfig _questConfig;
        [Inject] private ServerConfig _serverConfig;
        [Inject] private ServerActionDataConfig _serverActionDataConfig;
        [Inject] private ServerActionCaseDatasConfig _serverActionCaseDatasConfig;
        [Inject] private GameCyclesConfig _gameCyclesConfig;
        private int _initedConfigurations;
        public event Action OnInitedData;


        private void Start()
        {
            ReadFiles();
        }

        public void ReadFiles()
        {
            ReadFile<GameSettingsData>(_gameSettingsData, "ConfigData.json");
            ReadFile<EventConfig>(_eventConfig, "EventConfig.json");
            ReadFile<LawConfig>(_lawConfig, "LawConfig.json");
            ReadFile<ProjectConfig>(_projectConfig, "ProjectConfig.json");
            ReadFile<QuestConfig>(_questConfig, "QuestConfig.json");
            ReadFile<ServerConfig>(_serverConfig, "ServerConfig.json");
            ReadFile<ServerActionDataConfig>(_serverActionDataConfig, "ServerActions.json");
            ReadFile<ServerActionCaseDatasConfig>(_serverActionCaseDatasConfig, "ServerActionCases.json");
            ReadFile<GameCyclesConfig>(_gameCyclesConfig, "GameCyclesConfig.json");
            //OnInitedData?.Invoke();
        }

        public void LoadProjects()
        {
            ReadFile<ProjectConfig>(_projectConfig, "ProjectConfig.json");
        }

        private void InitedConfig()
        {
            _initedConfigurations++;
            if (_initedConfigurations == 9)
                OnInitedData?.Invoke();
        }

        public void ReadFile<T>(IConfig configHandler,string configName)
        {
#if UNITY_EDITOR
            string path = Path.Combine(Application.streamingAssetsPath, configName);
            var json = File.ReadAllText(path);
            T config = JsonConvert.DeserializeObject<T>(json);
            configHandler.SetValues(config);
            InitedConfig();
#else
            StartCoroutine(GetJsonData<T>(configHandler, configName));
#endif

        }

        IEnumerator GetJsonData<T>(IConfig configHandler, string jsonUrl)
        {
            string filePath;
            filePath = Path.Combine(Application.streamingAssetsPath, jsonUrl);
            string dataAsJson;
            if (filePath.Contains("://") || filePath.Contains(":///"))
            {
                WWW www = new WWW(filePath);
                while (!www.isDone)
                {
                    yield return null;
                }
                dataAsJson = www.text;
            }
            else
            {
                dataAsJson = File.ReadAllText(filePath);
            }

            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T config = JsonConvert.DeserializeObject<T>(dataAsJson , settings);
            configHandler.SetValues(config);
            InitedConfig();
        }

    }
}