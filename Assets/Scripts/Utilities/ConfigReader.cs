﻿using Assets.Scripts.Data.GameData.Configs;
using Assets.Scripts.Data.GameData.FileLoaders;
using Newtonsoft.Json;
using System.Collections;
using System.IO;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Utilities
{
    [CreateAssetMenu(fileName = "ConfigReader", menuName = "ScriptableObjects/ConfigReader")]
    public class ConfigReader : ScriptableObject
    {
        [SerializeField] private GameSettingsData _gameSettingsData;
        [SerializeField] private EventConfig _eventConfig;
        [SerializeField] private LawConfig _lawConfig;
        [SerializeField] private ProjectConfig _projectConfig;
        [SerializeField] private QuestConfig _questConfig;
        [SerializeField] private ServerConfig _serverConfig;
        [SerializeField] private ServerActionDataConfig _serverActionDataConfig;
        [SerializeField] private ServerActionCaseDatasConfig _serverActionCaseDatasConfig;
        [SerializeField] private GameCyclesConfig _gameCyclesConfig;


        public void ReadFiles()
        {
            //ReadFile<GameSettingsData>(GameSettingsData, "ConfigData.json");
            //ReadFile<EventConfig>(_eventConfig, "EventConfig.json");
            //ReadFile<LawConfig>(_lawConfig, "LawConfig.json");
            //ReadFile<ProjectConfig>(_projectConfig, "ProjectConfig.json");
            //ReadFile<QuestConfig>(_questConfig, "QuestConfig.json");
            //ReadFile<ServerConfig>(_serverConfig, "ServerConfig.json");
            //ReadFile<ServerActionDataConfig>(_serverActionDataConfig, "ServerActions.json");
            //ReadFile<ServerActionCaseDatasConfig>(_serverActionCaseDatasConfig, "ServerActionCases.json");
            //ReadFile<GameCyclesConfig>(_gameCyclesConfig, "GameCyclesConfig.json");
            //Debug.Log("Readed");
        }

//        public void ReadFile<T>(IConfig configHandler, string configName)
//        {
//            string path;

//#if UNITY_EDITOR
//            path = Path.Combine(Application.streamingAssetsPath, configName);

//#else
//            path = Path.Combine(Application.persistentDataPath, "/Resources/" + configName);
//#endif

//            var json = File.ReadAllText(path);
//            T config = JsonConvert.DeserializeObject<T>(json);
//            configHandler.SetValues(config);
        
//        }

//        IEnumerator GetJsonData<T>(string jsonUrl)
//        {
//            WWW www = new WWW(jsonUrl);

//            yield return www;

//            T config = JsonConvert.DeserializeObject<T>(www.text);

//        }
    }
}