﻿using Assets.Scripts.Data.GameData;
using Assets.Scripts.TriggersAndOperationsTypes;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public class DateTriggerConventer : MonoBehaviour
    {
        public bool TryConvertStringToDateTrigger(string date, out DateTrigger dateTrigger)
        {
            if (date != null && date.Length > 0)
            {
                string[] data = date.Split('/');
                dateTrigger = new DateTrigger(int.Parse(data[0]), int.Parse(data[1]));
                return true;
            }
            dateTrigger = null;
            return false;
            
        }
    }
}