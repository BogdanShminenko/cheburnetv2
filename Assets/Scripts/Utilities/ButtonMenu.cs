﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Utilities
{
    public class ButtonMenu : MonoBehaviour
    {
        [SerializeField] private ButtonsMenu _buttonsMenu;
        [SerializeField] private Image _image;
        [SerializeField] private Sprite _activeSprite;
        [SerializeField] private Sprite _inactiveSprite;
        [SerializeField] private Vector3 _activePosition;

        private Vector3 _startPostion;

        private void Awake()
        {
            _startPostion = transform.localPosition;
        }

        public void EnterActive()
        {
            _buttonsMenu.ResetButtons();
            _image.sprite = _activeSprite;
            transform.localPosition = new Vector3(transform.localPosition.x , _activePosition.y, transform.localPosition.z);
        }

        public void ResetButton()
        {
            _image.sprite = _inactiveSprite;
            transform.localPosition = _startPostion;
        }

    }
}