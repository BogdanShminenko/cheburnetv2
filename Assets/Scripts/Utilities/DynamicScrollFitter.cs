using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.MVC.ProjectMVC.Views;

[ExecuteInEditMode]
public class DynamicScrollFitter : MonoBehaviour
{
    [SerializeField] private Transform[] _parents;
    [SerializeField] private RectTransform _selfTransfrom;
    [SerializeField] private float _spacing;
    [SerializeField] private Vector3 _baseDelta;
    private Transform[] childTransforms;
    private Vector3[] originalRelativePositions;

    //private void OnEnable()
    //{
    //    if (!Application.isPlaying)
    //    {
    //        UpdateSize();
    //    }
    //}

    public void UpdateSize()
    {
        _parents = GetComponentsInChildren<ProjectTreeItem>().Select(item => item.transform).ToArray();
        _selfTransfrom.sizeDelta = new Vector3(_selfTransfrom.sizeDelta.x, _baseDelta.y, 0);
        CheckForChanges();
        // �������� ��� �������� ���������� � ��������� �� �������� ������������� �������
        childTransforms = new Transform[transform.childCount];
        originalRelativePositions = new Vector3[transform.childCount];

    }



    public void CheckForChanges()
    {
        float min_x, max_x, min_y, max_y;
        min_x = max_x = transform.localPosition.x;
        min_y = max_y = transform.localPosition.y;
        List<RectTransform> rectTransforms = transform.GetComponentsInChildren<RectTransform>().ToList();
        foreach (var item in _parents)
            rectTransforms.AddRange(item.GetComponentsInChildren<RectTransform>().ToList());

        foreach (RectTransform child in rectTransforms)
        {
            Vector2 scale = child.sizeDelta;
            float temp_min_x, temp_max_x, temp_min_y, temp_max_y;

            temp_min_x = child.localPosition.x - (scale.x / 2);
            temp_max_x = child.localPosition.x + (scale.x / 2);
            temp_min_y = child.localPosition.y - (scale.y / 2);
            temp_max_y = child.localPosition.y + (scale.y / 2);

            if (temp_min_x < min_x)
                min_x = temp_min_x;
            if (temp_max_x > max_x)
                max_x = temp_max_x;

            if (temp_min_y < min_y)
                min_y = temp_min_y;
            if (temp_max_y > max_y)
                max_y = temp_max_y;
        }
        _selfTransfrom.sizeDelta = new Vector2(_selfTransfrom.sizeDelta.x, max_y - min_y);
    }

    //[SerializeField] private float _horizontalSpacing;
    //[SerializeField] private float _verticalSpacing;
    //public bool expandX, expandY = true;

    //RectTransform myTransform;
    //GridLayoutGroup layoutGroup;

    //[SerializeField] Vector2 cellSize;
    //[SerializeField] RectOffset padding;

    //Vector2 newScale;

    //// Use this for initialization
    //void Start()
    //{

    //    myTransform = GetComponent<RectTransform>();
    //    //layoutGroup = GetComponent<GridLayoutGroup>();
    //    //cellSize = layoutGroup.cellSize;
    //    //padding = layoutGroup.padding;

    //}

    //void OnGUI()
    //{
    //    newScale = myTransform.sizeDelta;

    //    if (expandX) newScale.x = padding.left + padding.right + ((cellSize.x + _horizontalSpacing) * transform.childCount);
    //    if (expandY) newScale.y = padding.top + padding.bottom + ((cellSize.y + _verticalSpacing) * transform.childCount);

    //    myTransform.sizeDelta = newScale;
    //}

}