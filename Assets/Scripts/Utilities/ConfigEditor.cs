using Assets.Scripts.Data.GameData.FileLoaders;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Zenject;

public class ConfigEditor : MonoBehaviour
{
#if UNITY_EDITOR
    [CustomEditor(typeof(GameSettingsData))]
    public class customButton : Editor
    {
        [Inject] private ConfigFileWriter _configFileWriter;
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            
            if (GUILayout.Button("Save"))
            {
                _configFileWriter.WriteFiles();
            }
        }

    }
#endif
}
