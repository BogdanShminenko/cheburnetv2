﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public class RandomSystem 
    {
        private float _m = 0;

        public bool CheckRandom(int randomMax)
        {
            float normalaziedRandomMax = randomMax / 100f;
            System.Random random = new System.Random();
            float rnd = (float)random.NextDouble();
            float probability = normalaziedRandomMax + (1 - normalaziedRandomMax) * _m;
            _m = probability;
            if (rnd <= probability)
            {
                _m = 0;
                return true;
            }
            return false;
        }

    }
}