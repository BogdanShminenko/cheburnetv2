using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillEffect : MonoBehaviour
{
    [SerializeField] private float _fillAmount = 1.0f;

    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void UpdateFill(float fill)
    {
        _spriteRenderer.material.SetFloat("_FillAmount", fill); // ��� ��������� ���������� � �������
    }
}
