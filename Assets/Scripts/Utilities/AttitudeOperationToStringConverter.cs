﻿using Assets.Scripts.TriggersAndOperationsTypes;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public class AttitudeOperationToStringConverter
    {
        public bool TryConvertAttitudeToOperation(AttitudeOperation attitudeOperation, out string result)
        {
            result = "";

            if (attitudeOperation == null)
            {
                result = "";
                return false;
            }

            if(attitudeOperation.AccuralDirection == Accrual.OperationType.Increase)
            {
                result += "+ ";
            }
            else if (attitudeOperation.AccuralDirection == Accrual.OperationType.Decrease)
            {
                result += "- ";
            }

            result += attitudeOperation.CurrentAdditude.ToString() + " ";

            if (attitudeOperation.CurrencyType == Accrual.CurrencyType.Will)
            {
                result += "Воля";
            }
            else if (attitudeOperation.CurrencyType == Accrual.CurrencyType.Money)
            {
                result += "Деньги";
            }
            else if (attitudeOperation.CurrencyType == Accrual.CurrencyType.Loyal)
            {
                result += "Лояльности";
            }
            else if (attitudeOperation.CurrencyType == Accrual.CurrencyType.Rebel)
            {
                result += "Протестов";
            }
            else if (attitudeOperation.CurrencyType == Accrual.CurrencyType.Neutral)
            {
                result += "Нейтральных";
            }
            return true;
        }
    }
}