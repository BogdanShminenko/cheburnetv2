using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Distance : MonoBehaviour
{
    [SerializeField] private Transform _point1;
    [SerializeField] private Transform _point2;
    void Update()
    {
        if (_point1 != null && _point2 != null)
            Debug.Log(Vector3.Distance(_point1.position, _point2.position));
    }
}
