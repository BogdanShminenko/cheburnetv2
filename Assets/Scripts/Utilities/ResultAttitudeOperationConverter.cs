﻿using Assets.Scripts.Accrual;
using Assets.Scripts.TriggersAndOperationsTypes;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public class ResultAttitudeOperationConverter
    {
        public bool TryConventResultToAttitudeOperation(string result, out AttitudeOperation attitudeOperation)
        {
            if (int.TryParse(string.Join("", result.Where(c => char.IsDigit(c))), out int value))
            {
                CurrencyType currencyType = CurrencyType.Will;
                OperationType accuralDirection = OperationType.Increase;

                if (result.ToCharArray()[0] == '+')
                    accuralDirection = OperationType.Increase;
                else if (result.ToCharArray()[0] == '-')
                    accuralDirection = OperationType.Decrease;

                if (result.Split('_')[1] == "Rebel")
                    currencyType = CurrencyType.Rebel;
                else if (result.Split('_')[1] == "Loyal")
                    currencyType = CurrencyType.Loyal;
                else if (result.Split('_')[1] == "Neutral")
                    currencyType = CurrencyType.Neutral;

                attitudeOperation = new AttitudeOperation(accuralDirection, currencyType, value);
                return true;
            }
            attitudeOperation = null;
            return false;

        }
    }
}