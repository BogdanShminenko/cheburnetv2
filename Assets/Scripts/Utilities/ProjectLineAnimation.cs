using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectLineAnimation : MonoBehaviour
{
    [SerializeField] private LineRenderer _lineRenderer;
    [SerializeField] private Texture[] _textures;
    [SerializeField] private int _minTime;
    [SerializeField] private int _maxTime;
    [SerializeField] private float _frameTime;
    private int _index;

    private Coroutine _randomStartAnimateCoroutine;
    private Coroutine _animateCoroutine;

    public void SetSettings(int minTime, int maxTime, Texture[] textures, float frameTime)
    {
        _minTime = minTime;
        _maxTime = maxTime;
        _textures = textures;
        _frameTime = frameTime;
    }

    private void OnEnable()
    {
        _randomStartAnimateCoroutine = StartCoroutine(RandomStartAnimate());
        //_animateCoroutine = StartCoroutine(Animate());
    }

    private void OnDisable()
    {
        if(_randomStartAnimateCoroutine != null )
            StopCoroutine(_randomStartAnimateCoroutine);
        if(_animateCoroutine != null )
            StopCoroutine(_animateCoroutine);
    }

    private IEnumerator RandomStartAnimate()
    {
        int time;
        while (true)
        {
            StartCoroutine(Animate());
            time = Random.Range(_minTime, _maxTime);
            yield return new WaitForSeconds(time);
        }
    }

    private IEnumerator Animate()
    {
        while(_index < _textures.Length)
        {
            _lineRenderer.material.SetTexture("_MainTex", _textures[_index]);
            _index++;
            yield return new WaitForSeconds(_frameTime);
        }
        _index = 0;
        _lineRenderer.material.SetTexture("_MainTex", _textures[0]);
    }
}
