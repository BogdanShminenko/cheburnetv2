using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionChecker : MonoBehaviour
{
    private Vector3 _point1;
    private Vector3 _point2;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _point1 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }else if (Input.GetMouseButtonDown(1))
        {
            _point2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Debug.Log(Vector3.Distance(_point1,_point2));
        }
    }
}
