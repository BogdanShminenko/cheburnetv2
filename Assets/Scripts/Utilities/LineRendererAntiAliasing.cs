using UnityEngine;

[ExecuteInEditMode]
public class LineRendererAntiAliasing : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public int subdivisions = 2; // ���������� ������������� ��� �����������

    void OnEnable()
    {
        if (lineRenderer != null)
        {
            SmoothLine();
        }
    }

    void SmoothLine()
    {
        int originalVertexCount = lineRenderer.positionCount;
        Vector3[] originalPositions = new Vector3[originalVertexCount];
        lineRenderer.GetPositions(originalPositions);

        Vector3[] smoothedPositions = new Vector3[originalVertexCount * subdivisions];

        for (int i = 0; i < originalVertexCount - 1; i++)
        {
            int startIndex = i * subdivisions;
            int endIndex = (i + 1) * subdivisions;

            for (int j = startIndex; j < endIndex; j++)
            {
                float t = (float)(j - startIndex) / (endIndex - startIndex);
                smoothedPositions[j] = Vector3.Lerp(originalPositions[i], originalPositions[i + 1], t);
            }
        }

        lineRenderer.positionCount = smoothedPositions.Length;
        lineRenderer.SetPositions(smoothedPositions);
    }
}
