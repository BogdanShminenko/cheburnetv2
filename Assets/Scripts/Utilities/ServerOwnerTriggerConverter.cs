﻿using Assets.Scripts.TriggersAndOperationsTypes;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public class ServerOwnerTriggerConverter
    {
        public bool TryConvertStringToServerOwnerTrigger(string trigger, out ServerOwnerTrigger serverOwnerTrigger)
        {
            if (int.TryParse(string.Join("", trigger.Where(c => char.IsDigit(c))), out int value))
            {
                serverOwnerTrigger = new ServerOwnerTrigger(value);
                return true;
            }

            serverOwnerTrigger = null;
            return false;
        }
    }
}