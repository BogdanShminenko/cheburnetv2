﻿using Assets.Scripts.MVC.TimerMVC;
using System;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Utilities
{
    public class CalculateUsersGrow
    {
        [Inject] private TimerModel _timerModel;

        public int GetTotalUsersNumber()
        {
            int count = (int)(150 * Math.Tanh((_timerModel.TotalMonthCount / 80f) - 1.5f) + 150);
            return count;
        }
    }
}