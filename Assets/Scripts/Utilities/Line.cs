using Assets.Scripts.MVC.ProjectMVC;
using Assets.Scripts.MVC.ProjectMVC.Views;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TechnoBabelGames;
using Unity.VisualScripting;
using UnityEngine;

[ExecuteInEditMode]
public class Line : MonoBehaviour
{
    [SerializeField] private LineRenderer _lineRenderer;
    [SerializeField] private GameObject _pointPrefab;
    [SerializeField] private List<GameObject> _points = new List<GameObject>();
    [SerializeField] public int _subdivisions = 10; // ���������� ������������� ��� �����������
    [SerializeField] private List<ProjectTreeItem> _projectsTreeItems = new List<ProjectTreeItem>();
    private ProjectLineSettings _projectLineSettings;

    public IEnumerable<ProjectTreeItem> ProjectsTreeItems => _projectsTreeItems;
    [SerializeField] private int _numCornerVertices = 5;

    public void SetProjectLineSettings(ProjectLineSettings projectLineSettings)
    {
        _projectLineSettings = projectLineSettings;
    }


    public void UpdateLineSettings()
    {
        _lineRenderer.colorGradient = _projectLineSettings.Gradient;
        _lineRenderer.widthCurve = _projectLineSettings.WidthCurve;
    }

    public void SetPoints(Vector3 start, Vector3 end)
    {
        GameObject startPoint = Instantiate(_pointPrefab, start, Quaternion.identity, transform);
        GameObject endPoint = Instantiate(_pointPrefab, end, Quaternion.identity, transform);
        _points.Add(startPoint);
        _points.Add(endPoint);
        _lineRenderer.positionCount = 2;
        _lineRenderer.SetPosition(0, start);
        _lineRenderer.SetPosition(1, end);
    }

    public void SetProjectTreeItems(ProjectTreeItem firstProjectTreeItem, ProjectTreeItem secondProjectTreeItem)
    {
        _projectsTreeItems.Add(firstProjectTreeItem);
        _projectsTreeItems.Add(secondProjectTreeItem);
    }

    private void CreatePoints()
    {
        int count = _lineRenderer.positionCount - _points.Count;
        if(count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                Vector3 position = _lineRenderer.GetPosition(3 + i);
                GameObject point = Instantiate(_pointPrefab, new Vector3(position.x, position.y, 0), Quaternion.identity, transform);
                point.transform.localPosition = new Vector3(point.transform.localPosition.x, point.transform.localPosition.y, 0);
                _points.Add(point);
            }
        }
    }

    private void Update()
    {
        _lineRenderer.numCornerVertices = _numCornerVertices;
        _lineRenderer.numCapVertices = _numCornerVertices;
        CreatePoints();
        UpdatePoints();
    }


    public void UpdatePoints()
    {
        _points = _points.Where(item => item != null).ToList();
   
        for (int i = 0; i < _points.Count; i++)
        {
            _lineRenderer.SetPosition(i, new Vector3(_points[i].transform.position.x, _points[i].transform.position.y, 90));
        }
    }
}
