﻿using Assets.Scripts.MVC.UsersAttitude;
using Assets.Scripts.TriggersAndOperationsTypes;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Utilities
{
    public class AttitudeCheck
    {
        [Inject] private UsersAttitudeModel _usersAttitudeModel;


        public bool CheckByAttitude(AttitudeOperation attitudeTrigger)
        {
            if (attitudeTrigger != null)
            {
                int currentValue = 0;
                if (attitudeTrigger.CurrencyType == Accrual.CurrencyType.Rebel)
                {
                    currentValue = (int)_usersAttitudeModel.RebelUsersPercent;
                }
                else if (attitudeTrigger.CurrencyType == Accrual.CurrencyType.Loyal)
                {
                    currentValue =  (int)_usersAttitudeModel.LoyalUsersPercent;
                }
                else if (attitudeTrigger.CurrencyType == Accrual.CurrencyType.Neutral)
                {
                    currentValue = (int)_usersAttitudeModel.NeutralUsersPercent;
                }


                if (attitudeTrigger.AccuralDirection == Accrual.OperationType.Increase)
                {
                    if (currentValue >= attitudeTrigger.CurrentAdditude)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (attitudeTrigger.AccuralDirection == Accrual.OperationType.Decrease)
                {
                    if (currentValue <= attitudeTrigger.CurrentAdditude)
                    {

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;

        }
    }
}