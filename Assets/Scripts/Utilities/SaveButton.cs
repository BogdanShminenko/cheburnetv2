using Assets.Scripts.Data.GameData.FileLoaders;
using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
[CustomEditor(typeof(ConfigReader))]
public class SaveButton : Editor
{

    [SerializeField] private ConfigReader _configFileReader;

    //public void OnEnable()
    //{
    //    if (_configFileReader == null)
    //    {
    //        _configFileReader = target as YourScriptableObjClass;
    //    }
    //}

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Save"))
        {
            _configFileReader.ReadFiles();
        }
    }

}
#endif