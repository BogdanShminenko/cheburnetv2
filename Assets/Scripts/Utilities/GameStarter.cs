﻿using Assets.Scripts.Accrual;
using Assets.Scripts.Data.GameData.FileLoaders;
using Assets.Scripts.Game;
using Assets.Scripts.MVC.CurrencyMVC;
using Assets.Scripts.MVC.EventMVC;
using Assets.Scripts.MVC.InternetMVC;
using Assets.Scripts.MVC.LawsMVC;
using Assets.Scripts.MVC.ProjectMVC;
using Assets.Scripts.MVC.QuestMVC;
using Assets.Scripts.MVC.SaveSystemMVC;
using Assets.Scripts.MVC.ServerMVC;
using Assets.Scripts.MVC.TimerMVC;
using Assets.Scripts.MVC.UsersAttitude;
using Assets.Scripts.TriggersAndOperationsTypes;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Utilities
{
    public sealed class GameStarter : MonoBehaviour
    {
        [Inject] private ConfigFileReader _configReader;
        [Inject] private TimerController _timerController;
        [Inject] private InternetController _internetController;
        [Inject] private UsersAttitudeController _usersAttitudeController;
        [Inject] private EventController _eventController;
        [Inject] private ProjectController _projectController;
        [Inject] private LawController _lawController;
        [Inject] private ServerController _serverController;
        [Inject] private GameSettings _gameSettings;
        [Inject] private SaveSystemController _saveSystemController;
        [Inject] private MoneyModel _moneyModel;
        [Inject] private WillModel _willModel;
        [Inject] private GameSettingsData _settingsData;
        [Inject] private QuestController _questController;
        [Inject] private GameLoop _gameLoop;



        private void Awake()
        {
            _configReader.OnInitedData += StartGame;
        }

        private void StartGame()
        {
            if (_gameSettings.IsStartWithSave)
            {
                _saveSystemController.CallLoadSave();
            }
            else
            {
                StartWithoutSave();
            }

            //StartWithoutSave();

            _gameLoop.StartLoop();
        }

        private void StartWithoutSave()
        {
            _usersAttitudeController.InitUserAttitude();
            _timerController.StartTimer();
            _internetController.InitInternet();
            _eventController.InitEvents();
            _projectController.InitProjects();
            _lawController.InitLaws();
            _serverController.InitServers();
            _questController.InitQuests();
            _willModel.InitCurrency(_settingsData.MaxWillAmount, _settingsData.WillRestoreAmount, _settingsData.WillRestoreTimeCofficient);
            _moneyModel.InitCurrency(_settingsData.MaxMoneyAmount, _settingsData.MoneyRestoreAmount, _settingsData.MoneyRestoreTimeCofficient);
        }
    }
}