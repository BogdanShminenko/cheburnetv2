﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public class DateToStringConverter
    {
        private Dictionary<int, string> _mounthPerText = new Dictionary<int, string>()
        {
            [1] = "месяц",
            [2] = "месяца",
            [3] = "месяца",
            [4] = "месяца",
            [5] = "месяцев",
            [6] = "месяцев",
            [7] = "месяцев",
            [8] = "месяцев",
            [10] = "месяцев",
            [11] = "месяцев",
            [12] = "месяцев",
        };

        private string YearPerText(int year)
        {
            if(year <= 4)
            {
                return "года";
            }
            else
            {
                return "лет";
            }
        }

        public string ConvertDate(int year , int mounth)
        {
            if(year != 0 && mounth != 0)
            {
                return $"{year} {YearPerText(year)} и {mounth} {_mounthPerText[mounth]}";
            }else if(year != 0 && mounth == 0)
            {
                return $"{year} {YearPerText(year)}";
            }
            else if (year == 0 && mounth != 0)
            {
                return $"{mounth} {_mounthPerText[mounth]}";
            }
            return "";
        }
    }
}