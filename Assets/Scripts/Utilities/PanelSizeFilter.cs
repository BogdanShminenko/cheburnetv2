using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PanelSizeFilter : MonoBehaviour
{
    [SerializeField] private bool _notStartBasePosition;
    [SerializeField] private RectTransform _selfRect;
    [SerializeField] private RectTransform _targetObject;
    [SerializeField] private float _basePreferedHeightSize;
    [SerializeField] private float _preferedHeightSize;
    [SerializeField] private Vector2 _baseDeltaSize;
    [SerializeField] private Vector2 _basePosition;

    public void OnEnable()
    {
        CallResize();
    }

    public void Resize()
    {
        if(!_notStartBasePosition)
            SetBasePositions();
        CheckForChanges();
    }

    public void CallResize()
    {
        Invoke(nameof(Resize), 0.02f);
    }

    private void SetBasePositions()
    {
        _preferedHeightSize = _basePreferedHeightSize;
        _selfRect.sizeDelta = _baseDeltaSize;
        _selfRect.anchoredPosition = _basePosition;
    }

    public void CheckForChanges()
    {
        float ySizeDifference = _targetObject.sizeDelta.y - _preferedHeightSize;
        if (ySizeDifference > 0)
        {
            Vector2 currentSize = _selfRect.sizeDelta;
            Vector3 currentPosition = _selfRect.localPosition;
            currentSize.y += ySizeDifference;
            _selfRect.sizeDelta = currentSize;
            currentPosition.y -= ySizeDifference / 2; 
            _selfRect.localPosition = currentPosition;
            _preferedHeightSize = _targetObject.sizeDelta.y;
        }
    }
}
