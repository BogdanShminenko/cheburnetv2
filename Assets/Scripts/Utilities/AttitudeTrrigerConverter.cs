﻿using Assets.Scripts.Accrual;
using Assets.Scripts.TriggersAndOperationsTypes;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public class AttitudeTrrigerConverter 
    {

        // Конвертируем и опаределяем триггер
        public bool TryConvertAttitudeTrigger(string triggerAttitude, out AttitudeTrigger attitudeTrigger, out CurrencyType currencyType)
        {
            string[] trigger = triggerAttitude.Split('_');
            if (trigger[1] == "Rebel")
            {
                if (TryConvertTrigger(trigger, out AttitudeTrigger attituderTrigger))
                {
                    currencyType = CurrencyType.Rebel;
                    attitudeTrigger = attituderTrigger;
                    return true;
                }

            }
            else if (trigger[1] == "Loyal")
            {
                if (TryConvertTrigger(trigger, out AttitudeTrigger attituderTrigger))
                {
                    currencyType = CurrencyType.Loyal;
                    attitudeTrigger = attituderTrigger;
                    return true;
                }
            }
            else if (trigger[1] == "Neutral")
            {
                if (TryConvertTrigger(trigger, out AttitudeTrigger attituderTrigger))
                {
                    currencyType = CurrencyType.Neutral;
                    attitudeTrigger = attituderTrigger;
                    return true;
                }
            }
            currencyType = CurrencyType.Rebel;
            attitudeTrigger = null;
            return false;
        }

        // Попытка конвертировать значения и направления триггера
        private bool TryConvertTrigger(string[] triggersString, out AttitudeTrigger attitudeTrigger)
        {
            if (triggersString[3] == "H")
            {
                if (int.TryParse(triggersString[2], out int value))
                {
                    attitudeTrigger = new AttitudeTrigger(OperationType.Increase, value);
                    return true;
                }
                else
                    Debug.Log("Error rebbel trigger in Event");
            }
            else if (triggersString[3] == "L")
            {
                if (int.TryParse(triggersString[2], out int value))
                {
                    attitudeTrigger = new AttitudeTrigger(OperationType.Decrease, value);
                    return true;
                }
                else
                    Debug.Log("Error rebbel trigger in Event");
            }

            attitudeTrigger = null;
            return false;
        }

    }
}