using Assets.Scripts.GameStates;
using Assets.Scripts.MVC.EventMVC;
using Assets.Scripts.MVC.LawsMVC;
using Assets.Scripts.MVC.ProjectMVC;
using Assets.Scripts.MVC.QuestMVC;
using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ButtonsMenu : MonoBehaviour
{
    [SerializeField] private ButtonMenu[] _buttonMenus;
    [Inject] private QuestView _questView;
    [Inject] private ProjectView _projectView;
    [Inject] private LawView _lawView;
    [Inject] private EventsPanel _eventView;
    [Inject] private GameState _gameState;

    public void ResetButtons()
    {
        if (_gameState.CurrentGameState == GameStates.Events)
            _eventView.ClosePanel();
        else if (_gameState.CurrentGameState == GameStates.Quests)
            _questView.Close();
        else if (_gameState.CurrentGameState == GameStates.Projects)
            _projectView.CloseTreeWithChangeGameState();
        else if (_gameState.CurrentGameState == GameStates.Laws)
            _lawView.CloseLawsPanel();

        foreach (var item in _buttonMenus)
            item.ResetButton();
    }

    public void ResetButtonsWithOutOffPanels()
    {
        foreach (var item in _buttonMenus)
            item.ResetButton();
    }
}
