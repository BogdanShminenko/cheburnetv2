﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public static class MonthConverter
    {

        public static string GetMonthNumeral(int month)
        {
            if(month == 1)
                return "Месяц";
            else if(month <= 4 && month != 1)
                return "Месяца";
            else if (month >= 5)
                return "Месяцев";
            return "Месяцев";
        }
 
    }
}