using UnityEngine;
using System.Collections;
using UnityEditor;
using Assets.Scripts.MVC.ProjectMVC;

#if UNITY_EDITOR
[CustomEditor(typeof(ProjectController))]
public class LoadProjectButton : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ProjectController myScript = (ProjectController)target;
        if (GUILayout.Button("Load"))
        {
            myScript.LoadProjects();
        }
        if (GUILayout.Button("Update lines"))
        {
            myScript.UpdateLines();
        }
        if (GUILayout.Button("Resize"))
        {
            myScript.ResizeTreePanel();
        }
    }

}
#endif