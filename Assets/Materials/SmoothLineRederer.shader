Shader "Custom/SmoothLineShaderWithRotation"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _LineWidth ("Line Width", Range(0.01, 0.5)) = 0.1
        _RotationAngle ("Rotation Angle", Range(0, 360)) = 0
    }
    
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            
            struct appdata_t
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };
            
            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };
            
            sampler2D _MainTex;
            float _LineWidth;
            float _RotationAngle;
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

                // ������������ �������� �� ��������� ����
                float angleRad = radians(_RotationAngle);
                float2 rotatedUV = float2(
                    v.uv.x * cos(angleRad) - v.uv.y * sin(angleRad),
                    v.uv.x * sin(angleRad) + v.uv.y * cos(angleRad)
                );
                o.uv = rotatedUV;

                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                // ������������ �����-�������� ��� ����������� �����
                float2 uv = i.uv;
                float2 d = fwidth(uv);
                float2 grid = abs(frac(uv - 0.5) - 0.5) / d;
                float alpha = 1.0 - min(grid.x, grid.y);
                
                // ���������� �������� ������ ����� ��� ��������������� �����������
                alpha *= smoothstep(_LineWidth, 0, i.uv.x);
                
                // ��������� ��������
                fixed4 col = tex2D(_MainTex, i.uv);
                col.a *= alpha;
                return col;
            }
            ENDCG
        }
    }
}
