﻿Shader "Sprite/VerticalFillBottomToTopWithSorting"
{
    Properties
    {
        _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint Color", Color) = (1, 1, 1, 1)
        _FillAmount ("Fill Amount", Range(0, 1)) = 1
    }

    SubShader
    {
        Tags
        { 
        "Queue"="Transparent" 
        "RenderType"="Transparent" 
        }
        
        LOD 100

        Pass
        {
            ZWrite Off
            ZTest Always
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv       : TEXCOORD0;
                float4 vertex   : SV_POSITION;
            };
            
            float _FillAmount;
            fixed4 _Color;

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.uv = IN.texcoord;  // Store UV coordinates in v2f
                return OUT;
            }

            sampler2D _MainTex;
            fixed4 frag(v2f IN) : SV_Target
            {
                // Calculate the vertical fill based on Fill Amount
                if (IN.uv.y > _FillAmount)
                    discard;

                fixed4 c = tex2D(_MainTex, IN.uv);
                c.rgb *= c.a; // Multiply RGB by alpha to handle transparency
                c *= _Color; // Apply the tint color
                return c;
            }
            ENDCG
        }
    }
}
