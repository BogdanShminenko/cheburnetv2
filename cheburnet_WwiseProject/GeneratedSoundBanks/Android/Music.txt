Event	ID	Name			Wwise Object Path	Notes
	734886871	Pause_MUS_Theme			\Default Work Unit\MUS\Pause_MUS_Theme	
	1002182008	Continue_MUS_Theme			\Default Work Unit\MUS\Continue_MUS_Theme	
	1387273221	Play_MUS_Theme			\Default Work Unit\MUS\Play_MUS_Theme	
	3592307487	Stop_MUS_Theme			\Default Work Unit\MUS\Stop_MUS_Theme	

Switch Group	ID	Name			Wwise Object Path	Notes
	287349236	MUS_Tension_Switch			\Default Work Unit\MUS_Tension_Switch	

Switch	ID	Name	Switch Group			Notes
	171713761	MUS_Tension_Danger	MUS_Tension_Switch			
	1289733057	MUS_Tension_Normal	MUS_Tension_Switch			
	2324204055	MUS_Tension_Control	MUS_Tension_Switch			

State Group	ID	Name			Wwise Object Path	Notes
	766723505	Game_State			\Default Work Unit\Game_State	

State	ID	Name	State Group			Notes
	748895195	None	Game_State			
	1053859784	Game_Active	Game_State			
	1494646133	Game_Started	Game_State			
	2772308904	Game_Pause	Game_State			
	3147090477	Game_Showed_Popup	Game_State			
	3218375656	Game_Win	Game_State			
	3425053597	Game_Lose	Game_State			

Game Parameter	ID	Name			Wwise Object Path	Notes
	4018350178	MUS_Playback			\Default Work Unit\MUS_Playback	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	2603300	MUS_Final_Lose	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Final_Lose_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Game_Lose\MUS_Final_Lose		1496804
	46018050	3_PERC_1	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Perc_1_Loop_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Control\Music Control Theme 1\3_PERC_1		4286608
	68034760	MUS_Final_Roll_Transition	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Final_Roll_Transition_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Game_Final_Roll_Transition\MUS_Final_Roll_Transition		533916
	183115894	2_LOW BRASS	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Low_Brass_Loop_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Control\Music Control Theme 1\2_LOW BRASS		4286608
	199345610	7_LEAD_2	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Theme_2_Lead_2_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Normal\Music Main Theme 2\7_LEAD_2		4286608
	207748038	2_PERC_1	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Danger_Perc_1_Loop_1_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Danger\MUS_Main_Danger\2_PERC_1		4285764
	218906738	4_LEAD_1	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Danger_Lead_1_Loop_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Danger\MUS_Main_Danger\4_LEAD_1		2143352
	263534515	5_LEAD_2	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Danger_Lead_2_Loop_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Danger\MUS_Main_Danger\5_LEAD_2		2679168
	364961182	6_LEAD	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Theme_3_Lead_1_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Normal\Music Main Theme 3\6_LEAD		4286608
	397445131	Whoosh	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Perc_2_Break_3_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Whoosh_Passive_Transition\Whoosh		2679168
	418027305	5_PERC_BREAK	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Perc_3_Break_1_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Control\Music Control Theme 1\5_PERC_BREAK		2679168
	514000576	4_PERC_2	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Perc_2_Loop_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Control\Music Control Theme 1\4_PERC_2		4286608
	518128155	1_BODY_LOOP	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Danger_Body_Loop_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Danger\MUS_Main_Danger\1_BODY_LOOP		4285764
	540450298	3_PAD	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Pad_Loop_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Control\Music Control Theme 2\3_PAD		5358236
	561254907	3_PERC_2	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Danger_Perc_2_Loop_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Danger\MUS_Main_Danger\3_PERC_2		2143352
	632847094	3_PERC_BREAK	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Perc_1_Break_1_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Normal\Music Body Theme 1\3_PERC_BREAK		2679168
	643712319	6_LEAD	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Theme_1_Lead_1_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Normal\Music Main Theme 1\6_LEAD		4286608
	648550923	MUS_Final_Win	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Final_Win_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Game_Win\MUS_Final_Win		1810820
	687544205	Whoosh	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Danger_Whoosh_Break_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Whoosh_Active_Transition\Whoosh		4286608
	695959298	7_LEAD_2	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Theme_1_Lead_2_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Normal\Music Main Theme 1\7_LEAD_2		4286608
	747612909	2_PERC_1	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Danger_Perc_1_Loop_2_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Danger\MUS_Main_Danger\2_PERC_1		4286608
	749897873	2_PERC_1	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Danger_Perc_1_Loop_3_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Danger\MUS_Main_Danger\2_PERC_1		4286608
	785702053	Whoosh	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Perc_2_Break_2_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Whoosh_Passive_Transition\Whoosh		2679168
	871863783	6_LEAD	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Theme_2_Lead_1_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Normal\Music Main Theme 2\6_LEAD		4286608
	875125694	Whoosh	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Perc_2_Break_1_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Whoosh_Passive_Transition\Whoosh		2679168
	891660709	2_RHYTHM	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Rhythm_Loop_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Normal\Music Body Theme 1\2_RHYTHM		4286608
	984877936	1_BODY	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Body_Loop_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Control\Music Control Theme 1\1_BODY		4286608
	1008529897	3_PERC_BREAK	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Main_Perc_1_Break_2_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Normal\Music Body Theme 1\3_PERC_BREAK		2679168
	1045939216	7_LEAD_2	Y:\Developer\Unity\cheburnet\cheburnet_WwiseProject\.cache\Android\SFX\MUSIC\MUS_Theme_3_Lead_2_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\MUS_Main\MUS_Main_Normal\Music Main Theme 3\7_LEAD_2		4286608

