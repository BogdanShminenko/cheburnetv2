/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID CONTINUE_MUS_THEME = 1002182008U;
        static const AkUniqueID PAUSE_MUS_THEME = 734886871U;
        static const AkUniqueID PLAY_AMB_AMBIENCE_GAME = 3767329542U;
        static const AkUniqueID PLAY_AMB_AMBIENCE_MENU = 1323989005U;
        static const AkUniqueID PLAY_MUS_THEME = 1387273221U;
        static const AkUniqueID PLAY_UI_ACTION_FAILED = 3988201775U;
        static const AkUniqueID PLAY_UI_ACTION_RESTORED = 66960494U;
        static const AkUniqueID PLAY_UI_ACTION_SUCCESS = 2125888985U;
        static const AkUniqueID PLAY_UI_BUTTON_BASIC_CLICK = 1012064981U;
        static const AkUniqueID PLAY_UI_BUTTON_BUY_CLICK = 1488971667U;
        static const AkUniqueID PLAY_UI_BUTTON_INACTIVE_CLICK = 3304247840U;
        static const AkUniqueID PLAY_UI_BUTTON_OK_CLICK = 2925385593U;
        static const AkUniqueID PLAY_UI_BUTTON_VOLUME_CLICK = 3628389673U;
        static const AkUniqueID PLAY_UI_GENERIC_CLICK = 49475123U;
        static const AkUniqueID PLAY_UI_NEWSERVER_APPEARED = 3024354565U;
        static const AkUniqueID PLAY_UI_NEWUSER_APPEARED = 689636797U;
        static const AkUniqueID PLAY_UI_NOTIFY_AUTHORITY = 1702373358U;
        static const AkUniqueID PLAY_UI_NOTIFY_POPUP = 750348203U;
        static const AkUniqueID PLAY_UI_POPUP_CLOSE = 3704868366U;
        static const AkUniqueID PLAY_UI_TABBAR_CLICK = 2210275470U;
        static const AkUniqueID STOP_AMB_AMBIENCE_GAME = 535683132U;
        static const AkUniqueID STOP_AMB_AMBIENCE_MENU = 2398138763U;
        static const AkUniqueID STOP_MUS_THEME = 3592307487U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace GAME_STATE
        {
            static const AkUniqueID GROUP = 766723505U;

            namespace STATE
            {
                static const AkUniqueID GAME_ACTIVE = 1053859784U;
                static const AkUniqueID GAME_LOSE = 3425053597U;
                static const AkUniqueID GAME_PAUSE = 2772308904U;
                static const AkUniqueID GAME_SHOWED_POPUP = 3147090477U;
                static const AkUniqueID GAME_STARTED = 1494646133U;
                static const AkUniqueID GAME_WIN = 3218375656U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace GAME_STATE

        namespace VOLUME_MUSIC_STATE
        {
            static const AkUniqueID GROUP = 3232877205U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID VOLUME_MUSIC_OFF = 2614289165U;
                static const AkUniqueID VOLUME_MUSIC_ON = 424418753U;
            } // namespace STATE
        } // namespace VOLUME_MUSIC_STATE

        namespace VOLUME_SFX_STATE
        {
            static const AkUniqueID GROUP = 2662268689U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID VOLUME_SFX_OFF = 2849584537U;
                static const AkUniqueID VOLUME_SFX_ON = 994012525U;
            } // namespace STATE
        } // namespace VOLUME_SFX_STATE

    } // namespace STATES

    namespace SWITCHES
    {
        namespace MUS_TENSION_SWITCH
        {
            static const AkUniqueID GROUP = 287349236U;

            namespace SWITCH
            {
                static const AkUniqueID MUS_TENSION_CONTROL = 2324204055U;
                static const AkUniqueID MUS_TENSION_DANGER = 171713761U;
                static const AkUniqueID MUS_TENSION_NORMAL = 1289733057U;
            } // namespace SWITCH
        } // namespace MUS_TENSION_SWITCH

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID MUS_PLAYBACK = 4018350178U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID AMB = 1117531639U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID UI = 1551306167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMB = 1117531639U;
        static const AkUniqueID GAME_UI = 2125279911U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID UI = 1551306167U;
        static const AkUniqueID USER_UI = 886916554U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
