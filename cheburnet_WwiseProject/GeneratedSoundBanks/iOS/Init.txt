State Group	ID	Name			Wwise Object Path	Notes
	766723505	Game_State			\Default Work Unit\Game_State	
	2662268689	Volume_Sfx_State			\Default Work Unit\Volume_Sfx_State	
	3232877205	Volume_Music_State			\Default Work Unit\Volume_Music_State	

State	ID	Name	State Group			Notes
	748895195	None	Game_State			
	1053859784	Game_Active	Game_State			
	1494646133	Game_Started	Game_State			
	2772308904	Game_Pause	Game_State			
	3147090477	Game_Showed_Popup	Game_State			
	3218375656	Game_Win	Game_State			
	3425053597	Game_Lose	Game_State			
	748895195	None	Volume_Sfx_State			
	994012525	Volume_Sfx_On	Volume_Sfx_State			
	2849584537	Volume_Sfx_Off	Volume_Sfx_State			
	424418753	Volume_Music_On	Volume_Music_State			
	748895195	None	Volume_Music_State			
	2614289165	Volume_Music_Off	Volume_Music_State			

Custom State	ID	Name	State Group	Owner		Notes
	13858985	Game_Showed_Popup	Game_State	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\MUSIC		
	48034479	Volume_Music_Off	Volume_Music_State	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\MUSIC		
	74313885	Volume_Sfx_Off	Volume_Sfx_State	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\AMB		
	150104477	Volume_Sfx_Off	Volume_Sfx_State	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\UI		
	270394878	Game_Pause	Game_State	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\MUSIC		
	485453076	Game_Started	Game_State	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\UI\Game UI		
	516821238	Game_Showed_Popup	Game_State	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\AMB		
	897485950	Game_Pause	Game_State	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\AMB		
	937325245	Volume_Music_Off	Volume_Music_State	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\AMB		

Audio Bus	ID	Name			Wwise Object Path	Notes
	886916554	User UI			\Default Work Unit\Master Audio Bus\UI\User UI	
	1117531639	AMB			\Default Work Unit\Master Audio Bus\AMB	
	1551306167	UI			\Default Work Unit\Master Audio Bus\UI	
	2125279911	Game UI			\Default Work Unit\Master Audio Bus\UI\Game UI	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	
	3991942870	MUSIC			\Default Work Unit\Master Audio Bus\MUSIC	

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

